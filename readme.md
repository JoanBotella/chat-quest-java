# Chat Quest

A Java engine for Interactive Fiction games, or in castillian, Aventuras Conversacionales.

## License

GNU GPL v3 or later <https://www.gnu.org/licenses/gpl-3.0.en.html>

## Dependency

### JSON in Java [package org.json]

Repository: <https://github.com/stleary/JSON-java>
License: <https://github.com/stleary/JSON-java/blob/master/LICENSE>
Jar: <https://search.maven.org/remotecontent?filepath=org/json/json/20210307/json-20210307.jar>

## Author

Joan Botella Vinaches <https://joanbotella.com>
