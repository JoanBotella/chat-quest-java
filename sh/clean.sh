#! /bin/bash

source _config.sh

echo -n 'Cleaning... '



for DIR in "$DST_DIR" "$TMP_DIR" "$META_DIR"
do
	if [ -d "$DIR" ]
	then
		rm \
			-rf "$DIR"
	fi
done



echo 'Done.'
