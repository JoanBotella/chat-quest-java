
PROJECT_NAME="Chat Quest"

PROJECT_PACKAGE="chatQuest"

PROJECT_VERSION="0.0.0"

MANIFEST_CREATED_BY="Joan Botella Vinaches"

# Leave this variable as an empty string if project is a library
MANIFEST_MAIN_CLASS="$PROJECT_PACKAGE.demo.cli.Main"

PROJECT_DIR=".."

	DEP_DIR="$PROJECT_DIR/dep"

	DST_DIR="$PROJECT_DIR/dst"

		JAR_FILE="$DST_DIR/$PROJECT_PACKAGE-$PROJECT_VERSION.jar"

	META_DIR="$PROJECT_DIR/meta"

		MANIFEST_FILE="$META_DIR/manifest.txt"

	SRC_DIR="$PROJECT_DIR/src"

	TMP_DIR="$PROJECT_DIR/tmp"
