#! /bin/bash

source _config.sh

echo -n 'Compiling... '



if [ ! -d "$TMP_DIR" ]
then
	mkdir -p "$TMP_DIR"
fi



rsync --archive --progress --delete "$SRC_DIR/" "$TMP_DIR/" > /dev/null



if [ ! -d "$DEP_DIR" ]
then
	mkdir -p "$DEP_DIR"
fi



CLASSPATH="$JAR_FILE"

for FILE in `find "$DEP_DIR" -name *.jar`
do
	CLASSPATH="$CLASSPATH:$FILE"
done



javac \
	-sourcepath "$TMP_DIR" \
	-classpath "$CLASSPATH" \
	`find "$TMP_DIR" -name "*.java"`



echo 'Done.'