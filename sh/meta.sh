#! /bin/bash

source _config.sh

echo -n 'Creating manifest... '



if [ ! -d "$META_DIR" ]
then
	mkdir -p "$META_DIR"
fi



echo "Created-By: $MANIFEST_CREATED_BY" > "$MANIFEST_FILE"



if [ "$MANIFEST_MAIN_CLASS" != '' ]
then
	echo "Main-Class: $MANIFEST_MAIN_CLASS" >> "$MANIFEST_FILE"
fi



if [ ! -d "$DEP_DIR" ]
then
	mkdir -p "$DEP_DIR"
fi



DEPENDENCIES=('json-20210307')

for FILE in `find "$DEP_DIR" -name *.jar`
do
	DEPENDENCIES+=("$FILE")
done

if [ ${#DEPENDENCIES[@]} -gt 0 ]
then
	echo "Class-Path: ${DEPENDENCIES[@]}" >> "$MANIFEST_FILE"
fi



echo 'Done.'