#! /bin/bash

source _config.sh


./clean.sh

if [ $? -gt 0 ]
then
	exit 1
fi

./meta.sh

if [ $? -gt 0 ]
then
	exit 2
fi

./cmp.sh

if [ $? -gt 0 ]
then
	exit 3
fi



echo -n 'Building... '



if [ ! -d "$DST_DIR" ]
then
	mkdir -p "$DST_DIR"
fi



rm `find "$TMP_DIR" -name "*.java"` 2> /dev/null



cd "$TMP_DIR"

jar \
	-cfm \
		"$JAR_FILE" \
		"$MANIFEST_FILE" \
		*

JAR_EXIT_CODE=$?

cd - > /dev/null

if [ $JAR_EXIT_CODE -eq 1 ]
then
	exit 1
fi



echo 'Done.'