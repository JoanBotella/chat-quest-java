package chatQuest.demo.cli.main.library.serviceContainer;

import chatQuest.demo.cli.main.library.serviceContainerComponent.DemoCliServiceContainerComponentItf;
import chatQuest.demo.core.main.library.serviceContainerComponent.DemoCoreServiceContainerComponentItf;

public interface DemoCliServiceContainerItf
	extends
		DemoCliServiceContainerComponentItf,
		DemoCoreServiceContainerComponentItf
{

}
