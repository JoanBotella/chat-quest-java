package chatQuest.demo.cli.main.library.serviceContainer;

import chatQuest.demo.core.main.library.DemoCoreNodeId;
import chatQuest.demo.core.main.service.enabledLanguagesContainer.DemoCoreEnabledLanguagesContainer;
import chatQuest.demo.core.main.service.enabledLanguagesContainer.DemoCoreEnabledLanguagesContainerItf;
import chatQuest.demo.core.main.service.nodeBuilder.DemoCoreNodeBuilder;
import chatQuest.demo.core.main.service.nodeBuilder.DemoCoreNodeBuilderItf;
import chatQuest.demo.core.main.service.router.DemoCoreRouter;
import chatQuest.demo.core.main.service.router.DemoCoreRouterItf;
import chatQuest.demo.core.translation.service.translationContainer.DemoCoreTranslationContainer;
import chatQuest.demo.core.translation.service.translationContainer.DemoCoreTranslationContainerItf;
import chatQuest.fw.cli.main.service.appRequestBuilder.FwCliAppRequestBuilder;
import chatQuest.fw.cli.main.service.appRequestBuilder.FwCliAppRequestBuilderItf;
import chatQuest.fw.cli.main.service.appResponseDispatcher.FwCliAppResponseDispatcher;
import chatQuest.fw.cli.main.service.appResponseDispatcher.FwCliAppResponseDispatcherItf;
import chatQuest.fw.cli.main.service.appRunner.FwCliAppRunner;
import chatQuest.fw.cli.main.service.appRunner.FwCliAppRunnerItf;
import chatQuest.fw.cli.main.service.prompter.FwCliPrompter;
import chatQuest.fw.cli.main.service.prompter.FwCliPrompterItf;
import chatQuest.fw.cli.main.service.queryBuilder.FwCliQueryBuilder;
import chatQuest.fw.cli.main.service.queryBuilder.FwCliQueryBuilderItf;
import chatQuest.fw.cli.main.service.queryNormalizerContainer.FwCliQueryNormalizerContainer;
import chatQuest.fw.cli.main.service.queryNormalizerContainer.FwCliQueryNormalizerContainerItf;
import chatQuest.fw.cli.main.service.stylizer.FwCliStylizer;
import chatQuest.fw.cli.main.service.stylizer.FwCliStylizerItf;
import chatQuest.fw.cli.translation.service.translationContainer.FwCliTranslationContainer;
import chatQuest.fw.cli.translation.service.translationContainer.FwCliTranslationContainerItf;
import chatQuest.fw.core.main.library.language.FwCoreLanguageCode;
import chatQuest.fw.core.main.service.app.FwCoreApp;
import chatQuest.fw.core.main.service.app.FwCoreAppItf;
import chatQuest.fw.core.main.service.appResponseByNodeResponseBuilder.FwCoreAppResponseByNodeResponseBuilder;
import chatQuest.fw.core.main.service.appResponseByNodeResponseBuilder.FwCoreAppResponseByNodeResponseBuilderItf;
import chatQuest.fw.core.main.service.bodyBuilderBySnapshotCreator.FwCoreBodyBuilderBySnapshotCreator;
import chatQuest.fw.core.main.service.bodyBuilderBySnapshotCreator.FwCoreBodyBuilderBySnapshotCreatorItf;
import chatQuest.fw.core.main.service.bodyBuilderBySnapshotDeleter.FwCoreBodyBuilderBySnapshotDeleter;
import chatQuest.fw.core.main.service.bodyBuilderBySnapshotDeleter.FwCoreBodyBuilderBySnapshotDeleterItf;
import chatQuest.fw.core.main.service.bodyBuilderBySnapshotLoader.FwCoreBodyBuilderBySnapshotLoader;
import chatQuest.fw.core.main.service.bodyBuilderBySnapshotLoader.FwCoreBodyBuilderBySnapshotLoaderItf;
import chatQuest.fw.core.main.service.bodyBuilderBySnapshotOverwriter.FwCoreBodyBuilderBySnapshotOverwriter;
import chatQuest.fw.core.main.service.bodyBuilderBySnapshotOverwriter.FwCoreBodyBuilderBySnapshotOverwriterItf;
import chatQuest.fw.core.main.service.breadcrumbsCopier.FwCoreBreadcrumbsCopier;
import chatQuest.fw.core.main.service.breadcrumbsCopier.FwCoreBreadcrumbsCopierItf;
import chatQuest.fw.core.main.service.configurationBuilder.FwCoreConfigurationBuilder;
import chatQuest.fw.core.main.service.configurationBuilder.FwCoreConfigurationBuilderItf;
import chatQuest.fw.core.main.service.configurationContainer.FwCoreConfigurationContainer;
import chatQuest.fw.core.main.service.configurationContainer.FwCoreConfigurationContainerItf;
import chatQuest.fw.core.main.service.fileBuilderBySnapshotMetadatasMenuQuery.FwCoreFileBuilderBySnapshotMetadatasMenuQuery;
import chatQuest.fw.core.main.service.fileBuilderBySnapshotMetadatasMenuQuery.FwCoreFileBuilderBySnapshotMetadatasMenuQueryItf;
import chatQuest.fw.core.main.service.fileLineReader.FwCoreFileLineReader;
import chatQuest.fw.core.main.service.fileLineReader.FwCoreFileLineReaderItf;
import chatQuest.fw.core.main.service.fileWriter.FwCoreFileWriter;
import chatQuest.fw.core.main.service.fileWriter.FwCoreFileWriterItf;
import chatQuest.fw.core.main.service.settingsCopier.FwCoreSettingsCopier;
import chatQuest.fw.core.main.service.settingsCopier.FwCoreSettingsCopierItf;
import chatQuest.fw.core.main.service.settingsJsonizer.FwCoreSettingsJsonizer;
import chatQuest.fw.core.main.service.settingsJsonizer.FwCoreSettingsJsonizerItf;
import chatQuest.fw.core.main.service.settingsUnjsonizer.FwCoreSettingsUnjsonizer;
import chatQuest.fw.core.main.service.settingsUnjsonizer.FwCoreSettingsUnjsonizerItf;
import chatQuest.fw.core.main.service.snapshotContentBuilderAndToFileWriter.FwCoreSnapshotContentBuilderAndToFileWriter;
import chatQuest.fw.core.main.service.snapshotContentBuilderAndToFileWriter.FwCoreSnapshotContentBuilderAndToFileWriterItf;
import chatQuest.fw.core.main.service.snapshotCreator.FwCoreSnapshotCreator;
import chatQuest.fw.core.main.service.snapshotCreator.FwCoreSnapshotCreatorItf;
import chatQuest.fw.core.main.service.snapshotDeleter.FwCoreSnapshotDeleter;
import chatQuest.fw.core.main.service.snapshotDeleter.FwCoreSnapshotDeleterItf;
import chatQuest.fw.core.main.service.snapshotFileBuilder.FwCoreSnapshotFileBuilder;
import chatQuest.fw.core.main.service.snapshotFileBuilder.FwCoreSnapshotFileBuilderItf;
import chatQuest.fw.core.main.service.snapshotFileContentBuilder.FwCoreSnapshotFileContentBuilder;
import chatQuest.fw.core.main.service.snapshotFileContentBuilder.FwCoreSnapshotFileContentBuilderItf;
import chatQuest.fw.core.main.service.snapshotFileParser.FwCoreSnapshotFileParser;
import chatQuest.fw.core.main.service.snapshotFileParser.FwCoreSnapshotFileParserItf;
import chatQuest.fw.core.main.service.snapshotFilesBuilder.FwCoreSnapshotFilesBuilder;
import chatQuest.fw.core.main.service.snapshotFilesBuilder.FwCoreSnapshotFilesBuilderItf;
import chatQuest.fw.core.main.service.snapshotLoader.FwCoreSnapshotLoader;
import chatQuest.fw.core.main.service.snapshotLoader.FwCoreSnapshotLoaderItf;
import chatQuest.fw.core.main.service.snapshotMetadataJsonizer.FwCoreSnapshotMetadataJsonizer;
import chatQuest.fw.core.main.service.snapshotMetadataJsonizer.FwCoreSnapshotMetadataJsonizerItf;
import chatQuest.fw.core.main.service.snapshotMetadataSerializer.FwCoreSnapshotMetadataSerializer;
import chatQuest.fw.core.main.service.snapshotMetadataSerializer.FwCoreSnapshotMetadataSerializerItf;
import chatQuest.fw.core.main.service.snapshotMetadataUnjsonizer.FwCoreSnapshotMetadataUnjsonizer;
import chatQuest.fw.core.main.service.snapshotMetadataUnjsonizer.FwCoreSnapshotMetadataUnjsonizerItf;
import chatQuest.fw.core.main.service.snapshotMetadataUnserializer.FwCoreSnapshotMetadataUnserializer;
import chatQuest.fw.core.main.service.snapshotMetadataUnserializer.FwCoreSnapshotMetadataUnserializerItf;
import chatQuest.fw.core.main.service.snapshotMetadatasLister.FwCoreSnapshotMetadatasLister;
import chatQuest.fw.core.main.service.snapshotMetadatasLister.FwCoreSnapshotMetadatasListerItf;
import chatQuest.fw.core.main.service.snapshotMetadatasMenuBuilder.FwCoreSnapshotMetadatasMenuBuilder;
import chatQuest.fw.core.main.service.snapshotMetadatasMenuBuilder.FwCoreSnapshotMetadatasMenuBuilderItf;
import chatQuest.fw.core.main.service.snapshotOverwriter.FwCoreSnapshotOverwriter;
import chatQuest.fw.core.main.service.snapshotOverwriter.FwCoreSnapshotOverwriterItf;
import chatQuest.fw.core.main.service.nextNodeRequestBuilder.FwCoreNextNodeRequestBuilder;
import chatQuest.fw.core.main.service.nextNodeRequestBuilder.FwCoreNextNodeRequestBuilderItf;
import chatQuest.fw.core.main.service.nodeBrowser.FwCoreNodeBrowser;
import chatQuest.fw.core.main.service.nodeBrowser.FwCoreNodeBrowserItf;
import chatQuest.fw.core.main.service.nodeRequestByAppRequestBuilder.FwCoreNodeRequestByAppRequestBuilder;
import chatQuest.fw.core.main.service.nodeRequestByAppRequestBuilder.FwCoreNodeRequestByAppRequestBuilderItf;
import chatQuest.fw.core.main.service.nodeRequestProcessor.FwCoreNodeRequestProcessor;
import chatQuest.fw.core.main.service.nodeRequestProcessor.FwCoreNodeRequestProcessorItf;
import chatQuest.fw.core.main.service.nodeResponseByNodeRequestBuilder.FwCoreNodeResponseByNodeRequestBuilder;
import chatQuest.fw.core.main.service.nodeResponseByNodeRequestBuilder.FwCoreNodeResponseByNodeRequestBuilderItf;
import chatQuest.fw.core.main.service.randomStringGenerator.FwCoreRandomStringGenerator;
import chatQuest.fw.core.main.service.randomStringGenerator.FwCoreRandomStringGeneratorItf;
import chatQuest.fw.core.main.service.startingStatusBuilder.FwCoreStartingStatusBuilder;
import chatQuest.fw.core.main.service.startingStatusBuilder.FwCoreStartingStatusBuilderItf;
import chatQuest.fw.core.main.service.statusCopier.FwCoreStatusCopier;
import chatQuest.fw.core.main.service.statusCopier.FwCoreStatusCopierItf;
import chatQuest.fw.core.main.service.statusJsonizer.FwCoreStatusJsonizer;
import chatQuest.fw.core.main.service.statusJsonizer.FwCoreStatusJsonizerItf;
import chatQuest.fw.core.main.service.statusSerializer.FwCoreStatusSerializer;
import chatQuest.fw.core.main.service.statusSerializer.FwCoreStatusSerializerItf;
import chatQuest.fw.core.main.service.statusUnjsonizer.FwCoreStatusUnjsonizer;
import chatQuest.fw.core.main.service.statusUnjsonizer.FwCoreStatusUnjsonizerItf;
import chatQuest.fw.core.main.service.statusUnserializer.FwCoreStatusUnserializer;
import chatQuest.fw.core.main.service.statusUnserializer.FwCoreStatusUnserializerItf;
import chatQuest.fw.core.translation.service.translationContainer.FwCoreTranslationContainer;
import chatQuest.fw.core.translation.service.translationContainer.FwCoreTranslationContainerItf;
import chatQuest.fw.core.main.service.variablesCopier.FwCoreVariablesCopier;
import chatQuest.fw.core.main.service.variablesCopier.FwCoreVariablesCopierItf;

public final class DemoCliServiceContainer
	implements
		DemoCliServiceContainerItf
{

// --- FwCore ---

	private FwCoreAppItf fwCoreApp;

	public FwCoreAppItf getFwCoreApp()
	{
		if (fwCoreApp == null)
		{
			fwCoreApp = buildFwCoreApp();
		}
		return fwCoreApp;
	}

		protected FwCoreAppItf buildFwCoreApp()
		{
			return new FwCoreApp(
				getFwCoreNodeRequestByAppRequestBuilder(),
				getFwCoreNodeRequestProcessor(),
				getFwCoreNextNodeRequestBuilder(),
				getFwCoreAppResponseByNodeResponseBuilder()
			);
		}

	private FwCoreAppResponseByNodeResponseBuilderItf fwCoreAppResponseByNodeResponseBuilder;

	public FwCoreAppResponseByNodeResponseBuilderItf getFwCoreAppResponseByNodeResponseBuilder()
	{
		if (fwCoreAppResponseByNodeResponseBuilder == null)
		{
			fwCoreAppResponseByNodeResponseBuilder = buildFwCoreAppResponseByNodeResponseBuilder();
		}
		return fwCoreAppResponseByNodeResponseBuilder;
	}

		protected FwCoreAppResponseByNodeResponseBuilderItf buildFwCoreAppResponseByNodeResponseBuilder()
		{
			return new FwCoreAppResponseByNodeResponseBuilder();
		}

	private FwCoreBreadcrumbsCopierItf fwCoreBreadcrumbsCopier;

	public FwCoreBreadcrumbsCopierItf getFwCoreBreadcrumbsCopier()
	{
		if (fwCoreBreadcrumbsCopier == null)
		{
			fwCoreBreadcrumbsCopier = buildFwCoreBreadcrumbsCopier();
		}
		return fwCoreBreadcrumbsCopier;
	}

		protected FwCoreBreadcrumbsCopierItf buildFwCoreBreadcrumbsCopier()
		{
			return new FwCoreBreadcrumbsCopier();
		}

	private FwCoreSettingsCopierItf fwCoreSettingsCopier;

	public FwCoreSettingsCopierItf getFwCoreSettingsCopier()
	{
		if (fwCoreSettingsCopier == null)
		{
			fwCoreSettingsCopier = buildFwCoreSettingsCopier();
		}
		return fwCoreSettingsCopier;
	}

		protected FwCoreSettingsCopierItf buildFwCoreSettingsCopier()
		{
			return new FwCoreSettingsCopier();
		}

	private FwCoreNextNodeRequestBuilderItf fwCoreNextNodeRequestBuilder;

	public FwCoreNextNodeRequestBuilderItf getFwCoreNextNodeRequestBuilder()
	{
		if (fwCoreNextNodeRequestBuilder == null)
		{
			fwCoreNextNodeRequestBuilder = buildFwCoreNextNodeRequestBuilder();
		}
		return fwCoreNextNodeRequestBuilder;
	}

		protected FwCoreNextNodeRequestBuilderItf buildFwCoreNextNodeRequestBuilder()
		{
			return new FwCoreNextNodeRequestBuilder();
		}

	private FwCoreNodeBrowserItf fwCoreNodeBrowser;

	public FwCoreNodeBrowserItf getFwCoreNodeBrowser()
	{
		if (fwCoreNodeBrowser == null)
		{
			fwCoreNodeBrowser = buildFwCoreNodeBrowser();
		}
		return fwCoreNodeBrowser;
	}

		protected FwCoreNodeBrowserItf buildFwCoreNodeBrowser()
		{
			return new FwCoreNodeBrowser();
		}

	private FwCoreNodeRequestByAppRequestBuilderItf fwCoreNodeRequestByAppRequestBuilder;

	public FwCoreNodeRequestByAppRequestBuilderItf getFwCoreNodeRequestByAppRequestBuilder()
	{
		if (fwCoreNodeRequestByAppRequestBuilder == null)
		{
			fwCoreNodeRequestByAppRequestBuilder = buildFwCoreNodeRequestByAppRequestBuilder();
		}
		return fwCoreNodeRequestByAppRequestBuilder;
	}

		protected FwCoreNodeRequestByAppRequestBuilderItf buildFwCoreNodeRequestByAppRequestBuilder()
		{
			return new FwCoreNodeRequestByAppRequestBuilder();
		}

	private FwCoreNodeRequestProcessorItf fwCoreNodeRequestProcessor;

	public FwCoreNodeRequestProcessorItf getFwCoreNodeRequestProcessor()
	{
		if (fwCoreNodeRequestProcessor == null)
		{
			fwCoreNodeRequestProcessor = buildFwCoreNodeRequestProcessor();
		}
		return fwCoreNodeRequestProcessor;
	}

		protected FwCoreNodeRequestProcessorItf buildFwCoreNodeRequestProcessor()
		{
			return new FwCoreNodeRequestProcessor(
				getDemoCoreRouter(),
				getDemoCoreNodeBuilder().system()
			);
		}

	private FwCoreNodeResponseByNodeRequestBuilderItf fwCoreNodeResponseByNodeRequestBuilder;

	public FwCoreNodeResponseByNodeRequestBuilderItf getFwCoreNodeResponseByNodeRequestBuilder()
	{
		if (fwCoreNodeResponseByNodeRequestBuilder == null)
		{
			fwCoreNodeResponseByNodeRequestBuilder = buildFwCoreNodeResponseByNodeRequestBuilder();
		}
		return fwCoreNodeResponseByNodeRequestBuilder;
	}

		protected FwCoreNodeResponseByNodeRequestBuilderItf buildFwCoreNodeResponseByNodeRequestBuilder()
		{
			return new FwCoreNodeResponseByNodeRequestBuilder(
				getFwCoreStatusCopier()
			);
		}

	private FwCoreStartingStatusBuilderItf fwCoreStartingStatusBuilder;

	public FwCoreStartingStatusBuilderItf getFwCoreStartingStatusBuilder()
	{
		if (fwCoreStartingStatusBuilder == null)
		{
			fwCoreStartingStatusBuilder = buildFwCoreStartingStatusBuilder();
		}
		return fwCoreStartingStatusBuilder;
	}

		protected FwCoreStartingStatusBuilderItf buildFwCoreStartingStatusBuilder()
		{
			return new FwCoreStartingStatusBuilder(
				DemoCoreNodeId.TITLE_MENU,
				FwCoreLanguageCode.EN
			);
		}

	private FwCoreStatusCopierItf fwCoreStatusCopier;

	public FwCoreStatusCopierItf getFwCoreStatusCopier()
	{
		if (fwCoreStatusCopier == null)
		{
			fwCoreStatusCopier = buildFwCoreStatusCopier();
		}
		return fwCoreStatusCopier;
	}

		protected FwCoreStatusCopierItf buildFwCoreStatusCopier()
		{
			return new FwCoreStatusCopier(
				getFwCoreBreadcrumbsCopier(),
				getFwCoreSettingsCopier(),
				getFwCoreVariablesCopier()
			);
		}

	private FwCoreTranslationContainerItf fwCoreTranslationContainer;

	public FwCoreTranslationContainerItf getFwCoreTranslationContainer()
	{
		if (fwCoreTranslationContainer == null)
		{
			fwCoreTranslationContainer = buildFwCoreTranslationContainer();
		}
		return fwCoreTranslationContainer;
	}

		protected FwCoreTranslationContainerItf buildFwCoreTranslationContainer()
		{
			return new FwCoreTranslationContainer();
		}

	private FwCoreVariablesCopierItf fwCoreVariablesCopier;

	public FwCoreVariablesCopierItf getFwCoreVariablesCopier()
	{
		if (fwCoreVariablesCopier == null)
		{
			fwCoreVariablesCopier = buildFwCoreVariablesCopier();
		}
		return fwCoreVariablesCopier;
	}

		protected FwCoreVariablesCopierItf buildFwCoreVariablesCopier()
		{
			return new FwCoreVariablesCopier();
		}

	private FwCoreSettingsJsonizerItf fwCoreSettingsJsonizer;

	public FwCoreSettingsJsonizerItf getFwCoreSettingsJsonizer()
	{
		if (fwCoreSettingsJsonizer == null)
		{
			fwCoreSettingsJsonizer = buildFwCoreSettingsJsonizer();
		}
		return fwCoreSettingsJsonizer;
	}

		protected FwCoreSettingsJsonizerItf buildFwCoreSettingsJsonizer()
		{
			return new FwCoreSettingsJsonizer();
		}

	private FwCoreStatusJsonizerItf fwCoreStatusJsonizer;

	public FwCoreStatusJsonizerItf getFwCoreStatusJsonizer()
	{
		if (fwCoreStatusJsonizer == null)
		{
			fwCoreStatusJsonizer = buildFwCoreStatusJsonizer();
		}
		return fwCoreStatusJsonizer;
	}

		protected FwCoreStatusJsonizerItf buildFwCoreStatusJsonizer()
		{
			return new FwCoreStatusJsonizer(
				getFwCoreSettingsJsonizer()
			);
		}

	private FwCoreStatusSerializerItf fwCoreStatusSerializer;

	public FwCoreStatusSerializerItf getFwCoreStatusSerializer()
	{
		if (fwCoreStatusSerializer == null)
		{
			fwCoreStatusSerializer = buildFwCoreStatusSerializer();
		}
		return fwCoreStatusSerializer;
	}

		protected FwCoreStatusSerializerItf buildFwCoreStatusSerializer()
		{
			return new FwCoreStatusSerializer(
				getFwCoreStatusJsonizer()
			);
		}

	private FwCoreStatusUnserializerItf fwCoreStatusUnserializer;

	public FwCoreStatusUnserializerItf getFwCoreStatusUnserializer()
	{
		if (fwCoreStatusUnserializer == null)
		{
			fwCoreStatusUnserializer = buildFwCoreStatusUnserializer();
		}
		return fwCoreStatusUnserializer;
	}

		protected FwCoreStatusUnserializerItf buildFwCoreStatusUnserializer()
		{
			return new FwCoreStatusUnserializer(
				getFwCoreStatusUnjsonizer()
			);
		}

	private FwCoreStatusUnjsonizerItf fwCoreStatusUnjsonizer;

	public FwCoreStatusUnjsonizerItf getFwCoreStatusUnjsonizer()
	{
		if (fwCoreStatusUnjsonizer == null)
		{
			fwCoreStatusUnjsonizer = buildFwCoreStatusUnjsonizer();
		}
		return fwCoreStatusUnjsonizer;
	}

		protected FwCoreStatusUnjsonizerItf buildFwCoreStatusUnjsonizer()
		{
			return new FwCoreStatusUnjsonizer(
				getFwCoreSettingsUnjsonizer()
			);
		}

	private FwCoreSettingsUnjsonizerItf fwCoreSettingsUnjsonizer;

	public FwCoreSettingsUnjsonizerItf getFwCoreSettingsUnjsonizer()
	{
		if (fwCoreSettingsUnjsonizer == null)
		{
			fwCoreSettingsUnjsonizer = buildFwCoreSettingsUnjsonizer();
		}
		return fwCoreSettingsUnjsonizer;
	}

		protected FwCoreSettingsUnjsonizerItf buildFwCoreSettingsUnjsonizer()
		{
			return new FwCoreSettingsUnjsonizer();
		}

	private FwCoreConfigurationBuilderItf fwCoreConfigurationBuilder;

	public FwCoreConfigurationBuilderItf getFwCoreConfigurationBuilder()
	{
		if (fwCoreConfigurationBuilder == null)
		{
			fwCoreConfigurationBuilder = buildFwCoreConfigurationBuilder();
		}
		return fwCoreConfigurationBuilder;
	}

		protected FwCoreConfigurationBuilderItf buildFwCoreConfigurationBuilder()
		{
			return new FwCoreConfigurationBuilder();
		}

	private FwCoreConfigurationContainerItf fwCoreConfigurationContainer;

	public FwCoreConfigurationContainerItf getFwCoreConfigurationContainer()
	{
		if (fwCoreConfigurationContainer == null)
		{
			fwCoreConfigurationContainer = buildFwCoreConfigurationContainer();
		}
		return fwCoreConfigurationContainer;
	}

		protected FwCoreConfigurationContainerItf buildFwCoreConfigurationContainer()
		{
			return new FwCoreConfigurationContainer(
				getFwCoreConfigurationBuilder()
			);
		}

	private FwCoreSnapshotMetadatasListerItf fwCoreSnapshotMetadatasLister;

	public FwCoreSnapshotMetadatasListerItf getFwCoreSnapshotMetadatasLister()
	{
		if (fwCoreSnapshotMetadatasLister == null)
		{
			fwCoreSnapshotMetadatasLister = buildFwCoreSnapshotMetadatasLister();
		}
		return fwCoreSnapshotMetadatasLister;
	}

		protected FwCoreSnapshotMetadatasListerItf buildFwCoreSnapshotMetadatasLister()
		{
			return new FwCoreSnapshotMetadatasLister(
				getFwCoreSnapshotFilesBuilder(),
				getFwCoreSnapshotFileParser()
			);
		}

	private FwCoreSnapshotMetadataSerializerItf fwCoreSnapshotMetadataSerializer;

	public FwCoreSnapshotMetadataSerializerItf getFwCoreSnapshotMetadataSerializer()
	{
		if (fwCoreSnapshotMetadataSerializer == null)
		{
			fwCoreSnapshotMetadataSerializer = buildFwCoreSnapshotMetadataSerializer();
		}
		return fwCoreSnapshotMetadataSerializer;
	}

		protected FwCoreSnapshotMetadataSerializerItf buildFwCoreSnapshotMetadataSerializer()
		{
			return new FwCoreSnapshotMetadataSerializer(
				getFwCoreSnapshotMetadataJsonizer()
			);
		}

	private FwCoreSnapshotMetadataUnserializerItf fwCoreSnapshotMetadataUnserializer;

	public FwCoreSnapshotMetadataUnserializerItf getFwCoreSnapshotMetadataUnserializer()
	{
		if (fwCoreSnapshotMetadataUnserializer == null)
		{
			fwCoreSnapshotMetadataUnserializer = buildFwCoreSnapshotMetadataUnserializer();
		}
		return fwCoreSnapshotMetadataUnserializer;
	}

		protected FwCoreSnapshotMetadataUnserializerItf buildFwCoreSnapshotMetadataUnserializer()
		{
			return new FwCoreSnapshotMetadataUnserializer(
				getFwCoreSnapshotMetadataUnjsonizer()
			);
		}

	private FwCoreSnapshotMetadataJsonizerItf fwCoreSnapshotMetadataJsonizer;

	public FwCoreSnapshotMetadataJsonizerItf getFwCoreSnapshotMetadataJsonizer()
	{
		if (fwCoreSnapshotMetadataJsonizer == null)
		{
			fwCoreSnapshotMetadataJsonizer = buildFwCoreSnapshotMetadataJsonizer();
		}
		return fwCoreSnapshotMetadataJsonizer;
	}

		protected FwCoreSnapshotMetadataJsonizerItf buildFwCoreSnapshotMetadataJsonizer()
		{
			return new FwCoreSnapshotMetadataJsonizer();
		}

	private FwCoreSnapshotMetadataUnjsonizerItf fwCoreSnapshotMetadataUnjsonizer;

	public FwCoreSnapshotMetadataUnjsonizerItf getFwCoreSnapshotMetadataUnjsonizer()
	{
		if (fwCoreSnapshotMetadataUnjsonizer == null)
		{
			fwCoreSnapshotMetadataUnjsonizer = buildFwCoreSnapshotMetadataUnjsonizer();
		}
		return fwCoreSnapshotMetadataUnjsonizer;
	}

		protected FwCoreSnapshotMetadataUnjsonizerItf buildFwCoreSnapshotMetadataUnjsonizer()
		{
			return new FwCoreSnapshotMetadataUnjsonizer();
		}

	private FwCoreSnapshotCreatorItf fwCoreSnapshotCreator;

	public FwCoreSnapshotCreatorItf getFwCoreSnapshotCreator()
	{
		if (fwCoreSnapshotCreator == null)
		{
			fwCoreSnapshotCreator = buildFwCoreSnapshotCreator();
		}
		return fwCoreSnapshotCreator;
	}

		protected FwCoreSnapshotCreatorItf buildFwCoreSnapshotCreator()
		{
			return new FwCoreSnapshotCreator(
				getFwCoreConfigurationContainer(),
				getFwCoreSnapshotFileBuilder(),
				getFwCoreSnapshotContentBuilderAndToFileWriter()
			);
		}

	private FwCoreRandomStringGeneratorItf fwCoreRandomStringGenerator;

	public FwCoreRandomStringGeneratorItf getFwCoreRandomStringGenerator()
	{
		if (fwCoreRandomStringGenerator == null)
		{
			fwCoreRandomStringGenerator = buildFwCoreRandomStringGenerator();
		}
		return fwCoreRandomStringGenerator;
	}

		protected FwCoreRandomStringGeneratorItf buildFwCoreRandomStringGenerator()
		{
			return new FwCoreRandomStringGenerator();
		}

	private FwCoreSnapshotFileParserItf fwCoreSnapshotFileParser;

	public FwCoreSnapshotFileParserItf getFwCoreSnapshotFileParser()
	{
		if (fwCoreSnapshotFileParser == null)
		{
			fwCoreSnapshotFileParser = buildFwCoreSnapshotFileParser();
		}
		return fwCoreSnapshotFileParser;
	}

		protected FwCoreSnapshotFileParserItf buildFwCoreSnapshotFileParser()
		{
			return new FwCoreSnapshotFileParser(
				getFwCoreSnapshotMetadataUnserializer(),
				getFwCoreStatusUnserializer(),
				getFwCoreFileLineReader()
			);
		}

	private FwCoreFileLineReaderItf fwCoreFileLineReader;

	public FwCoreFileLineReaderItf getFwCoreFileLineReader()
	{
		if (fwCoreFileLineReader == null)
		{
			fwCoreFileLineReader = buildFwCoreFileLineReader();
		}
		return fwCoreFileLineReader;
	}

		protected FwCoreFileLineReaderItf buildFwCoreFileLineReader()
		{
			return new FwCoreFileLineReader();
		}

	private FwCoreSnapshotMetadatasMenuBuilderItf fwCoreSnapshotMetadatasMenuBuilder;

	public FwCoreSnapshotMetadatasMenuBuilderItf getFwCoreSnapshotMetadatasMenuBuilder()
	{
		if (fwCoreSnapshotMetadatasMenuBuilder == null)
		{
			fwCoreSnapshotMetadatasMenuBuilder = buildFwCoreSnapshotMetadatasMenuBuilder();
		}
		return fwCoreSnapshotMetadatasMenuBuilder;
	}

		protected FwCoreSnapshotMetadatasMenuBuilderItf buildFwCoreSnapshotMetadatasMenuBuilder()
		{
			return new FwCoreSnapshotMetadatasMenuBuilder(
				getFwCoreSnapshotMetadatasLister()
			);
		}

	private FwCoreFileBuilderBySnapshotMetadatasMenuQueryItf fwCoreFileBuilderBySnapshotMetadatasMenuQuery;

	public FwCoreFileBuilderBySnapshotMetadatasMenuQueryItf getFwCoreFileBuilderBySnapshotMetadatasMenuQuery()
	{
		if (fwCoreFileBuilderBySnapshotMetadatasMenuQuery == null)
		{
			fwCoreFileBuilderBySnapshotMetadatasMenuQuery = buildFwCoreFileBuilderBySnapshotMetadatasMenuQuery();
		}
		return fwCoreFileBuilderBySnapshotMetadatasMenuQuery;
	}

		protected FwCoreFileBuilderBySnapshotMetadatasMenuQueryItf buildFwCoreFileBuilderBySnapshotMetadatasMenuQuery()
		{
			return new FwCoreFileBuilderBySnapshotMetadatasMenuQuery(
				getFwCoreSnapshotFilesBuilder()
			);
		}

	private FwCoreSnapshotFilesBuilderItf fwCoreSnapshotFilesBuilder;

	public FwCoreSnapshotFilesBuilderItf getFwCoreSnapshotFilesBuilder()
	{
		if (fwCoreSnapshotFilesBuilder == null)
		{
			fwCoreSnapshotFilesBuilder = buildFwCoreSnapshotFilesBuilder();
		}
		return fwCoreSnapshotFilesBuilder;
	}

		protected FwCoreSnapshotFilesBuilderItf buildFwCoreSnapshotFilesBuilder()
		{
			return new FwCoreSnapshotFilesBuilder(
				getFwCoreConfigurationContainer()
			);
		}

	private FwCoreSnapshotDeleterItf fwCoreSnapshotDeleter;

	public FwCoreSnapshotDeleterItf getFwCoreSnapshotDeleter()
	{
		if (fwCoreSnapshotDeleter == null)
		{
			fwCoreSnapshotDeleter = buildFwCoreSnapshotDeleter();
		}
		return fwCoreSnapshotDeleter;
	}

		protected FwCoreSnapshotDeleterItf buildFwCoreSnapshotDeleter()
		{
			return new FwCoreSnapshotDeleter(
				getFwCoreFileBuilderBySnapshotMetadatasMenuQuery()
			);
		}

	private FwCoreSnapshotOverwriterItf fwCoreSnapshotOverwriter;

	public FwCoreSnapshotOverwriterItf getFwCoreSnapshotOverwriter()
	{
		if (fwCoreSnapshotOverwriter == null)
		{
			fwCoreSnapshotOverwriter = buildFwCoreSnapshotOverwriter();
		}
		return fwCoreSnapshotOverwriter;
	}

		protected FwCoreSnapshotOverwriterItf buildFwCoreSnapshotOverwriter()
		{
			return new FwCoreSnapshotOverwriter(
				getFwCoreFileBuilderBySnapshotMetadatasMenuQuery(),
				getFwCoreSnapshotContentBuilderAndToFileWriter()
			);
		}

	private FwCoreSnapshotFileContentBuilderItf fwCoreSnapshotFileContentBuilder;

	public FwCoreSnapshotFileContentBuilderItf getFwCoreSnapshotFileContentBuilder()
	{
		if (fwCoreSnapshotFileContentBuilder == null)
		{
			fwCoreSnapshotFileContentBuilder = buildFwCoreSnapshotFileContentBuilder();
		}
		return fwCoreSnapshotFileContentBuilder;
	}

		protected FwCoreSnapshotFileContentBuilderItf buildFwCoreSnapshotFileContentBuilder()
		{
			return new FwCoreSnapshotFileContentBuilder(
				getFwCoreSnapshotMetadataSerializer(),
				getFwCoreStatusSerializer()
			);
		}

	private FwCoreFileWriterItf fwCoreFileWriter;

	public FwCoreFileWriterItf getFwCoreFileWriter()
	{
		if (fwCoreFileWriter == null)
		{
			fwCoreFileWriter = buildFwCoreFileWriter();
		}
		return fwCoreFileWriter;
	}

		protected FwCoreFileWriterItf buildFwCoreFileWriter()
		{
			return new FwCoreFileWriter();
		}

	private FwCoreSnapshotLoaderItf fwCoreSnapshotLoader;

	public FwCoreSnapshotLoaderItf getFwCoreSnapshotLoader()
	{
		if (fwCoreSnapshotLoader == null)
		{
			fwCoreSnapshotLoader = buildFwCoreSnapshotLoader();
		}
		return fwCoreSnapshotLoader;
	}

		protected FwCoreSnapshotLoaderItf buildFwCoreSnapshotLoader()
		{
			return new FwCoreSnapshotLoader(
				getFwCoreFileBuilderBySnapshotMetadatasMenuQuery(),
				getFwCoreSnapshotFileParser()
			);
		}

	private FwCoreSnapshotContentBuilderAndToFileWriterItf fwCoreSnapshotContentBuilderAndToFileWriter;

	public FwCoreSnapshotContentBuilderAndToFileWriterItf getFwCoreSnapshotContentBuilderAndToFileWriter()
	{
		if (fwCoreSnapshotContentBuilderAndToFileWriter == null)
		{
			fwCoreSnapshotContentBuilderAndToFileWriter = buildFwCoreSnapshotContentBuilderAndToFileWriter();
		}
		return fwCoreSnapshotContentBuilderAndToFileWriter;
	}

		protected FwCoreSnapshotContentBuilderAndToFileWriterItf buildFwCoreSnapshotContentBuilderAndToFileWriter()
		{
			return new FwCoreSnapshotContentBuilderAndToFileWriter(
				getFwCoreSnapshotFileContentBuilder(),
				getFwCoreFileWriter()
			);
		}

	private FwCoreSnapshotFileBuilderItf fwCoreSnapshotFileBuilder;

	public FwCoreSnapshotFileBuilderItf getFwCoreSnapshotFileBuilder()
	{
		if (fwCoreSnapshotFileBuilder == null)
		{
			fwCoreSnapshotFileBuilder = buildFwCoreSnapshotFileBuilder();
		}
		return fwCoreSnapshotFileBuilder;
	}

		protected FwCoreSnapshotFileBuilderItf buildFwCoreSnapshotFileBuilder()
		{
			return new FwCoreSnapshotFileBuilder();
		}

	private FwCoreBodyBuilderBySnapshotLoaderItf fwCoreBodyBuilderBySnapshotLoader;

	public FwCoreBodyBuilderBySnapshotLoaderItf getFwCoreBodyBuilderBySnapshotLoader()
	{
		if (fwCoreBodyBuilderBySnapshotLoader == null)
		{
			fwCoreBodyBuilderBySnapshotLoader = buildFwCoreBodyBuilderBySnapshotLoader();
		}
		return fwCoreBodyBuilderBySnapshotLoader;
	}

		protected FwCoreBodyBuilderBySnapshotLoaderItf buildFwCoreBodyBuilderBySnapshotLoader()
		{
			return new FwCoreBodyBuilderBySnapshotLoader();
		}

	private FwCoreBodyBuilderBySnapshotDeleterItf fwCoreBodyBuilderBySnapshotDeleter;

	public FwCoreBodyBuilderBySnapshotDeleterItf getFwCoreBodyBuilderBySnapshotDeleter()
	{
		if (fwCoreBodyBuilderBySnapshotDeleter == null)
		{
			fwCoreBodyBuilderBySnapshotDeleter = buildFwCoreBodyBuilderBySnapshotDeleter();
		}
		return fwCoreBodyBuilderBySnapshotDeleter;
	}

		protected FwCoreBodyBuilderBySnapshotDeleterItf buildFwCoreBodyBuilderBySnapshotDeleter()
		{
			return new FwCoreBodyBuilderBySnapshotDeleter();
		}

	private FwCoreBodyBuilderBySnapshotOverwriterItf fwCoreBodyBuilderBySnapshotOverwriter;

	public FwCoreBodyBuilderBySnapshotOverwriterItf getFwCoreBodyBuilderBySnapshotOverwriter()
	{
		if (fwCoreBodyBuilderBySnapshotOverwriter == null)
		{
			fwCoreBodyBuilderBySnapshotOverwriter = buildFwCoreBodyBuilderBySnapshotOverwriter();
		}
		return fwCoreBodyBuilderBySnapshotOverwriter;
	}

		protected FwCoreBodyBuilderBySnapshotOverwriterItf buildFwCoreBodyBuilderBySnapshotOverwriter()
		{
			return new FwCoreBodyBuilderBySnapshotOverwriter();
		}

	private FwCoreBodyBuilderBySnapshotCreatorItf fwCoreBodyBuilderBySnapshotCreator;

	public FwCoreBodyBuilderBySnapshotCreatorItf getFwCoreBodyBuilderBySnapshotCreator()
	{
		if (fwCoreBodyBuilderBySnapshotCreator == null)
		{
			fwCoreBodyBuilderBySnapshotCreator = buildFwCoreBodyBuilderBySnapshotCreator();
		}
		return fwCoreBodyBuilderBySnapshotCreator;
	}

		protected FwCoreBodyBuilderBySnapshotCreatorItf buildFwCoreBodyBuilderBySnapshotCreator()
		{
			return new FwCoreBodyBuilderBySnapshotCreator();
		}

// --- FwCli ---

	private FwCliAppRequestBuilderItf fwCliAppRequestBuilder;

	public FwCliAppRequestBuilderItf getFwCliAppRequestBuilder()
	{
		if (fwCliAppRequestBuilder == null)
		{
			fwCliAppRequestBuilder = buildFwCliAppRequestBuilder();
		}
		return fwCliAppRequestBuilder;
	}

		protected FwCliAppRequestBuilderItf buildFwCliAppRequestBuilder()
		{
			return new FwCliAppRequestBuilder(
				getFwCliQueryBuilder()
			);
		}

	private FwCliAppResponseDispatcherItf fwCliAppResponseDispatcher;

	public FwCliAppResponseDispatcherItf getFwCliAppResponseDispatcher()
	{
		if (fwCliAppResponseDispatcher == null)
		{
			fwCliAppResponseDispatcher = buildFwCliAppResponseDispatcher();
		}
		return fwCliAppResponseDispatcher;
	}

		protected FwCliAppResponseDispatcherItf buildFwCliAppResponseDispatcher()
		{
			return new FwCliAppResponseDispatcher(
				getFwCliStylizer(),
				getFwCliPrompter(),
				getFwCliTranslationContainer()
			);
		}

	private FwCliAppRunnerItf fwCliAppRunner;

	public FwCliAppRunnerItf getFwCliAppRunner()
	{
		if (fwCliAppRunner == null)
		{
			fwCliAppRunner = buildFwCliAppRunner();
		}
		return fwCliAppRunner;
	}

		protected FwCliAppRunnerItf buildFwCliAppRunner()
		{
			return new FwCliAppRunner(
				getFwCoreStartingStatusBuilder(),
				getFwCliAppRequestBuilder(),
				getFwCoreApp(),
				getFwCliAppResponseDispatcher()
			);
		}

	private FwCliPrompterItf fwCliPrompter;

	public FwCliPrompterItf getFwCliPrompter()
	{
		if (fwCliPrompter == null)
		{
			fwCliPrompter = buildFwCliPrompter();
		}
		return fwCliPrompter;
	}

		protected FwCliPrompterItf buildFwCliPrompter()
		{
			return new FwCliPrompter();
		}

	private FwCliQueryBuilderItf fwCliQueryBuilder;

	public FwCliQueryBuilderItf getFwCliQueryBuilder()
	{
		if (fwCliQueryBuilder == null)
		{
			fwCliQueryBuilder = buildFwCliQueryBuilder();
		}
		return fwCliQueryBuilder;
	}

		protected FwCliQueryBuilderItf buildFwCliQueryBuilder()
		{
			return new FwCliQueryBuilder(
				getFwCliPrompter(),
				getFwCliQueryNormalizerContainer(),
				getFwCliTranslationContainer()
			);
		}

	private FwCliQueryNormalizerContainerItf fwCliQueryNormalizerContainer;

	public FwCliQueryNormalizerContainerItf getFwCliQueryNormalizerContainer()
	{
		if (fwCliQueryNormalizerContainer == null)
		{
			fwCliQueryNormalizerContainer = buildFwCliQueryNormalizerContainer();
		}
		return fwCliQueryNormalizerContainer;
	}

		protected FwCliQueryNormalizerContainerItf buildFwCliQueryNormalizerContainer()
		{
			return new FwCliQueryNormalizerContainer();
		}

	private FwCliStylizerItf fwCliStylizer;

	public FwCliStylizerItf getFwCliStylizer()
	{
		if (fwCliStylizer == null)
		{
			fwCliStylizer = buildFwCliStylizer();
		}
		return fwCliStylizer;
	}

		protected FwCliStylizerItf buildFwCliStylizer()
		{
			return new FwCliStylizer();
		}

	private FwCliTranslationContainerItf fwCliTranslationContainer;

	public FwCliTranslationContainerItf getFwCliTranslationContainer()
	{
		if (fwCliTranslationContainer == null)
		{
			fwCliTranslationContainer = buildFwCliTranslationContainer();
		}
		return fwCliTranslationContainer;
	}

		protected FwCliTranslationContainerItf buildFwCliTranslationContainer()
		{
			return new FwCliTranslationContainer(){
				
			};
		}

// --- DemoCore ---

	private DemoCoreNodeBuilderItf demoCoreNodeBuilder;

	public DemoCoreNodeBuilderItf getDemoCoreNodeBuilder()
	{
		if (demoCoreNodeBuilder == null)
		{
			demoCoreNodeBuilder = buildDemoCoreNodeBuilder();
		}
		return demoCoreNodeBuilder;
	}

		protected DemoCoreNodeBuilderItf buildDemoCoreNodeBuilder()
		{
			return new DemoCoreNodeBuilder(
				this
			);
		}

	private DemoCoreRouterItf demoCoreRouter;

	public DemoCoreRouterItf getDemoCoreRouter()
	{
		if (demoCoreRouter == null)
		{
			demoCoreRouter = buildDemoCoreRouter();
		}
		return demoCoreRouter;
	}

		protected DemoCoreRouterItf buildDemoCoreRouter()
		{
			return new DemoCoreRouter(
				getDemoCoreNodeBuilder()
			);
		}

	private DemoCoreTranslationContainerItf demoCoreTranslationContainer;

	public DemoCoreTranslationContainerItf getDemoCoreTranslationContainer()
	{
		if (demoCoreTranslationContainer == null)
		{
			demoCoreTranslationContainer = buildDemoCoreTranslationContainer();
		}
		return demoCoreTranslationContainer;
	}

		protected DemoCoreTranslationContainerItf buildDemoCoreTranslationContainer()
		{
			return new DemoCoreTranslationContainer();
		}

	private DemoCoreEnabledLanguagesContainerItf demoCoreEnabledLanguagesContainer;

	public DemoCoreEnabledLanguagesContainerItf getDemoCoreEnabledLanguagesContainer()
	{
		if (demoCoreEnabledLanguagesContainer == null)
		{
			demoCoreEnabledLanguagesContainer = buildDemoCoreEnabledLanguagesContainer();
		}
		return demoCoreEnabledLanguagesContainer;
	}

		protected DemoCoreEnabledLanguagesContainerItf buildDemoCoreEnabledLanguagesContainer()
		{
			return new DemoCoreEnabledLanguagesContainer();
		}


// --- DemoCli ---

}
