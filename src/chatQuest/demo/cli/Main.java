package chatQuest.demo.cli;

import chatQuest.demo.cli.main.library.serviceContainer.DemoCliServiceContainer;
import chatQuest.demo.cli.main.library.serviceContainer.DemoCliServiceContainerItf;
import chatQuest.fw.cli.main.service.appRunner.FwCliAppRunnerItf;

public final class Main
{

	public static void main(String[] args)
	{
		DemoCliServiceContainerItf serviceContainer = new DemoCliServiceContainer();
		FwCliAppRunnerItf appRunner = serviceContainer.getFwCliAppRunner();
		appRunner.run();
	}

}
