package chatQuest.demo.core.node;

import chatQuest.demo.core.main.library.DemoCoreNodeId;
import chatQuest.demo.core.main.library.node.DemoCoreNodeAbs;
import chatQuest.demo.core.translation.library.translation.DemoCoreTranslationItf;
import chatQuest.demo.core.translation.service.translationContainer.DemoCoreTranslationContainerItf;
import chatQuest.fw.core.main.library.nodeResponse.FwCoreNodeResponseItf;
import chatQuest.fw.core.main.library.slide.FwCoreSlide;
import chatQuest.fw.core.main.library.slide.FwCoreSlideItf;
import chatQuest.fw.core.main.service.nodeBrowser.FwCoreNodeBrowserItf;
import chatQuest.fw.core.main.service.nodeResponseByNodeRequestBuilder.FwCoreNodeResponseByNodeRequestBuilderItf;

public final class DemoCoreBedroomNode
	extends
		DemoCoreNodeAbs
{

	public DemoCoreBedroomNode(
		FwCoreNodeBrowserItf nodeBrowser,
		FwCoreNodeResponseByNodeRequestBuilderItf nodeResponseByNodeRequestBuilder,
		DemoCoreTranslationContainerItf translationContainer
	)
	{
		super(
			nodeBrowser,
			nodeResponseByNodeRequestBuilder,
			translationContainer
		);
	}

	public void tryToSetupNodeResponseWithoutQuery()
	{
		DemoCoreTranslationItf translation = getTranslation();
		FwCoreNodeResponseItf nodeResponse = buildNodeResponse();

		FwCoreSlideItf slide = new FwCoreSlide();

		slide.setTitle(
			translation.demoCore_bedroom_title()
		);

		slide.setBody(
			translation.demoCore_bedroom_body_look()
		);

		nodeResponse.getSlides().add(slide);

		setNodeResponse(nodeResponse);
	}

	protected void tryToSetupNodeResponseWithQuery(String query)
	{
		DemoCoreTranslationItf translation = getTranslation();

		if (matches(query, translation.demoCore_shared_query_look()))
		{
			FwCoreNodeResponseItf nodeResponse = buildNodeResponse();

			FwCoreSlideItf slide = new FwCoreSlide();
			slide.setBody(
				translation.demoCore_bedroom_body_look()
			);
			nodeResponse.getSlides().add(slide);

			setNodeResponse(nodeResponse);
			return;
		}

		if (matches(query, translation.demoCore_bedroom_query_go_corridor()))
		{
			FwCoreNodeResponseItf nodeResponse = buildNodeResponse();

			nodeResponse = nodeChange(
				nodeResponse,
				DemoCoreNodeId.CORRIDOR
			);

			setNodeResponse(nodeResponse);
			return;
		}

	}

}
