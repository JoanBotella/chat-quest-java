package chatQuest.demo.core.main.library.node;

import chatQuest.demo.core.translation.library.translation.DemoCoreTranslationItf;
import chatQuest.demo.core.translation.service.translationContainer.DemoCoreTranslationContainerItf;
import chatQuest.fw.core.main.library.node.FwCoreNodeAbs;
import chatQuest.fw.core.main.service.nodeBrowser.FwCoreNodeBrowserItf;
import chatQuest.fw.core.main.service.nodeResponseByNodeRequestBuilder.FwCoreNodeResponseByNodeRequestBuilderItf;

public abstract class DemoCoreNodeAbs
	extends
		FwCoreNodeAbs
	implements
		DemoCoreNodeItf
{

	private DemoCoreTranslationContainerItf translationContainer;

	public DemoCoreNodeAbs(
		FwCoreNodeBrowserItf nodeBrowser,
		FwCoreNodeResponseByNodeRequestBuilderItf nodeResponseByNodeRequestBuilder,
		DemoCoreTranslationContainerItf translationContainer
	)
	{
		super(
			nodeBrowser,
			nodeResponseByNodeRequestBuilder
		);
		this.translationContainer = translationContainer;
	}

	protected DemoCoreTranslationItf getTranslation()
	{
		return translationContainer.get(
			getNodeRequest()
			.getStatus()
			.getSettings()
			.getLanguageCode()
		);
	}

}
