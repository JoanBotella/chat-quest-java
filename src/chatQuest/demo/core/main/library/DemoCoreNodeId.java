package chatQuest.demo.core.main.library;

public final class DemoCoreNodeId
{

	public static final String CREATE_SNAPSHOT_ASK_NAME = "createSnapshotAskName";
	public static final String CREATE_SNAPSHOT_PROCESS = "createSnapshotProcess";
	public static final String DELETE_SNAPSHOT_MENU = "deleteSnapshotMenu";
	public static final String EXIT_MENU = "exitMenu";
	public static final String LANGUAGE_MENU = "languageMenu";
	public static final String LOAD_SNAPSHOT_MENU = "loadSnapshotMenu";
	public static final String MAIN_MENU = "mainMenu";
	public static final String OVERWRITE_SNAPSHOT_ASK_NAME = "overwriteSnapshotAskName";
	public static final String OVERWRITE_SNAPSHOT_MENU = "overwriteSnapshotMenu";
	public static final String OVERWRITE_SNAPSHOT_PROCESS = "overwriteSnapshotProcess";
	public static final String SETTINGS_MENU = "settingsMenu";
	public static final String SNAPSHOTS_MENU = "snapshotsMenu";
	public static final String TITLE_MENU = "titleMenu";

	// Demo specific nodes

	public static final String BEDROOM = "bedroom";
	public static final String CORRIDOR = "corridor";

}