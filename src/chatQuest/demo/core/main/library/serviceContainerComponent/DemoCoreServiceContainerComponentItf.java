package chatQuest.demo.core.main.library.serviceContainerComponent;

import chatQuest.demo.core.main.service.enabledLanguagesContainer.DemoCoreEnabledLanguagesContainerItf;
import chatQuest.demo.core.main.service.nodeBuilder.DemoCoreNodeBuilderItf;
import chatQuest.demo.core.main.service.router.DemoCoreRouterItf;
import chatQuest.demo.core.translation.service.translationContainer.DemoCoreTranslationContainerItf;
import chatQuest.fw.core.main.library.serviceContainerComponent.FwCoreServiceContainerComponentItf;

public interface DemoCoreServiceContainerComponentItf
	extends
		FwCoreServiceContainerComponentItf
{

	public DemoCoreNodeBuilderItf getDemoCoreNodeBuilder();

	public DemoCoreRouterItf getDemoCoreRouter();

	public DemoCoreTranslationContainerItf getDemoCoreTranslationContainer();

	public DemoCoreEnabledLanguagesContainerItf getDemoCoreEnabledLanguagesContainer();

}
