package chatQuest.demo.core.main.service.enabledLanguagesContainer;

import java.util.List;

import chatQuest.fw.core.main.library.enabledLanguagesContainer.FwCoreEnabledLanguagesContainerAbs;
import chatQuest.fw.core.main.library.language.FwCoreLanguage;
import chatQuest.fw.core.main.library.language.FwCoreLanguageCode;
import chatQuest.fw.core.main.library.language.FwCoreLanguageItf;

public final class DemoCoreEnabledLanguagesContainer
	extends
		FwCoreEnabledLanguagesContainerAbs
	implements
		DemoCoreEnabledLanguagesContainerItf
{

	protected List<FwCoreLanguageItf> addLanguages(List<FwCoreLanguageItf> built)
	{
		built.add(
			new FwCoreLanguage(
				"English",
				FwCoreLanguageCode.EN
			)
		);

		built.add(
			new FwCoreLanguage(
				"Spanish - Castellano",
				FwCoreLanguageCode.ES
			)
		);

		return built;
	}

}
