package chatQuest.demo.core.main.service.nodeBuilder;

import chatQuest.demo.core.main.library.node.DemoCoreNodeItf;
import chatQuest.fw.core.main.library.node.FwCoreTranslatableNodeItf;

public interface DemoCoreNodeBuilderItf
{

	public FwCoreTranslatableNodeItf createSnapshotAskName();

	public FwCoreTranslatableNodeItf createSnapshotProcess();

	public FwCoreTranslatableNodeItf deleteSnapshotMenu();

	public FwCoreTranslatableNodeItf exitMenu();

	public FwCoreTranslatableNodeItf languageMenu();

	public FwCoreTranslatableNodeItf loadSnapshotMenu();

	public FwCoreTranslatableNodeItf mainMenu();

	public FwCoreTranslatableNodeItf overwriteSnapshotAskName();

	public FwCoreTranslatableNodeItf overwriteSnapshotMenu();

	public FwCoreTranslatableNodeItf overwriteSnapshotProcess();

	public FwCoreTranslatableNodeItf settingsMenu();

	public FwCoreTranslatableNodeItf snapshotsMenu();

	public FwCoreTranslatableNodeItf system();

	public FwCoreTranslatableNodeItf titleMenu();

	// Demo specific nodes

	public DemoCoreNodeItf bedroom();

	public DemoCoreNodeItf corridor();

}
