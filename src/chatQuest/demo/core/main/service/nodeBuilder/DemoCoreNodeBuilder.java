package chatQuest.demo.core.main.service.nodeBuilder;

import chatQuest.demo.core.main.library.DemoCoreNodeId;
import chatQuest.demo.core.main.library.node.DemoCoreNodeItf;
import chatQuest.demo.core.main.library.serviceContainerComponent.DemoCoreServiceContainerComponentItf;
import chatQuest.demo.core.node.DemoCoreBedroomNode;
import chatQuest.demo.core.node.DemoCoreCorridorNode;
import chatQuest.fw.core.main.library.node.FwCoreTranslatableNodeItf;
import chatQuest.fw.core.node.FwCoreSettingsMenuNode;
import chatQuest.fw.core.node.FwCoreSnapshotsMenuNode;
import chatQuest.fw.core.node.FwCoreCreateSnapshotAskNameNode;
import chatQuest.fw.core.node.FwCoreCreateSnapshotProcessNode;
import chatQuest.fw.core.node.FwCoreDeleteSnapshotMenuNode;
import chatQuest.fw.core.node.FwCoreExitMenuNode;
import chatQuest.fw.core.node.FwCoreLanguageMenuNode;
import chatQuest.fw.core.node.FwCoreLoadSnapshotMenuNode;
import chatQuest.fw.core.node.FwCoreMainMenuNode;
import chatQuest.fw.core.node.FwCoreOverwriteSnapshotAskNameNode;
import chatQuest.fw.core.node.FwCoreOverwriteSnapshotMenuNode;
import chatQuest.fw.core.node.FwCoreOverwriteSnapshotProcessNode;
import chatQuest.fw.core.node.FwCoreSystemNode;
import chatQuest.fw.core.node.FwCoreTitleMenuNode;

public final class DemoCoreNodeBuilder
	implements
		DemoCoreNodeBuilderItf
{

	private DemoCoreServiceContainerComponentItf serviceContainer;

	public DemoCoreNodeBuilder(
		DemoCoreServiceContainerComponentItf serviceContainer
	)
	{
		this.serviceContainer = serviceContainer;
	}

	public FwCoreTranslatableNodeItf createSnapshotAskName()
	{
		return new FwCoreCreateSnapshotAskNameNode(
			serviceContainer.getFwCoreNodeBrowser(),
			serviceContainer.getFwCoreNodeResponseByNodeRequestBuilder(),
			serviceContainer.getFwCoreTranslationContainer(),
			DemoCoreNodeId.CREATE_SNAPSHOT_PROCESS
		);
	}

	public FwCoreTranslatableNodeItf createSnapshotProcess()
	{
		return new FwCoreCreateSnapshotProcessNode(
			serviceContainer.getFwCoreNodeBrowser(),
			serviceContainer.getFwCoreNodeResponseByNodeRequestBuilder(),
			serviceContainer.getFwCoreTranslationContainer(),
			serviceContainer.getFwCoreSnapshotCreator(),
			serviceContainer.getFwCoreBodyBuilderBySnapshotCreator()
		);
	}

	public FwCoreTranslatableNodeItf deleteSnapshotMenu()
	{
		return new FwCoreDeleteSnapshotMenuNode(
			serviceContainer.getFwCoreNodeBrowser(),
			serviceContainer.getFwCoreNodeResponseByNodeRequestBuilder(),
			serviceContainer.getFwCoreTranslationContainer(),
			serviceContainer.getFwCoreSnapshotMetadatasMenuBuilder(),
			serviceContainer.getFwCoreSnapshotDeleter(),
			serviceContainer.getFwCoreBodyBuilderBySnapshotDeleter()
		);
	}

	public FwCoreTranslatableNodeItf exitMenu()
	{
		return new FwCoreExitMenuNode(
			serviceContainer.getFwCoreNodeBrowser(),
			serviceContainer.getFwCoreNodeResponseByNodeRequestBuilder(),
			serviceContainer.getFwCoreTranslationContainer()
		);
	}

	public FwCoreTranslatableNodeItf languageMenu()
	{
		return new FwCoreLanguageMenuNode(
			serviceContainer.getFwCoreNodeBrowser(),
			serviceContainer.getFwCoreNodeResponseByNodeRequestBuilder(),
			serviceContainer.getFwCoreTranslationContainer(),
			serviceContainer.getDemoCoreEnabledLanguagesContainer().get()
		);
	}

	public FwCoreTranslatableNodeItf loadSnapshotMenu()
	{
		return new FwCoreLoadSnapshotMenuNode(
			serviceContainer.getFwCoreNodeBrowser(),
			serviceContainer.getFwCoreNodeResponseByNodeRequestBuilder(),
			serviceContainer.getFwCoreTranslationContainer(),
			serviceContainer.getFwCoreSnapshotMetadatasMenuBuilder(),
			serviceContainer.getFwCoreSnapshotLoader(),
			serviceContainer.getFwCoreBodyBuilderBySnapshotLoader()
		);
	}

	public FwCoreTranslatableNodeItf mainMenu()
	{
		return new FwCoreMainMenuNode(
			serviceContainer.getFwCoreNodeBrowser(),
			serviceContainer.getFwCoreNodeResponseByNodeRequestBuilder(),
			serviceContainer.getFwCoreTranslationContainer(),
			DemoCoreNodeId.SNAPSHOTS_MENU,
			DemoCoreNodeId.SETTINGS_MENU,
			DemoCoreNodeId.EXIT_MENU
		);
	}

	public FwCoreTranslatableNodeItf overwriteSnapshotAskName()
	{
		return new FwCoreOverwriteSnapshotAskNameNode(
			serviceContainer.getFwCoreNodeBrowser(),
			serviceContainer.getFwCoreNodeResponseByNodeRequestBuilder(),
			serviceContainer.getFwCoreTranslationContainer(),
			DemoCoreNodeId.OVERWRITE_SNAPSHOT_PROCESS
		);
	}
	public FwCoreTranslatableNodeItf overwriteSnapshotMenu()
	{
		return new FwCoreOverwriteSnapshotMenuNode(
			serviceContainer.getFwCoreNodeBrowser(),
			serviceContainer.getFwCoreNodeResponseByNodeRequestBuilder(),
			serviceContainer.getFwCoreTranslationContainer(),
			serviceContainer.getFwCoreSnapshotMetadatasMenuBuilder(),
			DemoCoreNodeId.OVERWRITE_SNAPSHOT_ASK_NAME
		);
	}

	public FwCoreTranslatableNodeItf overwriteSnapshotProcess()
	{
		return new FwCoreOverwriteSnapshotProcessNode(
			serviceContainer.getFwCoreNodeBrowser(),
			serviceContainer.getFwCoreNodeResponseByNodeRequestBuilder(),
			serviceContainer.getFwCoreTranslationContainer(),
			serviceContainer.getFwCoreSnapshotOverwriter(),
			serviceContainer.getFwCoreBodyBuilderBySnapshotOverwriter()
		);
	}

	public FwCoreTranslatableNodeItf settingsMenu()
	{
		return new FwCoreSettingsMenuNode(
			serviceContainer.getFwCoreNodeBrowser(),
			serviceContainer.getFwCoreNodeResponseByNodeRequestBuilder(),
			serviceContainer.getFwCoreTranslationContainer(),
			DemoCoreNodeId.LANGUAGE_MENU
		);
	}

	public FwCoreTranslatableNodeItf snapshotsMenu()
	{
		return new FwCoreSnapshotsMenuNode(
			serviceContainer.getFwCoreNodeBrowser(),
			serviceContainer.getFwCoreNodeResponseByNodeRequestBuilder(),
			serviceContainer.getFwCoreTranslationContainer(),
			DemoCoreNodeId.LOAD_SNAPSHOT_MENU,
			DemoCoreNodeId.CREATE_SNAPSHOT_ASK_NAME,
			DemoCoreNodeId.OVERWRITE_SNAPSHOT_MENU,
			DemoCoreNodeId.DELETE_SNAPSHOT_MENU
		);
	}

	private FwCoreTranslatableNodeItf fwCoreSystemNode;

	public FwCoreTranslatableNodeItf system()
	{
		if (fwCoreSystemNode == null)
		{
			fwCoreSystemNode = buildFwCoreSystemNode();
		}
		return fwCoreSystemNode;
	}

		protected FwCoreTranslatableNodeItf buildFwCoreSystemNode()
		{
			return new FwCoreSystemNode(
				serviceContainer.getFwCoreNodeBrowser(),
				serviceContainer.getFwCoreNodeResponseByNodeRequestBuilder(),
				serviceContainer.getFwCoreTranslationContainer(),
				DemoCoreNodeId.SETTINGS_MENU,
				DemoCoreNodeId.EXIT_MENU,
				serviceContainer.getFwCoreStatusSerializer(),
				DemoCoreNodeId.SNAPSHOTS_MENU,
				DemoCoreNodeId.MAIN_MENU
			);
		}

	public FwCoreTranslatableNodeItf titleMenu()
	{
		return new FwCoreTitleMenuNode(
			serviceContainer.getFwCoreNodeBrowser(),
			serviceContainer.getFwCoreNodeResponseByNodeRequestBuilder(),
			serviceContainer.getFwCoreTranslationContainer(),
			serviceContainer.getFwCoreSnapshotMetadatasLister(),
			serviceContainer.getFwCoreSnapshotLoader(),
			serviceContainer.getFwCoreBodyBuilderBySnapshotLoader(),
			DemoCoreNodeId.BEDROOM,
			DemoCoreNodeId.SETTINGS_MENU,
			DemoCoreNodeId.EXIT_MENU,
			DemoCoreNodeId.LOAD_SNAPSHOT_MENU
		);
	}

	// Demo specific nodes

	public DemoCoreNodeItf bedroom()
	{
		return new DemoCoreBedroomNode(
			serviceContainer.getFwCoreNodeBrowser(),
			serviceContainer.getFwCoreNodeResponseByNodeRequestBuilder(),
			serviceContainer.getDemoCoreTranslationContainer()
		);
	}

	public DemoCoreNodeItf corridor()
	{
		return new DemoCoreCorridorNode(
			serviceContainer.getFwCoreNodeBrowser(),
			serviceContainer.getFwCoreNodeResponseByNodeRequestBuilder(),
			serviceContainer.getDemoCoreTranslationContainer()
		);
	}

}
