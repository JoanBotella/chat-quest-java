package chatQuest.demo.core.main.service.router;

import chatQuest.demo.core.main.library.DemoCoreNodeId;
import chatQuest.demo.core.main.service.nodeBuilder.DemoCoreNodeBuilderItf;
import chatQuest.fw.core.main.library.node.FwCoreNodeItf;
import chatQuest.fw.core.main.library.router.FwCoreRouterAbs;

public final class DemoCoreRouter
	extends
		FwCoreRouterAbs
	implements
		DemoCoreRouterItf
{

	private DemoCoreNodeBuilderItf nodeBuilder;

	public DemoCoreRouter(
		DemoCoreNodeBuilderItf nodeBuilder
	)
	{
		super();
		this.nodeBuilder = nodeBuilder;
	}

	protected void setupNode(String nodeId)
	{
		FwCoreNodeItf node;

		switch (nodeId)
		{
			case DemoCoreNodeId.CREATE_SNAPSHOT_ASK_NAME:
				node = nodeBuilder.createSnapshotAskName();
				break;
			case DemoCoreNodeId.CREATE_SNAPSHOT_PROCESS:
				node = nodeBuilder.createSnapshotProcess();
				break;
			case DemoCoreNodeId.DELETE_SNAPSHOT_MENU:
				node = nodeBuilder.deleteSnapshotMenu();
				break;
			case DemoCoreNodeId.EXIT_MENU:
				node = nodeBuilder.exitMenu();
				break;
			case DemoCoreNodeId.LANGUAGE_MENU:
				node = nodeBuilder.languageMenu();
				break;
			case DemoCoreNodeId.LOAD_SNAPSHOT_MENU:
				node = nodeBuilder.loadSnapshotMenu();
				break;
			case DemoCoreNodeId.MAIN_MENU:
				node = nodeBuilder.mainMenu();
				break;
			case DemoCoreNodeId.OVERWRITE_SNAPSHOT_ASK_NAME:
				node = nodeBuilder.overwriteSnapshotAskName();
				break;
			case DemoCoreNodeId.OVERWRITE_SNAPSHOT_MENU:
				node = nodeBuilder.overwriteSnapshotMenu();
				break;
			case DemoCoreNodeId.OVERWRITE_SNAPSHOT_PROCESS:
				node = nodeBuilder.overwriteSnapshotProcess();
				break;
			case DemoCoreNodeId.SETTINGS_MENU:
				node = nodeBuilder.settingsMenu();
				break;
			case DemoCoreNodeId.SNAPSHOTS_MENU:
				node = nodeBuilder.snapshotsMenu();
				break;
			case DemoCoreNodeId.TITLE_MENU:
				node = nodeBuilder.titleMenu();
				break;

			// Demo specific nodes

			case DemoCoreNodeId.BEDROOM:
				node = nodeBuilder.bedroom();
				break;
			case DemoCoreNodeId.CORRIDOR:
				node = nodeBuilder.corridor();
				break;
			default:
				return;
		}

		setNode(node);
	}

}
