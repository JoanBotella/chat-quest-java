package chatQuest.demo.core.translation.service.translationContainer;

import chatQuest.demo.core.translation.library.translation.DemoCoreEnTranslation;
import chatQuest.demo.core.translation.library.translation.DemoCoreEsTranslation;
import chatQuest.demo.core.translation.library.translation.DemoCoreTranslationItf;
import chatQuest.fw.core.main.library.language.FwCoreLanguageCode;
import chatQuest.fw.core.translation.library.translationContainer.FwCoreBaseTranslationContainerAbs;

public final class DemoCoreTranslationContainer
	extends
		FwCoreBaseTranslationContainerAbs<
			DemoCoreTranslationItf
		>
	implements
		DemoCoreTranslationContainerItf
{

	protected void setupTranslation(String languageCode)
	{
		DemoCoreTranslationItf translation;

		switch (languageCode)
		{
			case FwCoreLanguageCode.EN:
				translation = buildEnTranslation();
				break;
			case FwCoreLanguageCode.ES:
				translation = buildEsTranslation();
				break;
			default:
				return;
		}

		setTranslation(translation);
	}

		private DemoCoreTranslationItf buildEnTranslation()
		{
			return new DemoCoreEnTranslation();
		}

		private DemoCoreTranslationItf buildEsTranslation()
		{
			return new DemoCoreEsTranslation();
		}

}
