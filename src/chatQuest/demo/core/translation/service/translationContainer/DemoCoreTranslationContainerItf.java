package chatQuest.demo.core.translation.service.translationContainer;

import chatQuest.demo.core.translation.library.translation.DemoCoreTranslationItf;
import chatQuest.fw.core.translation.library.translationContainer.FwCoreBaseTranslationContainerItf;

public interface DemoCoreTranslationContainerItf
	extends
		FwCoreBaseTranslationContainerItf<
			DemoCoreTranslationItf
		>
{

}
