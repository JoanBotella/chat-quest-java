package chatQuest.demo.core.translation.library.translation;

public final class DemoCoreEsTranslation
	extends
		DemoCoreTranslationAbs
{

	public String demoCore_bedroom_body_look() { return "<p>Éste es mi dormitorio.</p><p>Puedo ir al <span class=\"exit\">pasillo</span>.</p>"; }
	public String demoCore_bedroom_title() { return "Dormitorio"; }
	public String demoCore_bedroom_query_go_corridor() { return "ir pasillo"; }
	public String demoCore_corridor_body_look() { return "<p>Éste es el pasillo.</p><p>Puedo ir al <span class=\"exit\">dormitorio</span>.</p>"; }
	public String demoCore_corridor_title() { return "Pasillo"; }
	public String demoCore_corridor_query_go_bedroom() { return "ir dormitorio"; }
	public String demoCore_shared_query_look() { return "mirar"; }

}
