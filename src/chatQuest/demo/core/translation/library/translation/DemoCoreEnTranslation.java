package chatQuest.demo.core.translation.library.translation;

public final class DemoCoreEnTranslation
	extends
		DemoCoreTranslationAbs
{

	public String demoCore_bedroom_body_look() { return "<p>This is my room.</p><p>I can go to the <span class=\"exit\">corridor</span>.</p>"; }
	public String demoCore_bedroom_title() { return "Bedroom"; }
	public String demoCore_bedroom_query_go_corridor() { return "go corridor"; }
	public String demoCore_corridor_body_look() { return "<p>This is the corridor.</p><p>I can go to the <span class=\"exit\">bedroom</span>.</p>"; }
	public String demoCore_corridor_title() { return "Corridor"; }
	public String demoCore_corridor_query_go_bedroom() { return "go bedroom"; }
	public String demoCore_shared_query_look() { return "look"; }

}
