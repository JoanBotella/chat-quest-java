package chatQuest.demo.core.translation.library.translation;

public interface DemoCoreTranslationItf
{

	public String demoCore_bedroom_body_look();
	public String demoCore_bedroom_title();
	public String demoCore_bedroom_query_go_corridor();
	public String demoCore_corridor_body_look();
	public String demoCore_corridor_title();
	public String demoCore_corridor_query_go_bedroom();
	public String demoCore_shared_query_look();

}
