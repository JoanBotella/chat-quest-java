package chatQuest.fw.core.node;

import chatQuest.fw.core.main.library.VariableConstant;
import chatQuest.fw.core.main.library.node.FwCoreTranslatableNodeAbs;
import chatQuest.fw.core.main.library.nodeResponse.FwCoreNodeResponseItf;
import chatQuest.fw.core.main.library.slide.FwCoreSlide;
import chatQuest.fw.core.main.library.slide.FwCoreSlideItf;
import chatQuest.fw.core.main.service.bodyBuilderBySnapshotCreator.FwCoreBodyBuilderBySnapshotCreatorItf;
import chatQuest.fw.core.main.service.nodeBrowser.FwCoreNodeBrowserItf;
import chatQuest.fw.core.main.service.nodeResponseByNodeRequestBuilder.FwCoreNodeResponseByNodeRequestBuilderItf;
import chatQuest.fw.core.main.service.snapshotCreator.FwCoreSnapshotCreatorItf;
import chatQuest.fw.core.translation.service.translationContainer.FwCoreTranslationContainerItf;

public final class
	FwCoreCreateSnapshotProcessNode
extends
	FwCoreTranslatableNodeAbs
{

	private FwCoreSnapshotCreatorItf snapshotCreator;
	private FwCoreBodyBuilderBySnapshotCreatorItf bodyBuilderBySnapshotCreator;

	public FwCoreCreateSnapshotProcessNode(
		FwCoreNodeBrowserItf nodeBrowser,
		FwCoreNodeResponseByNodeRequestBuilderItf nodeResponseByNodeRequestBuilder,
		FwCoreTranslationContainerItf translationContainer,
		FwCoreSnapshotCreatorItf snapshotCreator,
		FwCoreBodyBuilderBySnapshotCreatorItf bodyBuilderBySnapshotCreator
	)
	{
		super(
			nodeBrowser,
			nodeResponseByNodeRequestBuilder,
			translationContainer
		);
		this.snapshotCreator = snapshotCreator;
		this.bodyBuilderBySnapshotCreator = bodyBuilderBySnapshotCreator;
	}

	public void tryToSetupNodeResponseWithoutQuery()
	{
		FwCoreNodeResponseItf nodeResponse = buildNodeResponse();

		FwCoreSlideItf slide = new FwCoreSlide();

		String body = processAndBuildBody(nodeResponse);

		slide.setBody(body);

		nodeResponse.getSlides().add(slide);

		nodeResponse = nodePop(nodeResponse);

		setNodeResponse(nodeResponse);
	}

		private String processAndBuildBody(FwCoreNodeResponseItf nodeResponse)
		{
			String variableNameKey = VariableConstant.SNAPSHOT_NAME_KEY;

			String snapshotName = getVariable(
				nodeResponse,
				variableNameKey
			);
			nodeResponse = removeVariable(
				nodeResponse,
				variableNameKey
			);
	
			snapshotCreator.create(
				snapshotName,
				nodeResponse.getStatus()
			);

			return bodyBuilderBySnapshotCreator.build(
				snapshotCreator,
				getTranslation()
			);
		}

}