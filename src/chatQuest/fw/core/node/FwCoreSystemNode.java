package chatQuest.fw.core.node;

import chatQuest.fw.core.main.library.node.FwCoreTranslatableNodeAbs;
import chatQuest.fw.core.main.library.nodeResponse.FwCoreNodeResponseItf;
import chatQuest.fw.core.main.library.slide.FwCoreSlide;
import chatQuest.fw.core.main.library.slide.FwCoreSlideItf;
import chatQuest.fw.core.translation.library.translation.FwCoreTranslationItf;
import chatQuest.fw.core.main.service.nodeBrowser.FwCoreNodeBrowserItf;
import chatQuest.fw.core.main.service.nodeResponseByNodeRequestBuilder.FwCoreNodeResponseByNodeRequestBuilderItf;
import chatQuest.fw.core.main.service.statusSerializer.FwCoreStatusSerializerItf;
import chatQuest.fw.core.translation.service.translationContainer.FwCoreTranslationContainerItf;

public final class FwCoreSystemNode
	extends
		FwCoreTranslatableNodeAbs
{

	private String settingsMenuNodeId;
	private String exitMenuNodeId;
	private FwCoreStatusSerializerItf statusSerializer;
	private String snapshotsMenuNodeId;
	private String mainMenuNodeId;

	public FwCoreSystemNode(
		FwCoreNodeBrowserItf nodeBrowser,
		FwCoreNodeResponseByNodeRequestBuilderItf nodeResponseByNodeRequestBuilder,
		FwCoreTranslationContainerItf translationContainer,
		String settingsMenuNodeId,
		String exitMenuNodeId,
		FwCoreStatusSerializerItf statusSerializer,
		String snapshotsMenuNodeId,
		String mainMenuNodeId
	)
	{
		super(
			nodeBrowser,
			nodeResponseByNodeRequestBuilder,
			translationContainer
		);
		this.settingsMenuNodeId = settingsMenuNodeId;
		this.exitMenuNodeId = exitMenuNodeId;
		this.statusSerializer = statusSerializer;
		this.snapshotsMenuNodeId = snapshotsMenuNodeId;
		this.mainMenuNodeId = mainMenuNodeId;
	}

	protected void tryToSetupNodeResponseWithQuery(String query)
	{
		FwCoreTranslationItf translation = getTranslation();

		if (matches(query, translation.fwCore_system_query_debugGetStatus()))
		{
			FwCoreNodeResponseItf nodeResponse = buildNodeResponse();

			FwCoreSlideItf slide = new FwCoreSlide();
			String statusString = statusSerializer.serialize(
				getNodeRequest().getStatus()
			);
			slide.setBody(statusString);
			nodeResponse.getSlides().add(slide);
	
			setNodeResponse(nodeResponse);
			return;
		}

		if (matches(query, translation.fwCore_system_query_mainMenu()))
		{
			FwCoreNodeResponseItf nodeResponse = buildNodeResponse();

			nodeResponse = nodePush(
				nodeResponse,
				mainMenuNodeId
			);

			setNodeResponse(nodeResponse);
			return;
		}

		if (matches(query, translation.fwCore_system_query_settingsMenu()))
		{
			FwCoreNodeResponseItf nodeResponse = buildNodeResponse();

			nodeResponse = nodePush(
				nodeResponse,
				settingsMenuNodeId
			);

			setNodeResponse(nodeResponse);
			return;
		}

		if (matches(query, translation.fwCore_system_query_snapshotsMenu()))
		{
			FwCoreNodeResponseItf nodeResponse = buildNodeResponse();

			nodeResponse = nodePush(
				nodeResponse,
				snapshotsMenuNodeId
			);

			setNodeResponse(nodeResponse);
			return;
		}

		if (matches(query, translation.fwCore_system_query_exit()))
		{
			FwCoreNodeResponseItf nodeResponse = buildNodeResponse();

			nodeResponse = nodePush(
				nodeResponse,
				exitMenuNodeId
			);

			setNodeResponse(nodeResponse);
			return;
		}

		FwCoreNodeResponseItf nodeResponse = buildNodeResponse();

		FwCoreSlideItf slide = new FwCoreSlide();
		slide.setBody(
			translation.fwCore_system_body_what()
		);
		nodeResponse.getSlides().add(slide);

		setNodeResponse(nodeResponse);
	}

}
