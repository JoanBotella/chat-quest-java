package chatQuest.fw.core.node;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import chatQuest.fw.core.main.library.node.FwCoreTranslatableNodeAbs;
import chatQuest.fw.core.main.library.nodeResponse.FwCoreNodeResponseItf;
import chatQuest.fw.core.main.library.slide.FwCoreSlide;
import chatQuest.fw.core.main.library.slide.FwCoreSlideItf;
import chatQuest.fw.core.main.library.snapshotMetadata.FwCoreSnapshotMetadataItf;
import chatQuest.fw.core.translation.library.translation.FwCoreTranslationItf;
import chatQuest.fw.core.main.service.bodyBuilderBySnapshotLoader.FwCoreBodyBuilderBySnapshotLoaderItf;
import chatQuest.fw.core.main.service.nodeBrowser.FwCoreNodeBrowserItf;
import chatQuest.fw.core.main.service.nodeResponseByNodeRequestBuilder.FwCoreNodeResponseByNodeRequestBuilderItf;
import chatQuest.fw.core.main.service.snapshotLoader.FwCoreSnapshotLoaderItf;
import chatQuest.fw.core.main.service.snapshotMetadatasLister.FwCoreSnapshotMetadatasListerItf;
import chatQuest.fw.core.translation.service.translationContainer.FwCoreTranslationContainerItf;

public final class FwCoreTitleMenuNode
	extends
		FwCoreTranslatableNodeAbs
{

	private FwCoreSnapshotMetadatasListerItf snapshotMetadatasLister;
	private FwCoreSnapshotLoaderItf snapshotLoader;
	private FwCoreBodyBuilderBySnapshotLoaderItf bodyBuilderBySnapshotLoader;
	private String startingNodeId;
	private String settingsMenuNodeId;
	private String exitMenuNodeId;
	private String loadSnapshotMenuNodeId;

	public FwCoreTitleMenuNode(
		FwCoreNodeBrowserItf nodeBrowser,
		FwCoreNodeResponseByNodeRequestBuilderItf nodeResponseByNodeRequestBuilder,
		FwCoreTranslationContainerItf translationContainer,
		FwCoreSnapshotMetadatasListerItf snapshotMetadatasLister,
		FwCoreSnapshotLoaderItf snapshotLoader,
		FwCoreBodyBuilderBySnapshotLoaderItf bodyBuilderBySnapshotLoader,
		String startingNodeId,
		String settingsMenuNodeId,
		String exitMenuNodeId,
		String loadSnapshotMenuNodeId
	)
	{
		super(
			nodeBrowser,
			nodeResponseByNodeRequestBuilder,
			translationContainer
		);
		this.snapshotMetadatasLister = snapshotMetadatasLister;
		this.snapshotLoader = snapshotLoader;
		this.bodyBuilderBySnapshotLoader = bodyBuilderBySnapshotLoader;
		this.startingNodeId = startingNodeId;
		this.settingsMenuNodeId = settingsMenuNodeId;
		this.exitMenuNodeId = exitMenuNodeId;
		this.loadSnapshotMenuNodeId = loadSnapshotMenuNodeId;
	}

	public void tryToSetupNodeResponseWithoutQuery()
	{
		FwCoreTranslationItf translation = getTranslation();
		FwCoreNodeResponseItf nodeResponse = buildNodeResponse();

		FwCoreSlideItf slide = new FwCoreSlide();

		slide.setTitle(
			translation.fwCore_titleMenu_title()
		);

		List<String> options = new ArrayList<String>();
		if (hasSnapshots())
		{
			options.add(translation.fwCore_titleMenu_option_continue());
		}
		options.add(translation.fwCore_titleMenu_option_start());
		options.add(translation.fwCore_titleMenu_option_loadSnapshot());
		options.add(translation.fwCore_titleMenu_option_settings());
		options.add(translation.fwCore_titleMenu_option_exit());

		Map<String, String> menu = instantiateMenu();

		int index = 0;
		for (String option : options)
		{
			index += 1;
			menu.put(
				Integer.toString(index),
				option
			);
		}

		slide.setBody(
			buildMenuText(menu)
		);

		nodeResponse.getSlides().add(slide);

		setNodeResponse(nodeResponse);
	}

		private boolean hasSnapshots()
		{
			List<FwCoreSnapshotMetadataItf> list = snapshotMetadatasLister.list();
			return !list.isEmpty();
		}

	protected void tryToSetupNodeResponseWithQuery(String query)
	{
		FwCoreTranslationItf translation = getTranslation();

		int index = 1;

		if (hasSnapshots())
		{
			if (matches(query, Integer.toString(index)))
			{
				FwCoreNodeResponseItf nodeResponse = buildNodeResponse();

				snapshotLoader.load("2");

				List<Integer> snapshotLoaderErrors = snapshotLoader.getErrors();

				if (
					snapshotLoaderErrors.isEmpty()
					&& snapshotLoader.hasStatus()
				)
				{
					nodeResponse.setStatus(
						snapshotLoader.getStatusAfterHas()
					);
				}

				FwCoreSlideItf slide = new FwCoreSlide();

				String body = bodyBuilderBySnapshotLoader.build(
					snapshotLoader,
					translation
				);

				slide.setBody(body);

				nodeResponse.getSlides().add(slide);

				nodeResponse = nodePop(nodeResponse);

				setNodeResponse(nodeResponse);
				return;
			}

			index += 1;
		}

		if (matches(query, Integer.toString(index)))
		{
			FwCoreNodeResponseItf nodeResponse = buildNodeResponse();

			FwCoreSlideItf slide = new FwCoreSlide();
			slide.setBody(
				translation.fwCore_titleMenu_body_letsPlay()
			);
			nodeResponse.getSlides().add(slide);

			nodeResponse = nodeChange(
				nodeResponse,
				startingNodeId
			);

			setNodeResponse(nodeResponse);
			return;
		}

		index += 1;

		if (matches(query, Integer.toString(index)))
		{
			FwCoreNodeResponseItf nodeResponse = buildNodeResponse();

			nodeResponse = nodePush(
				nodeResponse,
				loadSnapshotMenuNodeId
			);
	
			setNodeResponse(nodeResponse);
			return;
		}

		index += 1;

		if (matches(query, Integer.toString(index)))
		{
			FwCoreNodeResponseItf nodeResponse = buildNodeResponse();

			nodeResponse = nodePush(
				nodeResponse,
				settingsMenuNodeId
			);
	
			setNodeResponse(nodeResponse);
			return;
		}

		index += 1;

		if (matches(query, Integer.toString(index)))
		{
			FwCoreNodeResponseItf nodeResponse = buildNodeResponse();

			nodeResponse = this.nodePush(
				nodeResponse,
				exitMenuNodeId
			);
	
			setNodeResponse(nodeResponse);
			return;
		}
	}

}
