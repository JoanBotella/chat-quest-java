package chatQuest.fw.core.node;

import chatQuest.fw.core.main.library.VariableConstant;
import chatQuest.fw.core.main.library.node.FwCoreTranslatableNodeAbs;
import chatQuest.fw.core.main.library.nodeResponse.FwCoreNodeResponseItf;
import chatQuest.fw.core.main.library.slide.FwCoreSlide;
import chatQuest.fw.core.main.library.slide.FwCoreSlideItf;
import chatQuest.fw.core.translation.library.translation.FwCoreTranslationItf;
import chatQuest.fw.core.main.service.nodeBrowser.FwCoreNodeBrowserItf;
import chatQuest.fw.core.main.service.nodeResponseByNodeRequestBuilder.FwCoreNodeResponseByNodeRequestBuilderItf;
import chatQuest.fw.core.translation.service.translationContainer.FwCoreTranslationContainerItf;

public final class
	FwCoreOverwriteSnapshotAskNameNode
extends
	FwCoreTranslatableNodeAbs
{

	private String overwriteSnapshotProcessNodeId;

	public FwCoreOverwriteSnapshotAskNameNode(
		FwCoreNodeBrowserItf nodeBrowser,
		FwCoreNodeResponseByNodeRequestBuilderItf nodeResponseByNodeRequestBuilder,
		FwCoreTranslationContainerItf translationContainer,
		String overwriteSnapshotProcessNodeId
	)
	{
		super(
			nodeBrowser,
			nodeResponseByNodeRequestBuilder,
			translationContainer
		);
		this.overwriteSnapshotProcessNodeId = overwriteSnapshotProcessNodeId;
	}

	public void tryToSetupNodeResponseWithoutQuery()
	{
		FwCoreTranslationItf translation = getTranslation();
		FwCoreNodeResponseItf nodeResponse = buildNodeResponse();

		FwCoreSlideItf slide = new FwCoreSlide();

		slide.setBody(
			translation.fwCore_overwriteSnapshotAskName_body()
		);

		nodeResponse.getSlides().add(slide);

		setNodeResponse(nodeResponse);
	}

	protected void tryToSetupNodeResponseWithQuery(String query)
	{
		FwCoreNodeResponseItf nodeResponse = buildNodeResponse();

		nodeResponse = putVariable(
			nodeResponse,
			VariableConstant.SNAPSHOT_NAME_KEY,
			query
		);

		nodeResponse = nodeChange(
			nodeResponse,
			overwriteSnapshotProcessNodeId
		);

		setNodeResponse(nodeResponse);
	}

}
