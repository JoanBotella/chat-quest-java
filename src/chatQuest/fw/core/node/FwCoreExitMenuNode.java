package chatQuest.fw.core.node;

import java.util.Map;

import chatQuest.fw.core.main.library.node.FwCoreTranslatableNodeAbs;
import chatQuest.fw.core.main.library.nodeResponse.FwCoreNodeResponseItf;
import chatQuest.fw.core.main.library.slide.FwCoreSlide;
import chatQuest.fw.core.main.library.slide.FwCoreSlideItf;
import chatQuest.fw.core.translation.library.translation.FwCoreTranslationItf;
import chatQuest.fw.core.main.service.nodeBrowser.FwCoreNodeBrowserItf;
import chatQuest.fw.core.main.service.nodeResponseByNodeRequestBuilder.FwCoreNodeResponseByNodeRequestBuilderItf;
import chatQuest.fw.core.translation.service.translationContainer.FwCoreTranslationContainerItf;

public final class FwCoreExitMenuNode
	extends
		FwCoreTranslatableNodeAbs
{

	public FwCoreExitMenuNode(
		FwCoreNodeBrowserItf nodeBrowser,
		FwCoreNodeResponseByNodeRequestBuilderItf nodeResponseByNodeRequestBuilder,
		FwCoreTranslationContainerItf translationContainer
	)
	{
		super(
			nodeBrowser,
			nodeResponseByNodeRequestBuilder,
			translationContainer
		);
	}

	public void tryToSetupNodeResponseWithoutQuery()
	{
		FwCoreTranslationItf translation = getTranslation();
		FwCoreNodeResponseItf nodeResponse = buildNodeResponse();

		FwCoreSlideItf slide = new FwCoreSlide();

		slide.setTitle(
			translation.fwCore_exitMenu_title()
		);

		String body = translation.fwCore_exitMenu_body_menu();

		Map<String, String> menu = instantiateMenu();
		menu.put(
			"1",
			translation.fwCore_exitMenu_option_no()
		);
		menu.put(
			"2",
			translation.fwCore_exitMenu_option_yes()
		);
		body += buildMenuText(menu);

		slide.setBody(body);

		nodeResponse.getSlides().add(slide);

		setNodeResponse(nodeResponse);
	}

	protected void tryToSetupNodeResponseWithQuery(String query)
	{
		FwCoreTranslationItf translation = getTranslation();

		if (matches(query, "1"))
		{
			FwCoreNodeResponseItf nodeResponse = buildNodeResponse();

			nodeResponse = nodePop(nodeResponse);

			setNodeResponse(nodeResponse);
			return;
		}

		if (matches(query, "2"))
		{
			FwCoreNodeResponseItf nodeResponse = buildNodeResponse();

			FwCoreSlideItf slide = new FwCoreSlide();
			slide.setBody(
				translation.fwCore_exitMenu_body_bye()
			);
			nodeResponse.getSlides().add(slide);

			nodeResponse.setEnd(true);

			setNodeResponse(nodeResponse);
			return;
		}
	}

}
