package chatQuest.fw.core.node;

import java.util.List;
import java.util.Map;

import chatQuest.fw.core.main.library.language.FwCoreLanguageItf;
import chatQuest.fw.core.main.library.node.FwCoreTranslatableNodeAbs;
import chatQuest.fw.core.main.library.nodeResponse.FwCoreNodeResponseItf;
import chatQuest.fw.core.main.library.slide.FwCoreSlide;
import chatQuest.fw.core.main.library.slide.FwCoreSlideItf;
import chatQuest.fw.core.translation.library.translation.FwCoreTranslationItf;
import chatQuest.fw.core.main.service.nodeBrowser.FwCoreNodeBrowserItf;
import chatQuest.fw.core.main.service.nodeResponseByNodeRequestBuilder.FwCoreNodeResponseByNodeRequestBuilderItf;
import chatQuest.fw.core.translation.service.translationContainer.FwCoreTranslationContainerItf;

public final class FwCoreLanguageMenuNode
	extends
		FwCoreTranslatableNodeAbs
{

	private List<FwCoreLanguageItf> enabledLanguages;

	public FwCoreLanguageMenuNode(
		FwCoreNodeBrowserItf nodeBrowser,
		FwCoreNodeResponseByNodeRequestBuilderItf nodeResponseByNodeRequestBuilder,
		FwCoreTranslationContainerItf translationContainer,
		List<FwCoreLanguageItf> enabledLanguages
	)
	{
		super(
			nodeBrowser,
			nodeResponseByNodeRequestBuilder,
			translationContainer
		);
		this.enabledLanguages = enabledLanguages;
	}

	public void tryToSetupNodeResponseWithoutQuery()
	{
		FwCoreTranslationItf translation = getTranslation();
		FwCoreNodeResponseItf nodeResponse = buildNodeResponse();

		FwCoreSlideItf slide = new FwCoreSlide();

		slide.setTitle(
			translation.fwCore_languageMenu_title()
		);

		Map<String, String> menu = instantiateMenu();

		int enabledLanguagesSize = enabledLanguages.size();
		for (int i = 0; i < enabledLanguagesSize; i++)
		{
			menu.put(
				String.valueOf(i + 1),
				enabledLanguages.get(i).getName()
			);
		}

		slide.setBody(
			buildMenuText(menu)
		);

		nodeResponse.getSlides().add(slide);

		setNodeResponse(nodeResponse);
	}

	protected void tryToSetupNodeResponseWithQuery(String query)
	{
		int enabledLanguagesSize = enabledLanguages.size();
		for (int i = 0; i < enabledLanguagesSize; i++)
		{
			if (matches(query, String.valueOf(i + 1)))
			{
				FwCoreNodeResponseItf nodeResponse = buildNodeResponse();

				nodeResponse
					.getStatus()
					.getSettings()
					.setLanguageCode(
						enabledLanguages.get(i).getCode()
					)
				;

				nodeResponse = nodePop(nodeResponse);

				setNodeResponse(nodeResponse);
				return;
			}
		}
	}

}
