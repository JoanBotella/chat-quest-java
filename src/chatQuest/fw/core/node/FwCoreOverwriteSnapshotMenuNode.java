package chatQuest.fw.core.node;

import java.util.Map;

import chatQuest.fw.core.main.library.VariableConstant;
import chatQuest.fw.core.main.library.node.FwCoreTranslatableNodeAbs;
import chatQuest.fw.core.main.library.nodeResponse.FwCoreNodeResponseItf;
import chatQuest.fw.core.main.library.slide.FwCoreSlide;
import chatQuest.fw.core.main.library.slide.FwCoreSlideItf;
import chatQuest.fw.core.translation.library.translation.FwCoreTranslationItf;
import chatQuest.fw.core.main.service.nodeBrowser.FwCoreNodeBrowserItf;
import chatQuest.fw.core.main.service.nodeResponseByNodeRequestBuilder.FwCoreNodeResponseByNodeRequestBuilderItf;
import chatQuest.fw.core.main.service.snapshotMetadatasMenuBuilder.FwCoreSnapshotMetadatasMenuBuilderItf;
import chatQuest.fw.core.translation.service.translationContainer.FwCoreTranslationContainerItf;

public final class FwCoreOverwriteSnapshotMenuNode
	extends
		FwCoreTranslatableNodeAbs
{

	private FwCoreSnapshotMetadatasMenuBuilderItf snapshotMetadatasMenuBuilder;
	private String overwriteSnapshotAskNameNodeId;

	public FwCoreOverwriteSnapshotMenuNode(
		FwCoreNodeBrowserItf nodeBrowser,
		FwCoreNodeResponseByNodeRequestBuilderItf nodeResponseByNodeRequestBuilder,
		FwCoreTranslationContainerItf translationContainer,
		FwCoreSnapshotMetadatasMenuBuilderItf snapshotMetadatasMenuBuilder,
		String overwriteSnapshotAskNameNodeId
	)
	{
		super(
			nodeBrowser,
			nodeResponseByNodeRequestBuilder,
			translationContainer
		);
		this.snapshotMetadatasMenuBuilder = snapshotMetadatasMenuBuilder;
		this.overwriteSnapshotAskNameNodeId = overwriteSnapshotAskNameNodeId;
	}

	public void tryToSetupNodeResponseWithoutQuery()
	{
		FwCoreTranslationItf translation = getTranslation();
		FwCoreNodeResponseItf nodeResponse = buildNodeResponse();

		FwCoreSlideItf slide = new FwCoreSlide();

		slide.setTitle(
			translation.fwCore_overwriteSnapshotMenu_title()
		);

		Map<String, String> menu = snapshotMetadatasMenuBuilder.build(
			instantiateMenu(),
			translation.fwCore_shared_option_back()
		);

		nodeResponse = putVariable(
			nodeResponse,
			VariableConstant.LAST_MENU_SIZE,
			String.valueOf(menu.size())
		);

		String body = buildMenuText(menu);

		slide.setBody(body);

		nodeResponse.getSlides().add(slide);

		setNodeResponse(nodeResponse);
	}

	protected void tryToSetupNodeResponseWithQuery(String query)
	{
		FwCoreNodeResponseItf nodeResponse = buildNodeResponse();

		if (matches(query, snapshotMetadatasMenuBuilder.getBackQuery()))
		{
			nodeResponse = nodePop(nodeResponse);
			setNodeResponse(nodeResponse);
			return;
		}

		String lastMenuSizeString = getVariable(
			nodeResponse,
			VariableConstant.LAST_MENU_SIZE
		);
		nodeResponse = removeVariable(
			nodeResponse,
			VariableConstant.LAST_MENU_SIZE
		);

		if (!isQueryInMenu(query, lastMenuSizeString))
		{
			return;
		}

		nodeResponse = putVariable(
			nodeResponse,
			VariableConstant.LAST_MENU_QUERY,
			query
		);

		nodeResponse = nodeChange(
			nodeResponse,
			overwriteSnapshotAskNameNodeId
		);

		setNodeResponse(nodeResponse);
	}

		private boolean isQueryInMenu(String query, String lastMenuSizeString)
		{
			Integer index;

			try
			{
				index = Integer.parseInt(query);
			}
			catch (Exception e)
			{
				return false;
			}

			if (index < 1)
			{
				return false;
			}

			Integer size;

			try
			{
				size = Integer.parseInt(lastMenuSizeString);
			}
			catch(Exception e)
			{
				return false;
			}

			return index <= size;
		}

}
