package chatQuest.fw.core.node;

import java.util.Map;

import chatQuest.fw.core.main.library.node.FwCoreTranslatableNodeAbs;
import chatQuest.fw.core.main.library.nodeResponse.FwCoreNodeResponseItf;
import chatQuest.fw.core.main.library.slide.FwCoreSlide;
import chatQuest.fw.core.main.library.slide.FwCoreSlideItf;
import chatQuest.fw.core.translation.library.translation.FwCoreTranslationItf;
import chatQuest.fw.core.main.service.nodeBrowser.FwCoreNodeBrowserItf;
import chatQuest.fw.core.main.service.nodeResponseByNodeRequestBuilder.FwCoreNodeResponseByNodeRequestBuilderItf;
import chatQuest.fw.core.translation.service.translationContainer.FwCoreTranslationContainerItf;

public final class FwCoreMainMenuNode
	extends
		FwCoreTranslatableNodeAbs
{

	private String snapshotsMenuNodeId;
	private String settingsMenuNodeId;
	private String exitMenuNodeId;

	public FwCoreMainMenuNode(
		FwCoreNodeBrowserItf nodeBrowser,
		FwCoreNodeResponseByNodeRequestBuilderItf nodeResponseByNodeRequestBuilder,
		FwCoreTranslationContainerItf translationContainer,
		String snapshotsMenuNodeId,
		String settingsMenuNodeId,
		String exitMenuNodeId
	)
	{
		super(
			nodeBrowser,
			nodeResponseByNodeRequestBuilder,
			translationContainer
		);
		this.snapshotsMenuNodeId = snapshotsMenuNodeId;
		this.settingsMenuNodeId = settingsMenuNodeId;
		this.exitMenuNodeId = exitMenuNodeId;
	}

	public void tryToSetupNodeResponseWithoutQuery()
	{
		FwCoreTranslationItf translation = getTranslation();
		FwCoreNodeResponseItf nodeResponse = buildNodeResponse();

		FwCoreSlideItf slide = new FwCoreSlide();

		slide.setTitle(
			translation.fwCore_mainMenu_title()
		);

		Map<String, String> menu = instantiateMenu();
		menu.put(
			"1",
			translation.fwCore_shared_option_back()
		);
		menu.put(
			"2",
			translation.fwCore_mainMenu_option_snapshots()
		);
		menu.put(
			"3",
			translation.fwCore_mainMenu_option_settings()
		);
		menu.put(
			"4",
			translation.fwCore_mainMenu_option_exit()
		);
		slide.setBody(
			buildMenuText(menu)
		);

		nodeResponse.getSlides().add(slide);

		setNodeResponse(nodeResponse);
	}

	protected void tryToSetupNodeResponseWithQuery(String query)
	{
		FwCoreTranslationItf translation = getTranslation();

		if (matches(query, "1"))
		{
			FwCoreNodeResponseItf nodeResponse = buildNodeResponse();

			nodeResponse = nodePop(nodeResponse);

			setNodeResponse(nodeResponse);
			return;
		}

		if (matches(query, "2"))
		{
			FwCoreNodeResponseItf nodeResponse = buildNodeResponse();

			nodeResponse = nodePush(
				nodeResponse,
				snapshotsMenuNodeId
			);
	
			setNodeResponse(nodeResponse);
			return;
		}

		if (matches(query, "3"))
		{
			FwCoreNodeResponseItf nodeResponse = buildNodeResponse();

			nodeResponse = nodePush(
				nodeResponse,
				settingsMenuNodeId
			);
	
			setNodeResponse(nodeResponse);
			return;
		}

		if (matches(query, "4"))
		{
			FwCoreNodeResponseItf nodeResponse = buildNodeResponse();

			nodeResponse = this.nodePush(
				nodeResponse,
				exitMenuNodeId
			);
	
			setNodeResponse(nodeResponse);
			return;
		}
	}

}
