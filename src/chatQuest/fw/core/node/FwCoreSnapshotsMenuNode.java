package chatQuest.fw.core.node;

import java.util.Map;

import chatQuest.fw.core.main.library.node.FwCoreTranslatableNodeAbs;
import chatQuest.fw.core.main.library.nodeResponse.FwCoreNodeResponseItf;
import chatQuest.fw.core.main.library.slide.FwCoreSlide;
import chatQuest.fw.core.main.library.slide.FwCoreSlideItf;
import chatQuest.fw.core.translation.library.translation.FwCoreTranslationItf;
import chatQuest.fw.core.main.service.nodeBrowser.FwCoreNodeBrowserItf;
import chatQuest.fw.core.main.service.nodeResponseByNodeRequestBuilder.FwCoreNodeResponseByNodeRequestBuilderItf;
import chatQuest.fw.core.translation.service.translationContainer.FwCoreTranslationContainerItf;

public final class FwCoreSnapshotsMenuNode
	extends
		FwCoreTranslatableNodeAbs
{

	private String loadSnapshotMenuNodeId;
	private String createSnapshotAskNameNodeId;
	private String overwriteSnapshotMenuNodeId;
	private String deleteSnapshotMenuNodeId;

	public FwCoreSnapshotsMenuNode(
		FwCoreNodeBrowserItf nodeBrowser,
		FwCoreNodeResponseByNodeRequestBuilderItf nodeResponseByNodeRequestBuilder,
		FwCoreTranslationContainerItf translationContainer,
		String loadSnapshotMenuNodeId,
		String createSnapshotAskNameNodeId,
		String overwriteSnapshotMenuNodeId,
		String deleteSnapshotMenuNodeId
	)
	{
		super(
			nodeBrowser,
			nodeResponseByNodeRequestBuilder,
			translationContainer
		);
		this.loadSnapshotMenuNodeId = loadSnapshotMenuNodeId;
		this.createSnapshotAskNameNodeId = createSnapshotAskNameNodeId;
		this.overwriteSnapshotMenuNodeId = overwriteSnapshotMenuNodeId;
		this.deleteSnapshotMenuNodeId = deleteSnapshotMenuNodeId;
	}

	public void tryToSetupNodeResponseWithoutQuery()
	{
		FwCoreTranslationItf translation = getTranslation();
		FwCoreNodeResponseItf nodeResponse = buildNodeResponse();

		FwCoreSlideItf slide = new FwCoreSlide();

		slide.setTitle(
			translation.fwCore_snapshotsMenu_title()
		);

		Map<String, String> menu = instantiateMenu();
		menu.put(
			"1",
			translation.fwCore_shared_option_back()
		);
		menu.put(
			"2",
			translation.fwCore_snapshotsMenu_option_load()
		);
		menu.put(
			"3",
			translation.fwCore_snapshotsMenu_option_create()
		);
		menu.put(
			"4",
			translation.fwCore_snapshotsMenu_option_overwrite()
		);
		menu.put(
			"5",
			translation.fwCore_snapshotsMenu_option_delete()
		);

		String body = buildMenuText(menu);

		slide.setBody(body);

		nodeResponse.getSlides().add(slide);

		setNodeResponse(nodeResponse);
	}

	protected void tryToSetupNodeResponseWithQuery(String query)
	{
		// FwCoreTranslationItf translation = getTranslation();

		if (matches(query, "1"))
		{
			FwCoreNodeResponseItf nodeResponse = buildNodeResponse();

			nodeResponse = nodePop(nodeResponse);

			setNodeResponse(nodeResponse);
			return;
		}

		if (matches(query, "2"))
		{
			FwCoreNodeResponseItf nodeResponse = buildNodeResponse();

			nodeResponse = nodePush(
				nodeResponse,
				loadSnapshotMenuNodeId
			);

			setNodeResponse(nodeResponse);
			return;
		}

		if (matches(query, "3"))
		{
			FwCoreNodeResponseItf nodeResponse = buildNodeResponse();

			nodeResponse = nodePush(
				nodeResponse,
				createSnapshotAskNameNodeId
			);

			setNodeResponse(nodeResponse);
			return;
		}

		if (matches(query, "4"))
		{
			FwCoreNodeResponseItf nodeResponse = buildNodeResponse();

			nodeResponse = nodePush(
				nodeResponse,
				overwriteSnapshotMenuNodeId
			);

			setNodeResponse(nodeResponse);
			return;
		}

		if (matches(query, "5"))
		{
			FwCoreNodeResponseItf nodeResponse = buildNodeResponse();

			nodeResponse = nodePush(
				nodeResponse,
				deleteSnapshotMenuNodeId
			);

			setNodeResponse(nodeResponse);
			return;
		}

	}

}
