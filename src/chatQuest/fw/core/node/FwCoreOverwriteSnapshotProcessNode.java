package chatQuest.fw.core.node;

import chatQuest.fw.core.main.library.VariableConstant;
import chatQuest.fw.core.main.library.node.FwCoreTranslatableNodeAbs;
import chatQuest.fw.core.main.library.nodeResponse.FwCoreNodeResponseItf;
import chatQuest.fw.core.main.library.slide.FwCoreSlide;
import chatQuest.fw.core.main.library.slide.FwCoreSlideItf;
import chatQuest.fw.core.main.service.bodyBuilderBySnapshotOverwriter.FwCoreBodyBuilderBySnapshotOverwriterItf;
import chatQuest.fw.core.main.service.nodeBrowser.FwCoreNodeBrowserItf;
import chatQuest.fw.core.main.service.nodeResponseByNodeRequestBuilder.FwCoreNodeResponseByNodeRequestBuilderItf;
import chatQuest.fw.core.main.service.snapshotOverwriter.FwCoreSnapshotOverwriterItf;
import chatQuest.fw.core.translation.service.translationContainer.FwCoreTranslationContainerItf;

public final class
	FwCoreOverwriteSnapshotProcessNode
extends
	FwCoreTranslatableNodeAbs
{

	private FwCoreSnapshotOverwriterItf snapshotOverwriter;
	private FwCoreBodyBuilderBySnapshotOverwriterItf bodyBuilderBySnapshotOverwriter;

	public FwCoreOverwriteSnapshotProcessNode(
		FwCoreNodeBrowserItf nodeBrowser,
		FwCoreNodeResponseByNodeRequestBuilderItf nodeResponseByNodeRequestBuilder,
		FwCoreTranslationContainerItf translationContainer,
		FwCoreSnapshotOverwriterItf snapshotOverwriter,
		FwCoreBodyBuilderBySnapshotOverwriterItf bodyBuilderBySnapshotOverwriter
	)
	{
		super(
			nodeBrowser,
			nodeResponseByNodeRequestBuilder,
			translationContainer
		);
		this.snapshotOverwriter = snapshotOverwriter;
		this.bodyBuilderBySnapshotOverwriter = bodyBuilderBySnapshotOverwriter;
	}

	public void tryToSetupNodeResponseWithoutQuery()
	{
		FwCoreNodeResponseItf nodeResponse = buildNodeResponse();

		FwCoreSlideItf slide = new FwCoreSlide();

		String body = processAndBuildBody(nodeResponse);

		slide.setBody(body);

		nodeResponse.getSlides().add(slide);

		nodeResponse = nodePop(nodeResponse);

		setNodeResponse(nodeResponse);
	}

		private String processAndBuildBody(FwCoreNodeResponseItf nodeResponse)
		{
			String variableSnapshotNameKey = VariableConstant.SNAPSHOT_NAME_KEY;

			String snapshotName = getVariable(
				nodeResponse,
				variableSnapshotNameKey
			);
			nodeResponse = removeVariable(
				nodeResponse,
				variableSnapshotNameKey
			);

			String variableLastMenuQuery = VariableConstant.LAST_MENU_QUERY;

			String lastMenuQuery = getVariable(
				nodeResponse,
				variableLastMenuQuery
			);
			nodeResponse = removeVariable(
				nodeResponse,
				variableLastMenuQuery
			);

			snapshotOverwriter.overwrite(
				lastMenuQuery,
				snapshotName,
				nodeResponse.getStatus()
			);

			return bodyBuilderBySnapshotOverwriter.build(
				snapshotOverwriter,
				getTranslation()
			);
		}

}