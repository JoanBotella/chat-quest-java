package chatQuest.fw.core.node;

import java.util.Map;

import chatQuest.fw.core.main.library.node.FwCoreTranslatableNodeAbs;
import chatQuest.fw.core.main.library.nodeResponse.FwCoreNodeResponseItf;
import chatQuest.fw.core.main.library.slide.FwCoreSlide;
import chatQuest.fw.core.main.library.slide.FwCoreSlideItf;
import chatQuest.fw.core.translation.library.translation.FwCoreTranslationItf;
import chatQuest.fw.core.main.service.nodeBrowser.FwCoreNodeBrowserItf;
import chatQuest.fw.core.main.service.nodeResponseByNodeRequestBuilder.FwCoreNodeResponseByNodeRequestBuilderItf;
import chatQuest.fw.core.translation.service.translationContainer.FwCoreTranslationContainerItf;

public final class FwCoreSettingsMenuNode
	extends
		FwCoreTranslatableNodeAbs
{

	private String languageMenuNodeId;

	public FwCoreSettingsMenuNode(
		FwCoreNodeBrowserItf nodeBrowser,
		FwCoreNodeResponseByNodeRequestBuilderItf nodeResponseByNodeRequestBuilder,
		FwCoreTranslationContainerItf translationContainer,
		String languageMenuNodeId
	)
	{
		super(
			nodeBrowser,
			nodeResponseByNodeRequestBuilder,
			translationContainer
		);
		this.languageMenuNodeId = languageMenuNodeId;
	}

	public void tryToSetupNodeResponseWithoutQuery()
	{
		FwCoreTranslationItf translation = getTranslation();
		FwCoreNodeResponseItf nodeResponse = buildNodeResponse();

		FwCoreSlideItf slide = new FwCoreSlide();

		slide.setTitle(
			translation.fwCore_settingsMenu_title()
		);

		Map<String, String> menu = instantiateMenu();
		menu.put(
			"1",
			translation.fwCore_shared_option_back()
		);
		menu.put(
			"2",
			translation.fwCore_settingsMenu_option_language()
		);
		slide.setBody(
			buildMenuText(menu)
		);

		nodeResponse.getSlides().add(slide);

		setNodeResponse(nodeResponse);
	}

	public void tryToSetupNodeResponseWithQuery(String query)
	{
		FwCoreTranslationItf translation = getTranslation();

		if (matches(query, "1"))
		{
			FwCoreNodeResponseItf nodeResponse = buildNodeResponse();

			nodeResponse = nodePop(nodeResponse);

			setNodeResponse(nodeResponse);
			return;
		}

		if (matches(query, "2"))
		{
			FwCoreNodeResponseItf nodeResponse = buildNodeResponse();

			nodeResponse = nodePush(
				nodeResponse,
				languageMenuNodeId
			);

			setNodeResponse(nodeResponse);
			return;
		}
	}

}
