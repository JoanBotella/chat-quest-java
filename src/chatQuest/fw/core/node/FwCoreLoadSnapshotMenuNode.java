package chatQuest.fw.core.node;

import java.util.List;
import java.util.Map;

import chatQuest.fw.core.main.library.node.FwCoreTranslatableNodeAbs;
import chatQuest.fw.core.main.library.nodeResponse.FwCoreNodeResponseItf;
import chatQuest.fw.core.main.library.slide.FwCoreSlide;
import chatQuest.fw.core.main.library.slide.FwCoreSlideItf;
import chatQuest.fw.core.translation.library.translation.FwCoreTranslationItf;
import chatQuest.fw.core.main.service.bodyBuilderBySnapshotLoader.FwCoreBodyBuilderBySnapshotLoaderItf;
import chatQuest.fw.core.main.service.nodeBrowser.FwCoreNodeBrowserItf;
import chatQuest.fw.core.main.service.nodeResponseByNodeRequestBuilder.FwCoreNodeResponseByNodeRequestBuilderItf;
import chatQuest.fw.core.main.service.snapshotLoader.FwCoreSnapshotLoaderItf;
import chatQuest.fw.core.main.service.snapshotMetadatasMenuBuilder.FwCoreSnapshotMetadatasMenuBuilderItf;
import chatQuest.fw.core.translation.service.translationContainer.FwCoreTranslationContainerItf;

public final class FwCoreLoadSnapshotMenuNode
	extends
		FwCoreTranslatableNodeAbs
{

	private FwCoreSnapshotMetadatasMenuBuilderItf snapshotMetadatasMenuBuilder;
	private FwCoreSnapshotLoaderItf snapshotLoader;
	private FwCoreBodyBuilderBySnapshotLoaderItf bodyBuilderBySnapshotLoader;

	public FwCoreLoadSnapshotMenuNode(
		FwCoreNodeBrowserItf nodeBrowser,
		FwCoreNodeResponseByNodeRequestBuilderItf nodeResponseByNodeRequestBuilder,
		FwCoreTranslationContainerItf translationContainer,
		FwCoreSnapshotMetadatasMenuBuilderItf snapshotMetadatasMenuBuilder,
		FwCoreSnapshotLoaderItf snapshotLoader,
		FwCoreBodyBuilderBySnapshotLoaderItf bodyBuilderBySnapshotLoader
	)
	{
		super(
			nodeBrowser,
			nodeResponseByNodeRequestBuilder,
			translationContainer
		);
		this.snapshotMetadatasMenuBuilder = snapshotMetadatasMenuBuilder;
		this.snapshotLoader = snapshotLoader;
		this.bodyBuilderBySnapshotLoader = bodyBuilderBySnapshotLoader;
	}

	public void tryToSetupNodeResponseWithoutQuery()
	{
		FwCoreTranslationItf translation = getTranslation();
		FwCoreNodeResponseItf nodeResponse = buildNodeResponse();

		FwCoreSlideItf slide = new FwCoreSlide();

		slide.setTitle(
			translation.fwCore_loadSnapshotMenu_title()
		);

		Map<String, String> menu = snapshotMetadatasMenuBuilder.build(
			instantiateMenu(),
			translation.fwCore_shared_option_back()
		);

		String body = buildMenuText(menu);

		slide.setBody(body);

		nodeResponse.getSlides().add(slide);

		setNodeResponse(nodeResponse);
	}

	protected void tryToSetupNodeResponseWithQuery(String query)
	{
		FwCoreNodeResponseItf nodeResponse = buildNodeResponse();

		if (matches(query, snapshotMetadatasMenuBuilder.getBackQuery()))
		{
			nodeResponse = nodePop(nodeResponse);
			setNodeResponse(nodeResponse);
			return;
		}
	
		FwCoreSlideItf slide = new FwCoreSlide();

		String body = processAndBuildBody(
			nodeResponse,
			query
		);

		slide.setBody(body);

		nodeResponse.getSlides().add(slide);

		nodeResponse = nodePop(nodeResponse);

		setNodeResponse(nodeResponse);
	}

		private String processAndBuildBody(FwCoreNodeResponseItf nodeResponse, String query)
		{
			snapshotLoader.load(query);

			List<Integer> snapshotLoaderErrors = snapshotLoader.getErrors();
	
			if (
				snapshotLoaderErrors.isEmpty()
				&& snapshotLoader.hasStatus()
			)
			{
				nodeResponse.setStatus(
					snapshotLoader.getStatusAfterHas()
				);
			}
	
			return bodyBuilderBySnapshotLoader.build(
				snapshotLoader,
				getTranslation()
			);
		}

}
