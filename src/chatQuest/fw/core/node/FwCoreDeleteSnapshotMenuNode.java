package chatQuest.fw.core.node;

import java.util.Map;

import chatQuest.fw.core.main.library.node.FwCoreTranslatableNodeAbs;
import chatQuest.fw.core.main.library.nodeResponse.FwCoreNodeResponseItf;
import chatQuest.fw.core.main.library.slide.FwCoreSlide;
import chatQuest.fw.core.main.library.slide.FwCoreSlideItf;
import chatQuest.fw.core.translation.library.translation.FwCoreTranslationItf;
import chatQuest.fw.core.main.service.bodyBuilderBySnapshotDeleter.FwCoreBodyBuilderBySnapshotDeleterItf;
import chatQuest.fw.core.main.service.nodeBrowser.FwCoreNodeBrowserItf;
import chatQuest.fw.core.main.service.nodeResponseByNodeRequestBuilder.FwCoreNodeResponseByNodeRequestBuilderItf;
import chatQuest.fw.core.main.service.snapshotDeleter.FwCoreSnapshotDeleterItf;
import chatQuest.fw.core.main.service.snapshotMetadatasMenuBuilder.FwCoreSnapshotMetadatasMenuBuilderItf;
import chatQuest.fw.core.translation.service.translationContainer.FwCoreTranslationContainerItf;

public final class FwCoreDeleteSnapshotMenuNode
	extends
		FwCoreTranslatableNodeAbs
{

	private FwCoreSnapshotMetadatasMenuBuilderItf snapshotMetadatasMenuBuilder;
	private FwCoreSnapshotDeleterItf snapshotDeleter;
	private FwCoreBodyBuilderBySnapshotDeleterItf bodyBuilderBySnapshotDeleter;

	public FwCoreDeleteSnapshotMenuNode(
		FwCoreNodeBrowserItf nodeBrowser,
		FwCoreNodeResponseByNodeRequestBuilderItf nodeResponseByNodeRequestBuilder,
		FwCoreTranslationContainerItf translationContainer,
		FwCoreSnapshotMetadatasMenuBuilderItf snapshotMetadatasMenuBuilder,
		FwCoreSnapshotDeleterItf snapshotDeleter,
		FwCoreBodyBuilderBySnapshotDeleterItf bodyBuilderBySnapshotDeleter
	)
	{
		super(
			nodeBrowser,
			nodeResponseByNodeRequestBuilder,
			translationContainer
		);
		this.snapshotMetadatasMenuBuilder = snapshotMetadatasMenuBuilder;
		this.snapshotDeleter = snapshotDeleter;
		this.bodyBuilderBySnapshotDeleter = bodyBuilderBySnapshotDeleter;
	}

	public void tryToSetupNodeResponseWithoutQuery()
	{
		FwCoreTranslationItf translation = getTranslation();
		FwCoreNodeResponseItf nodeResponse = buildNodeResponse();

		FwCoreSlideItf slide = new FwCoreSlide();

		slide.setTitle(
			translation.fwCore_deleteSnapshotMenu_title()
		);

		Map<String, String> menu = snapshotMetadatasMenuBuilder.build(
			instantiateMenu(),
			translation.fwCore_shared_option_back()
		);

		String body = buildMenuText(menu);

		slide.setBody(body);

		nodeResponse.getSlides().add(slide);

		setNodeResponse(nodeResponse);
	}

	protected void tryToSetupNodeResponseWithQuery(String query)
	{
		FwCoreNodeResponseItf nodeResponse = buildNodeResponse();

		if (matches(query, snapshotMetadatasMenuBuilder.getBackQuery()))
		{
			nodeResponse = nodePop(nodeResponse);
			setNodeResponse(nodeResponse);
			return;
		}

		FwCoreSlideItf slide = new FwCoreSlide();

		String body = processAndBuildBody(query);

		slide.setBody(body);

		nodeResponse.getSlides().add(slide);

		nodeResponse = nodePop(nodeResponse);

		setNodeResponse(nodeResponse);
	}

		private String processAndBuildBody(String query)
		{
			snapshotDeleter.delete(query);

			return bodyBuilderBySnapshotDeleter.build(
				snapshotDeleter,
				getTranslation()
			);
		}

}
