package chatQuest.fw.core.main.library.slide;

public interface FwCoreSlideItf
{

	public boolean hasTitle();

	public String getTitleAfterHas();

	public void setTitle(String title);

	public void unsetTitle();

	public boolean hasBody();

	public String getBodyAfterHas();

	public void setBody(String body);

	public void unsetBody();

}
