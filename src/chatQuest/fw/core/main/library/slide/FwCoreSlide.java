package chatQuest.fw.core.main.library.slide;

public final class FwCoreSlide
	implements
		FwCoreSlideItf
{

	private String title;
	private String body;

	public boolean hasTitle()
	{
		return title != null;
	}

	public String getTitleAfterHas()
	{
		return title;
	}

	public void setTitle(String title)
	{
		this.title = title;
	}

	public void unsetTitle()
	{
		title = null;
	}

	public boolean hasBody()
	{
		return body != null;
	}

	public String getBodyAfterHas()
	{
		return body;
	}

	public void setBody(String body)
	{
		this.body = body;
	}

	public void unsetBody()
	{
		body = null;
	}

}
