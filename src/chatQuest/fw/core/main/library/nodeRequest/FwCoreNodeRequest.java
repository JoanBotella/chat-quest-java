package chatQuest.fw.core.main.library.nodeRequest;

import chatQuest.fw.core.main.library.status.FwCoreStatusItf;

public final class FwCoreNodeRequest
	implements
		FwCoreNodeRequestItf
{

	private FwCoreStatusItf status;
	private String query;

	public FwCoreNodeRequest(
		FwCoreStatusItf status
	)
	{
		this.status = status;
	}

	public FwCoreStatusItf getStatus()
	{
		return status;
	}

	public void setStatus(FwCoreStatusItf status)
	{
		this.status = status;
	}

	public boolean hasQuery()
	{
		return query != null;
	}

	public String getQueryAfterHas()
	{
		return query;
	}

	public void setQuery(String query)
	{
		this.query = query;
	}

	public void unsetQuery()
	{
		query = null;
	}

}
