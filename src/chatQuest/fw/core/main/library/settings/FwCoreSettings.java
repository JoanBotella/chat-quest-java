package chatQuest.fw.core.main.library.settings;

public final class FwCoreSettings
	implements
		FwCoreSettingsItf
{

	private String languageCode;

	public FwCoreSettings(
		String languageCode
	)
	{
		this.languageCode = languageCode;
	}

	public String getLanguageCode()
	{
		return languageCode;
	}

	public void setLanguageCode(String languageCode)
	{
		this.languageCode = languageCode;
	}

}
