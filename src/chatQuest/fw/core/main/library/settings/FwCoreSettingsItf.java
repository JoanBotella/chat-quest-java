package chatQuest.fw.core.main.library.settings;

public interface
	FwCoreSettingsItf
{

	public String getLanguageCode();

	public void setLanguageCode(String languageCode);

}
