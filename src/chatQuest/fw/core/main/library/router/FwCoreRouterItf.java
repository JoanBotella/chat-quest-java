package chatQuest.fw.core.main.library.router;

import chatQuest.fw.core.main.library.node.FwCoreNodeItf;

public interface FwCoreRouterItf
{

	public FwCoreNodeItf route(String nodeId);

}
