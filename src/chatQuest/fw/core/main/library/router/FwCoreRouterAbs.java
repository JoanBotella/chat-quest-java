package chatQuest.fw.core.main.library.router;

import chatQuest.fw.core.main.library.node.FwCoreNodeItf;

public abstract class FwCoreRouterAbs
	implements
		FwCoreRouterItf
{

	private FwCoreNodeItf node;

	public FwCoreNodeItf route(String nodeId)
	{
		setupNode(nodeId);

		if (node == null)
		{
			throw new RuntimeException("the node id " + nodeId + " is not routable.");
		}

		FwCoreNodeItf built = node;
		node = null;
		return built;
	}

		protected abstract void setupNode(String nodeId);

	protected boolean hasNode()
	{
		return node != null;
	}

	protected void setNode(FwCoreNodeItf node)
	{
		this.node = node;
	}

}
