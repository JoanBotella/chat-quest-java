package chatQuest.fw.core.main.library.status;

import java.util.List;
import java.util.Map;

import chatQuest.fw.core.main.library.settings.FwCoreSettingsItf;

public interface
	FwCoreStatusItf
{

	public List<String> getBreadcrumbs();

	public void setBreadcrumbs(List<String> breadcrumbs);

	public FwCoreSettingsItf getSettings();

	public void setSettings(FwCoreSettingsItf settings);

	public Map<String, String> getVariables();

	public void setVariables(Map<String, String> variables);

}
