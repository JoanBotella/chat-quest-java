package chatQuest.fw.core.main.library.status;

import java.util.List;
import java.util.Map;

import chatQuest.fw.core.main.library.settings.FwCoreSettingsItf;

public final class FwCoreStatus
	implements
		FwCoreStatusItf
{

	private List<String> breadcrumbs;
	private FwCoreSettingsItf settings;
	private Map<String, String> variables;

	public FwCoreStatus(
		List<String> breadcrumbs,
		FwCoreSettingsItf settings,
		Map<String, String> variables
	)
	{
		this.breadcrumbs = breadcrumbs;
		this.settings = settings;
		this.variables = variables;
	}

	public List<String> getBreadcrumbs()
	{
		return breadcrumbs;
	}

	public void setBreadcrumbs(List<String> breadcrumbs)
	{
		this.breadcrumbs = breadcrumbs;
	}

	public FwCoreSettingsItf getSettings()
	{
		return settings;
	}

	public void setSettings(FwCoreSettingsItf settings)
	{
		this.settings = settings;
	}

	public Map<String, String> getVariables()
	{
		return variables;
	}

	public void setVariables(Map<String, String> variables)
	{
		this.variables = variables;
	}

}
