package chatQuest.fw.core.main.library.enabledLanguagesContainer;

import java.util.List;

import chatQuest.fw.core.main.library.language.FwCoreLanguageItf;

public interface FwCoreEnabledLanguagesContainerItf
{

	public List<FwCoreLanguageItf> get();

}
