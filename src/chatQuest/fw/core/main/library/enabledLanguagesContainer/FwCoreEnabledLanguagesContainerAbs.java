package chatQuest.fw.core.main.library.enabledLanguagesContainer;

import java.util.ArrayList;
import java.util.List;

import chatQuest.fw.core.main.library.language.FwCoreLanguageItf;

public abstract class FwCoreEnabledLanguagesContainerAbs
	implements
		FwCoreEnabledLanguagesContainerItf
{

	private List<FwCoreLanguageItf> languages;

	public List<FwCoreLanguageItf> get()
	{
		if (languages == null)
		{
			languages = build();
		}
		return languages;
	}

		private List<FwCoreLanguageItf> build()
		{
			List<FwCoreLanguageItf> built = new ArrayList<FwCoreLanguageItf>();

			built = addLanguages(built);

			return built;
		}

			protected abstract List<FwCoreLanguageItf> addLanguages(List<FwCoreLanguageItf> built);

}
