package chatQuest.fw.core.main.library.nodeResponse;

import java.util.ArrayList;
import java.util.List;

import chatQuest.fw.core.main.library.slide.FwCoreSlideItf;
import chatQuest.fw.core.main.library.status.FwCoreStatusItf;

public final class FwCoreNodeResponse
	implements
		FwCoreNodeResponseItf
{

	private FwCoreStatusItf status;
	private List<FwCoreSlideItf> slides;
	private boolean end;
	private boolean processAgain;

	public FwCoreNodeResponse(
		FwCoreStatusItf status
	)
	{
		this.status = status;

		slides = new ArrayList<FwCoreSlideItf>();
		end = false;
		processAgain = false;
	}

	public FwCoreStatusItf getStatus()
	{
		return status;
	}

	public void setStatus(FwCoreStatusItf status)
	{
		this.status = status;
	}

	public List<FwCoreSlideItf> getSlides()
	{
		return slides;
	}

	public void setSlides(List<FwCoreSlideItf> slides)
	{
		this.slides = slides;
	}

	public boolean getEnd()
	{
		return end;
	}

	public void setEnd(boolean end)
	{
		this.end = end;
	}

	public void setProcessAgain(boolean processAgain)
	{
		this.processAgain = processAgain;
	}

	public boolean getProcessAgain()
	{
		return processAgain;
	}

}
