package chatQuest.fw.core.main.library.nodeResponse;

import java.util.List;

import chatQuest.fw.core.main.library.slide.FwCoreSlideItf;
import chatQuest.fw.core.main.library.status.FwCoreStatusItf;

public interface FwCoreNodeResponseItf
{

	public FwCoreStatusItf getStatus();

	public void setStatus(FwCoreStatusItf status);

	public List<FwCoreSlideItf> getSlides();

	public void setSlides(List<FwCoreSlideItf> slides);

	public boolean getEnd();

	public void setEnd(boolean end);

	public void setProcessAgain(boolean processAgain);

	public boolean getProcessAgain();

}
