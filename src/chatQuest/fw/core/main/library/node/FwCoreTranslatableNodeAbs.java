package chatQuest.fw.core.main.library.node;

import chatQuest.fw.core.translation.library.translation.FwCoreTranslationItf;
import chatQuest.fw.core.main.service.nodeBrowser.FwCoreNodeBrowserItf;
import chatQuest.fw.core.main.service.nodeResponseByNodeRequestBuilder.FwCoreNodeResponseByNodeRequestBuilderItf;
import chatQuest.fw.core.translation.service.translationContainer.FwCoreTranslationContainerItf;

public abstract class FwCoreTranslatableNodeAbs
	extends
		FwCoreNodeAbs
	implements
		FwCoreTranslatableNodeItf
{

	private FwCoreTranslationContainerItf translationContainer;

	public FwCoreTranslatableNodeAbs(
		FwCoreNodeBrowserItf nodeBrowser,
		FwCoreNodeResponseByNodeRequestBuilderItf nodeResponseByNodeRequestBuilder,
		FwCoreTranslationContainerItf translationContainer
	)
	{
		super(
			nodeBrowser,
			nodeResponseByNodeRequestBuilder
		);
		this.translationContainer = translationContainer;
	}

	protected FwCoreTranslationItf getTranslation()
	{
		return translationContainer.get(
			getNodeRequest()
			.getStatus()
			.getSettings()
			.getLanguageCode()
		);
	}

}
