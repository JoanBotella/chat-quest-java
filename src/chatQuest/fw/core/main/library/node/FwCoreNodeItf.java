package chatQuest.fw.core.main.library.node;

import chatQuest.fw.core.main.library.nodeRequest.FwCoreNodeRequestItf;
import chatQuest.fw.core.main.library.nodeResponse.FwCoreNodeResponseItf;

public interface FwCoreNodeItf
{

	public void run(FwCoreNodeRequestItf nodeRequest);

	public boolean hasNodeResponse();

	public FwCoreNodeResponseItf getNodeResponseAfterHas();

}
