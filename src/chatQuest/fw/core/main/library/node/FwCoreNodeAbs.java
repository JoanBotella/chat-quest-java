package chatQuest.fw.core.main.library.node;

import java.util.HashMap;
import java.util.Map;

import chatQuest.fw.core.main.library.nodeRequest.FwCoreNodeRequestItf;
import chatQuest.fw.core.main.library.nodeResponse.FwCoreNodeResponseItf;
import chatQuest.fw.core.main.library.status.FwCoreStatusItf;
import chatQuest.fw.core.main.service.nodeBrowser.FwCoreNodeBrowserItf;
import chatQuest.fw.core.main.service.nodeResponseByNodeRequestBuilder.FwCoreNodeResponseByNodeRequestBuilderItf;

public abstract class FwCoreNodeAbs
	implements
		FwCoreNodeItf
{

	private FwCoreNodeBrowserItf nodeBrowser;
	private FwCoreNodeResponseByNodeRequestBuilderItf nodeResponseByNodeRequestBuilder;

	private FwCoreNodeRequestItf nodeRequest;
	private FwCoreNodeResponseItf nodeResponse;

	public FwCoreNodeAbs(
		FwCoreNodeBrowserItf nodeBrowser,
		FwCoreNodeResponseByNodeRequestBuilderItf nodeResponseByNodeRequestBuilder
	)
	{
		this.nodeBrowser = nodeBrowser;
		this.nodeResponseByNodeRequestBuilder = nodeResponseByNodeRequestBuilder;
	}

	public void run(FwCoreNodeRequestItf nodeRequest)
	{
		this.nodeRequest = nodeRequest;
		nodeResponse = null;

		tryToSetupNodeResponse();
	}

		private void tryToSetupNodeResponse()
		{
			if (getNodeRequest().hasQuery())
			{
				tryToSetupNodeResponseWithQuery(
					getNodeRequest().getQueryAfterHas()
				);
				return;
			}
			tryToSetupNodeResponseWithoutQuery();
		}

			protected void tryToSetupNodeResponseWithQuery(String query)
			{
				tryToSetupNodeResponseWithoutQuery();
			}

			protected void tryToSetupNodeResponseWithoutQuery()
			{
		
			}
		
	public boolean hasNodeResponse()
	{
		return nodeResponse != null;
	}

	public FwCoreNodeResponseItf getNodeResponseAfterHas()
	{
		return nodeResponse;
	}

	protected void setNodeResponse(FwCoreNodeResponseItf nodeResponse)
	{
		this.nodeResponse = nodeResponse;
	}

	protected FwCoreNodeRequestItf getNodeRequest()
	{
		return nodeRequest;
	}

	protected boolean matches(String query, String list)
	{
		String[] items = list.split(",");

		for(String item: items)
		{
			if (item.equals(query))
			{
				return true;
			}
		}

		return false;
	}

	protected FwCoreNodeResponseItf buildNodeResponse()
	{
		return nodeResponseByNodeRequestBuilder.build(
			getNodeRequest()
		);
	}

	protected String buildMenuText(Map<String, String> choices)
	{
		String r = "";

		for (Map.Entry<String, String> entry: choices.entrySet())
		{
			r += "<p><span class=\"verb\">" + entry.getKey() + "</span>) " + entry.getValue() + "</p>";
		}

		return r;
	}

	protected Map<String, String> instantiateMenu()
	{
		return new HashMap<String, String>();
	}

	protected String getVariable(FwCoreNodeResponseItf nodeResponse, String key)
	{
		FwCoreStatusItf status = nodeResponse.getStatus();
		Map<String, String> variables = status.getVariables();

		return variables.get(key);
	}

	protected FwCoreNodeResponseItf removeVariable(FwCoreNodeResponseItf nodeResponse, String key)
	{
		FwCoreStatusItf status = nodeResponse.getStatus();
		Map<String, String> variables = status.getVariables();

		variables.remove(key);

		return nodeResponse;
	}

	protected FwCoreNodeResponseItf putVariable(FwCoreNodeResponseItf nodeResponse, String key, String value)
	{
		FwCoreStatusItf status = nodeResponse.getStatus();
		Map<String, String> variables = status.getVariables();

		variables.put(
			key,
			value
		);

		status.setVariables(variables);
		nodeResponse.setStatus(status);

		return nodeResponse;
	}

	protected FwCoreNodeResponseItf nodeChange(FwCoreNodeResponseItf nodeResponse, String nodeId)
	{
		return nodeBrowser.change(
			nodeResponse,
			nodeId
		);
	}

	protected FwCoreNodeResponseItf nodePush(FwCoreNodeResponseItf nodeResponse, String nodeId)
	{
		return nodeBrowser.push(
			nodeResponse,
			nodeId
		);
	}

	protected FwCoreNodeResponseItf nodePop(FwCoreNodeResponseItf nodeResponse)
	{
		return nodeBrowser.pop(
			nodeResponse
		);
	}

}
