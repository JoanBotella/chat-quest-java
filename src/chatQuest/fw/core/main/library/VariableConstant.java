package chatQuest.fw.core.main.library;

public final class
	VariableConstant
{

	public final static String LAST_MENU_QUERY = "FwCore_lastMenuQuery";

	public final static String LAST_MENU_SIZE = "FwCore_lastMenuSize";

	public final static String SNAPSHOT_NAME_KEY = "FwCore_snapshotNameKey";

	public final static String SNAPSHOT_MENU_INDEX = "FwCore_snapshotMenuIndex";

}
