package chatQuest.fw.core.main.library.serviceContainerComponent;

import chatQuest.fw.core.main.service.app.FwCoreAppItf;
import chatQuest.fw.core.main.service.appResponseByNodeResponseBuilder.FwCoreAppResponseByNodeResponseBuilderItf;
import chatQuest.fw.core.main.service.bodyBuilderBySnapshotCreator.FwCoreBodyBuilderBySnapshotCreatorItf;
import chatQuest.fw.core.main.service.bodyBuilderBySnapshotDeleter.FwCoreBodyBuilderBySnapshotDeleterItf;
import chatQuest.fw.core.main.service.bodyBuilderBySnapshotLoader.FwCoreBodyBuilderBySnapshotLoaderItf;
import chatQuest.fw.core.main.service.bodyBuilderBySnapshotOverwriter.FwCoreBodyBuilderBySnapshotOverwriterItf;
import chatQuest.fw.core.main.service.breadcrumbsCopier.FwCoreBreadcrumbsCopierItf;
import chatQuest.fw.core.main.service.configurationBuilder.FwCoreConfigurationBuilderItf;
import chatQuest.fw.core.main.service.configurationContainer.FwCoreConfigurationContainerItf;
import chatQuest.fw.core.main.service.fileBuilderBySnapshotMetadatasMenuQuery.FwCoreFileBuilderBySnapshotMetadatasMenuQueryItf;
import chatQuest.fw.core.main.service.fileLineReader.FwCoreFileLineReaderItf;
import chatQuest.fw.core.main.service.fileWriter.FwCoreFileWriterItf;
import chatQuest.fw.core.main.service.settingsCopier.FwCoreSettingsCopierItf;
import chatQuest.fw.core.main.service.settingsJsonizer.FwCoreSettingsJsonizerItf;
import chatQuest.fw.core.main.service.settingsUnjsonizer.FwCoreSettingsUnjsonizerItf;
import chatQuest.fw.core.main.service.snapshotContentBuilderAndToFileWriter.FwCoreSnapshotContentBuilderAndToFileWriterItf;
import chatQuest.fw.core.main.service.snapshotCreator.FwCoreSnapshotCreatorItf;
import chatQuest.fw.core.main.service.snapshotDeleter.FwCoreSnapshotDeleterItf;
import chatQuest.fw.core.main.service.snapshotFileBuilder.FwCoreSnapshotFileBuilderItf;
import chatQuest.fw.core.main.service.snapshotFileContentBuilder.FwCoreSnapshotFileContentBuilderItf;
import chatQuest.fw.core.main.service.snapshotFileParser.FwCoreSnapshotFileParserItf;
import chatQuest.fw.core.main.service.snapshotFilesBuilder.FwCoreSnapshotFilesBuilderItf;
import chatQuest.fw.core.main.service.snapshotLoader.FwCoreSnapshotLoaderItf;
import chatQuest.fw.core.main.service.snapshotMetadataJsonizer.FwCoreSnapshotMetadataJsonizerItf;
import chatQuest.fw.core.main.service.snapshotMetadataSerializer.FwCoreSnapshotMetadataSerializerItf;
import chatQuest.fw.core.main.service.snapshotMetadataUnjsonizer.FwCoreSnapshotMetadataUnjsonizerItf;
import chatQuest.fw.core.main.service.snapshotMetadataUnserializer.FwCoreSnapshotMetadataUnserializerItf;
import chatQuest.fw.core.main.service.snapshotMetadatasLister.FwCoreSnapshotMetadatasListerItf;
import chatQuest.fw.core.main.service.snapshotMetadatasMenuBuilder.FwCoreSnapshotMetadatasMenuBuilderItf;
import chatQuest.fw.core.main.service.snapshotOverwriter.FwCoreSnapshotOverwriterItf;
import chatQuest.fw.core.main.service.nextNodeRequestBuilder.FwCoreNextNodeRequestBuilderItf;
import chatQuest.fw.core.main.service.nodeBrowser.FwCoreNodeBrowserItf;
import chatQuest.fw.core.main.service.nodeRequestByAppRequestBuilder.FwCoreNodeRequestByAppRequestBuilderItf;
import chatQuest.fw.core.main.service.nodeRequestProcessor.FwCoreNodeRequestProcessorItf;
import chatQuest.fw.core.main.service.nodeResponseByNodeRequestBuilder.FwCoreNodeResponseByNodeRequestBuilderItf;
import chatQuest.fw.core.main.service.randomStringGenerator.FwCoreRandomStringGeneratorItf;
import chatQuest.fw.core.main.service.startingStatusBuilder.FwCoreStartingStatusBuilderItf;
import chatQuest.fw.core.main.service.statusCopier.FwCoreStatusCopierItf;
import chatQuest.fw.core.main.service.statusJsonizer.FwCoreStatusJsonizerItf;
import chatQuest.fw.core.main.service.statusSerializer.FwCoreStatusSerializerItf;
import chatQuest.fw.core.main.service.statusUnjsonizer.FwCoreStatusUnjsonizerItf;
import chatQuest.fw.core.main.service.statusUnserializer.FwCoreStatusUnserializerItf;
import chatQuest.fw.core.translation.service.translationContainer.FwCoreTranslationContainerItf;
import chatQuest.fw.core.main.service.variablesCopier.FwCoreVariablesCopierItf;

public interface FwCoreServiceContainerComponentItf
{

	public FwCoreAppItf getFwCoreApp();

	public FwCoreAppResponseByNodeResponseBuilderItf getFwCoreAppResponseByNodeResponseBuilder();

	public FwCoreBreadcrumbsCopierItf getFwCoreBreadcrumbsCopier();

	public FwCoreSettingsCopierItf getFwCoreSettingsCopier();

	public FwCoreNextNodeRequestBuilderItf getFwCoreNextNodeRequestBuilder();

	public FwCoreNodeBrowserItf getFwCoreNodeBrowser();

	public FwCoreNodeRequestByAppRequestBuilderItf getFwCoreNodeRequestByAppRequestBuilder();

	public FwCoreNodeRequestProcessorItf getFwCoreNodeRequestProcessor();

	public FwCoreNodeResponseByNodeRequestBuilderItf getFwCoreNodeResponseByNodeRequestBuilder();

	public FwCoreStartingStatusBuilderItf getFwCoreStartingStatusBuilder();

	public FwCoreStatusCopierItf getFwCoreStatusCopier();

	public FwCoreTranslationContainerItf getFwCoreTranslationContainer();

	public FwCoreVariablesCopierItf getFwCoreVariablesCopier();

	public FwCoreSettingsJsonizerItf getFwCoreSettingsJsonizer();

	public FwCoreStatusJsonizerItf getFwCoreStatusJsonizer();

	public FwCoreStatusSerializerItf getFwCoreStatusSerializer();

	public FwCoreStatusUnserializerItf getFwCoreStatusUnserializer();

	public FwCoreStatusUnjsonizerItf getFwCoreStatusUnjsonizer();

	public FwCoreSettingsUnjsonizerItf getFwCoreSettingsUnjsonizer();

	public FwCoreConfigurationBuilderItf getFwCoreConfigurationBuilder();

	public FwCoreConfigurationContainerItf getFwCoreConfigurationContainer();

	public FwCoreSnapshotMetadatasListerItf getFwCoreSnapshotMetadatasLister();

	public FwCoreSnapshotMetadataSerializerItf getFwCoreSnapshotMetadataSerializer();

	public FwCoreSnapshotMetadataUnserializerItf getFwCoreSnapshotMetadataUnserializer();

	public FwCoreSnapshotMetadataJsonizerItf getFwCoreSnapshotMetadataJsonizer();

	public FwCoreSnapshotMetadataUnjsonizerItf getFwCoreSnapshotMetadataUnjsonizer();

	public FwCoreSnapshotCreatorItf getFwCoreSnapshotCreator();

	public FwCoreRandomStringGeneratorItf getFwCoreRandomStringGenerator();

	public FwCoreSnapshotFileParserItf getFwCoreSnapshotFileParser();

	public FwCoreFileLineReaderItf getFwCoreFileLineReader();

	public FwCoreSnapshotMetadatasMenuBuilderItf getFwCoreSnapshotMetadatasMenuBuilder();

	public FwCoreFileBuilderBySnapshotMetadatasMenuQueryItf getFwCoreFileBuilderBySnapshotMetadatasMenuQuery();

	public FwCoreSnapshotFilesBuilderItf getFwCoreSnapshotFilesBuilder();

	public FwCoreSnapshotDeleterItf getFwCoreSnapshotDeleter();

	public FwCoreSnapshotOverwriterItf getFwCoreSnapshotOverwriter();

	public FwCoreSnapshotFileContentBuilderItf getFwCoreSnapshotFileContentBuilder();

	public FwCoreFileWriterItf getFwCoreFileWriter();

	public FwCoreSnapshotLoaderItf getFwCoreSnapshotLoader();

	public FwCoreSnapshotContentBuilderAndToFileWriterItf getFwCoreSnapshotContentBuilderAndToFileWriter();

	public FwCoreSnapshotFileBuilderItf getFwCoreSnapshotFileBuilder();

	public FwCoreBodyBuilderBySnapshotLoaderItf getFwCoreBodyBuilderBySnapshotLoader();

	public FwCoreBodyBuilderBySnapshotDeleterItf getFwCoreBodyBuilderBySnapshotDeleter();

	public FwCoreBodyBuilderBySnapshotOverwriterItf getFwCoreBodyBuilderBySnapshotOverwriter();

	public FwCoreBodyBuilderBySnapshotCreatorItf getFwCoreBodyBuilderBySnapshotCreator();

}
