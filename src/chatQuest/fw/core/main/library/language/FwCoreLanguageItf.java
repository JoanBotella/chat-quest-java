package chatQuest.fw.core.main.library.language;

public interface FwCoreLanguageItf
{

	public String getName();

	public String getCode();

}
