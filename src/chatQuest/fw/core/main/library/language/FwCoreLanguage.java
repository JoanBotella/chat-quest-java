package chatQuest.fw.core.main.library.language;

public final class FwCoreLanguage
	implements
		FwCoreLanguageItf
{

	private String name;
	private String code;

	public FwCoreLanguage(
		String name,
		String code
	)
	{
		this.name = name;
		this.code = code;
	}

	public String getName()
	{
		return name;
	}

	public String getCode()
	{
		return code;
	}

}
