package chatQuest.fw.core.main.library.language;

public final class FwCoreLanguageCode
{

	public final static String EN = "en";
	public final static String ES = "es";

}
