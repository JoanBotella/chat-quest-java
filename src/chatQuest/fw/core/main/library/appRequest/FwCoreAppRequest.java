package chatQuest.fw.core.main.library.appRequest;

import chatQuest.fw.core.main.library.status.FwCoreStatusItf;

public final class FwCoreAppRequest
	implements
		FwCoreAppRequestItf
{

	private FwCoreStatusItf status;
	private String query;

	public FwCoreAppRequest(
		FwCoreStatusItf status
	)
	{
		this.status = status;
	}

	public FwCoreStatusItf getStatus()
	{
		return status;
	}

	public void setStatus(FwCoreStatusItf status)
	{
		this.status = status;
	}

	public boolean hasQuery()
	{
		return query != null;
	}

	public String getQueryAfterHas()
	{
		return query;
	}

	public void setQuery(String query)
	{
		this.query = query;
	}

	public void unsetQuery()
	{
		query = null;
	}

}
