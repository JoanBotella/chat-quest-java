package chatQuest.fw.core.main.library.appRequest;

import chatQuest.fw.core.main.library.status.FwCoreStatusItf;

public interface
	FwCoreAppRequestItf
{

	public FwCoreStatusItf getStatus();

	public void setStatus(FwCoreStatusItf status);

	public boolean hasQuery();

	public String getQueryAfterHas();

	public void setQuery(String query);

	public void unsetQuery();

}
