package chatQuest.fw.core.main.library.configuration;

import java.io.File;

import chatQuest.fw.core.main.library.configuration.FwCoreConfigurationItf;

public final class
	FwCoreConfiguration
implements
	FwCoreConfigurationItf
{

	private File snapshotsDirectory;

	public FwCoreConfiguration(
		File snapshotsDirectory
	)
	{
		this.snapshotsDirectory = snapshotsDirectory;
	}

	public File getSnapshotsDirectory()
	{
		return snapshotsDirectory;
	}

}
