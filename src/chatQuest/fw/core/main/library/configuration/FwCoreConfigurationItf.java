package chatQuest.fw.core.main.library.configuration;

import java.io.File;

public interface
	FwCoreConfigurationItf
{

	public File getSnapshotsDirectory();

}
