package chatQuest.fw.core.main.library.appResponse;

import java.util.List;

import chatQuest.fw.core.main.library.slide.FwCoreSlideItf;
import chatQuest.fw.core.main.library.status.FwCoreStatusItf;

public interface FwCoreAppResponseItf
{

	public FwCoreStatusItf getStatus();

	public void setStatus(FwCoreStatusItf status);

	public List<FwCoreSlideItf> getSlides();

	public void setSlides(List<FwCoreSlideItf> slides);

	public boolean getEnd();

	public void setEnd(boolean end);

}
