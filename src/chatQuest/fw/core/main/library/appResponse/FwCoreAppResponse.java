package chatQuest.fw.core.main.library.appResponse;

import java.util.ArrayList;
import java.util.List;

import chatQuest.fw.core.main.library.slide.FwCoreSlideItf;
import chatQuest.fw.core.main.library.status.FwCoreStatusItf;

public final class FwCoreAppResponse
	implements
		FwCoreAppResponseItf
{

	private FwCoreStatusItf status;
	private List<FwCoreSlideItf> slides;
	private boolean end;

	public FwCoreAppResponse(
		FwCoreStatusItf status
	)
	{
		this.status = status;
		this.slides = new ArrayList<FwCoreSlideItf>();
		this.end = false;
	}

	public FwCoreStatusItf getStatus()
	{
		return status;
	}

	public void setStatus(FwCoreStatusItf status)
	{
		this.status = status;
	}

	public List<FwCoreSlideItf> getSlides()
	{
		return slides;
	}

	public void setSlides(List<FwCoreSlideItf> slides)
	{
		this.slides = slides;
	}

	public boolean getEnd()
	{
		return end;
	}

	public void setEnd(boolean end)
	{
		this.end = end;
	}

}
