package chatQuest.fw.core.main.library.snapshotMetadata;

import java.time.OffsetDateTime;

import chatQuest.fw.core.main.library.snapshotMetadata.FwCoreSnapshotMetadataItf;

public final class
	FwCoreSnapshotMetadata
implements
	FwCoreSnapshotMetadataItf
{

	private String name;
	private OffsetDateTime dateTime;

	public FwCoreSnapshotMetadata(
		String name,
		OffsetDateTime dateTime
	)
	{
		this.name = name;
		this.dateTime = dateTime;
	}

	public String getName()
	{
		return name;
	}

	public OffsetDateTime getDateTime()
	{
		return dateTime;
	}

}
