package chatQuest.fw.core.main.library.snapshotMetadata;

import java.time.OffsetDateTime;

public interface
	FwCoreSnapshotMetadataItf
{

	public String getName();

	public OffsetDateTime getDateTime();

}
