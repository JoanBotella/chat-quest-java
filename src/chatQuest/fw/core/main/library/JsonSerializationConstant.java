package chatQuest.fw.core.main.library;

public final class
	JsonSerializationConstant
{

	public final static String SETTINGS_LANGUAGE_CODE_KEY = "languageCode";

	public final static String SNAPSHOTS_METADATA_NAME_KEY = "name";
	public final static String SNAPSHOTS_METADATA_DATE_TIME_KEY = "dateTime";

	public final static String STATUS_BREADCRUMBS_KEY = "breadcrumbs";
	public final static String STATUS_SETTINGS_KEY = "settings";
	public final static String STATUS_VARIABLES_KEY = "variables";

}
