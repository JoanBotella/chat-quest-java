package chatQuest.fw.core.main.service.fileLineReader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import chatQuest.fw.core.main.service.fileLineReader.FwCoreFileLineReaderItf;

public final class
	FwCoreFileLineReader
implements
	FwCoreFileLineReaderItf
{

	private List<Integer> errors;
	private String line;

	public FwCoreFileLineReader(
	)
	{
		errors = buildErrors();
	}

		private List<Integer> buildErrors()
		{
			return new ArrayList<Integer>();
		}

	public List<Integer> getErrors()
	{
		return errors;
	}

	public void read(File file, int lineNumber)
	{
		errors.clear();

		line = null;

		FileReader fileReader;

		try
		{
			fileReader = new FileReader(file);
		}
		catch (Exception e)
		{
			errors.add(
				FwCoreFileLineReaderErrorConstant.ERROR_FILE_COULD_NOT_BE_FOUND
			);
			return;
		}

		BufferedReader bufferedReader = new BufferedReader(fileReader);

		String l;

		for (int i = 0; i <= lineNumber; i++)
		{
			try
			{
				l = bufferedReader.readLine();
			}
			catch (Exception e)
			{
				errors.add(
					FwCoreFileLineReaderErrorConstant.ERROR_FILE_COULD_NOT_BE_READ
				);
				break;
			}

			if (l == null)
			{
				break;
			}

			if (i < lineNumber)
			{
				continue;
			}

			if (i == lineNumber)
			{
				line = l;
				break;
			}
		}

		try
		{
			bufferedReader.close();
		}
		catch (Exception e)
		{
			errors.add(
				FwCoreFileLineReaderErrorConstant.ERROR_FILE_COULD_NOT_BE_CLOSED
			);
		}
	}

	public boolean hasLine()
	{
		return line != null;
	}

	public String getLineAfterHas()
	{
		return line;
	}

}
