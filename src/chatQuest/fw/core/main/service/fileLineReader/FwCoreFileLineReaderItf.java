package chatQuest.fw.core.main.service.fileLineReader;

import java.io.File;
import java.util.List;

public interface
	FwCoreFileLineReaderItf
{

	public List<Integer> getErrors();

	public void read(File file, int lineNumber);

	public boolean hasLine();

	public String getLineAfterHas();

}
