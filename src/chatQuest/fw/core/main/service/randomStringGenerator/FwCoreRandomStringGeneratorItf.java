package chatQuest.fw.core.main.service.randomStringGenerator;

public interface
	FwCoreRandomStringGeneratorItf
{

	public String generate(String characters, int length);

}
