package chatQuest.fw.core.main.service.randomStringGenerator;

import java.util.Random;

import chatQuest.fw.core.main.service.randomStringGenerator.FwCoreRandomStringGeneratorItf;

public final class
	FwCoreRandomStringGenerator
implements
	FwCoreRandomStringGeneratorItf
{

	public String generate(String characters, int length)
	{
		Random rand = new Random();
		String r = "";
		for (int i = 0; i < length; i++)
		{
			r += characters.charAt(
				rand.nextInt(
					characters.length()
				)
			);
		}
		return r;
	}

}
