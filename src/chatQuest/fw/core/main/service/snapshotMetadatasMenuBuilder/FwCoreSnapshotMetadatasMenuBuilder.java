package chatQuest.fw.core.main.service.snapshotMetadatasMenuBuilder;

import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;

import chatQuest.fw.core.main.library.snapshotMetadata.FwCoreSnapshotMetadataItf;
import chatQuest.fw.core.main.service.snapshotMetadatasLister.FwCoreSnapshotMetadatasListerItf;
import chatQuest.fw.core.main.service.snapshotMetadatasMenuBuilder.FwCoreSnapshotMetadatasMenuBuilderItf;

public final class
	FwCoreSnapshotMetadatasMenuBuilder
implements
	FwCoreSnapshotMetadatasMenuBuilderItf
{

	private static final String BACK_QUERY = "1";

	private FwCoreSnapshotMetadatasListerItf snapshotMetadatasLister;

	public FwCoreSnapshotMetadatasMenuBuilder(
		FwCoreSnapshotMetadatasListerItf snapshotMetadatasLister
	)
	{
		this.snapshotMetadatasLister = snapshotMetadatasLister;
	}

	public String getBackQuery()
	{
		return BACK_QUERY;
	}

	public Map<String, String> build(Map<String, String> menu, String backText)
	{
		menu.put(
			BACK_QUERY,
			backText
		);

		DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		
		List<FwCoreSnapshotMetadataItf> snapshotMetadatas = snapshotMetadatasLister.list();
		int size = snapshotMetadatas.size();

		FwCoreSnapshotMetadataItf metadata;
		String dateTimeString;

		for (int index = 0; index < size; index++)
		{
			metadata = snapshotMetadatas.get(index);
			dateTimeString = dateTimeFormatter.format(
				metadata.getDateTime()
			);

			menu.put(
				String.valueOf(index + 2),
				"[" + dateTimeString + "] " + metadata.getName()
			);
		}

		return menu;
	}

}
