package chatQuest.fw.core.main.service.snapshotMetadatasMenuBuilder;

import java.util.Map;

public interface
	FwCoreSnapshotMetadatasMenuBuilderItf
{

	public String getBackQuery();

	public Map<String, String> build(Map<String, String> menu, String backText);

}
