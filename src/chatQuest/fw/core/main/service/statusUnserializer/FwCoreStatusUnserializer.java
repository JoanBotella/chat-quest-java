package chatQuest.fw.core.main.service.statusUnserializer;

import chatQuest.fw.core.main.library.status.FwCoreStatusItf;
import chatQuest.fw.core.main.service.statusUnjsonizer.FwCoreStatusUnjsonizerItf;
import chatQuest.fw.core.main.service.statusUnserializer.FwCoreStatusUnserializerItf;
import org.json.JSONObject;
import org.json.JSONException;

public final class
	FwCoreStatusUnserializer
implements
	FwCoreStatusUnserializerItf
{

	private FwCoreStatusUnjsonizerItf statusUnjsonizer;

	private FwCoreStatusItf status;

	public FwCoreStatusUnserializer(
		FwCoreStatusUnjsonizerItf statusUnjsonizer
	)
	{
		this.statusUnjsonizer = statusUnjsonizer;
	}

	public boolean hasStatus()
	{
		return status != null;
	}

	public FwCoreStatusItf getStatusAfterHas()
	{
		return status;
	}

	public void unserialize(String statusString)
	{
		status = null;
		JSONObject statusJsonObject;

		try
		{
			statusJsonObject = new JSONObject(statusString);
		}
		catch (JSONException e)
		{
			return;
		}

		statusUnjsonizer.unjsonize(statusJsonObject);

		if (statusUnjsonizer.hasStatus())
		{
			status = statusUnjsonizer.getStatusAfterHas();
		}
	}

}
