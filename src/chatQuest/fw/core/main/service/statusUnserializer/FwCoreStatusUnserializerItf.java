package chatQuest.fw.core.main.service.statusUnserializer;

import chatQuest.fw.core.main.library.status.FwCoreStatusItf;

public interface
	FwCoreStatusUnserializerItf
{

	public void unserialize(String statusString);

	public boolean hasStatus();

	public FwCoreStatusItf getStatusAfterHas();

}
