package chatQuest.fw.core.main.service.startingStatusBuilder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import chatQuest.fw.core.main.library.settings.FwCoreSettings;
import chatQuest.fw.core.main.library.settings.FwCoreSettingsItf;
import chatQuest.fw.core.main.library.status.FwCoreStatus;
import chatQuest.fw.core.main.library.status.FwCoreStatusItf;

public final class FwCoreStartingStatusBuilder
	implements
		FwCoreStartingStatusBuilderItf
{

	private String nodeId;
	private String languageCode;

	public FwCoreStartingStatusBuilder(
		String nodeId,
		String languageCode
	)
	{
		this.nodeId = nodeId;
		this.languageCode = languageCode;
	}

	public FwCoreStatusItf build()
	{
		return new FwCoreStatus(
			buildBreadcrumbs(),
			buildSettings(),
			buildVariables()
		);
	}

		private List<String> buildBreadcrumbs()
		{
			List<String> breadcrumbs = new ArrayList<String>();
			breadcrumbs.add(nodeId);
			return breadcrumbs;
		}

		private FwCoreSettingsItf buildSettings()
		{
			return new FwCoreSettings(
				languageCode
			);
		}

		private Map<String, String> buildVariables()
		{
			return new HashMap<String, String>();
		}

}
