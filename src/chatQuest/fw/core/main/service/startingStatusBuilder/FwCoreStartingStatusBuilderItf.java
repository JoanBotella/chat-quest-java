package chatQuest.fw.core.main.service.startingStatusBuilder;

import chatQuest.fw.core.main.library.status.FwCoreStatusItf;

public interface FwCoreStartingStatusBuilderItf
{

	public FwCoreStatusItf build();

}
