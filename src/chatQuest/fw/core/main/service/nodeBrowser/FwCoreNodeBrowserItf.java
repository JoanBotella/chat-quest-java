package chatQuest.fw.core.main.service.nodeBrowser;

import chatQuest.fw.core.main.library.nodeResponse.FwCoreNodeResponseItf;

public interface FwCoreNodeBrowserItf
{

	public FwCoreNodeResponseItf change(FwCoreNodeResponseItf nodeResponse, String nodeId);

	public FwCoreNodeResponseItf push(FwCoreNodeResponseItf nodeResponse, String nodeId);

	public FwCoreNodeResponseItf pop(FwCoreNodeResponseItf nodeResponse);

}
