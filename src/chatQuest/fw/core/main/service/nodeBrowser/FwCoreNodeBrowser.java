package chatQuest.fw.core.main.service.nodeBrowser;

import java.util.List;

import chatQuest.fw.core.main.library.nodeResponse.FwCoreNodeResponseItf;
import chatQuest.fw.core.main.library.status.FwCoreStatusItf;

public final class FwCoreNodeBrowser
	implements
		FwCoreNodeBrowserItf
{

	public FwCoreNodeResponseItf change(FwCoreNodeResponseItf nodeResponse, String nodeId)
	{
		FwCoreStatusItf status = nodeResponse.getStatus();
		List<String> breadcrumbs = status.getBreadcrumbs();

		breadcrumbs.set(
			breadcrumbs.size() - 1,
			nodeId
		);

		status.setBreadcrumbs(breadcrumbs);
		nodeResponse.setStatus(status);

		nodeResponse.setProcessAgain(true);

		return nodeResponse;
	}

	public FwCoreNodeResponseItf push(FwCoreNodeResponseItf nodeResponse, String nodeId)
	{
		FwCoreStatusItf status = nodeResponse.getStatus();
		List<String> breadcrumbs = status.getBreadcrumbs();

		breadcrumbs.add(nodeId);

		status.setBreadcrumbs(breadcrumbs);
		nodeResponse.setStatus(status);

		nodeResponse.setProcessAgain(true);

		return nodeResponse;
	}

	public FwCoreNodeResponseItf pop(FwCoreNodeResponseItf nodeResponse)
	{
		FwCoreStatusItf status = nodeResponse.getStatus();
		List<String> breadcrumbs = status.getBreadcrumbs();
		int size = breadcrumbs.size();

		if (size <= 1)
		{
			throw new RuntimeException("The breadcrumbs cannot be popped to emptyness.");
		}

		breadcrumbs.remove(size - 1);

		status.setBreadcrumbs(breadcrumbs);
		nodeResponse.setStatus(status);

		nodeResponse.setProcessAgain(true);

		return nodeResponse;
	}

}
