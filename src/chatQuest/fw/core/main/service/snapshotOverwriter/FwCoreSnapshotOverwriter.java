package chatQuest.fw.core.main.service.snapshotOverwriter;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import chatQuest.fw.core.main.library.status.FwCoreStatusItf;
import chatQuest.fw.core.main.service.fileBuilderBySnapshotMetadatasMenuQuery.FwCoreFileBuilderBySnapshotMetadatasMenuQueryItf;
import chatQuest.fw.core.main.service.snapshotContentBuilderAndToFileWriter.FwCoreSnapshotContentBuilderAndToFileWriterErrorConstant;
import chatQuest.fw.core.main.service.snapshotContentBuilderAndToFileWriter.FwCoreSnapshotContentBuilderAndToFileWriterItf;
import chatQuest.fw.core.main.service.snapshotOverwriter.FwCoreSnapshotOverwriterItf;

public final class
	FwCoreSnapshotOverwriter
implements
	FwCoreSnapshotOverwriterItf
{

	private FwCoreFileBuilderBySnapshotMetadatasMenuQueryItf fileBuilderBySnapshotMetadatasMenuQuery;
	private FwCoreSnapshotContentBuilderAndToFileWriterItf snapshotContentBuilderAndToFileWriter;

	private List<Integer> errors;
	private File file;

	public FwCoreSnapshotOverwriter(
		FwCoreFileBuilderBySnapshotMetadatasMenuQueryItf fileBuilderBySnapshotMetadatasMenuQuery,
		FwCoreSnapshotContentBuilderAndToFileWriterItf snapshotContentBuilderAndToFileWriter
	)
	{
		this.fileBuilderBySnapshotMetadatasMenuQuery = fileBuilderBySnapshotMetadatasMenuQuery;
		this.snapshotContentBuilderAndToFileWriter = snapshotContentBuilderAndToFileWriter;

		errors = buildErrors();
	}

		public List<Integer> buildErrors()
		{
			return new ArrayList<Integer>();
		}

	public List<Integer> getErrors()
	{
		return errors;
	}

	public void overwrite(String snapshotMetadatasMenuQuery, String snapshotName, FwCoreStatusItf status)
	{
		errors.clear();
		file = null;

		tryToBuildFile(snapshotMetadatasMenuQuery);

		if (!errors.isEmpty())
		{
			return;
		}

		tryToBuildSnapshotContentAndWriteFile(
			snapshotName,
			status
		);

		file = null;
	}

		private void tryToBuildFile(String snapshotMetadatasMenuQuery)
		{
			fileBuilderBySnapshotMetadatasMenuQuery.build(snapshotMetadatasMenuQuery);

			if (fileBuilderBySnapshotMetadatasMenuQuery.hasFile())
			{
				file = fileBuilderBySnapshotMetadatasMenuQuery.getFileAfterHas();
				return;
			}

			errors.add(
				FwCoreSnapshotOverwriterErrorConstant.ERROR_QUERY_DOES_NOT_MATCH_A_FILE
			);
		}

		private void tryToBuildSnapshotContentAndWriteFile(String snapshotName, FwCoreStatusItf status)
		{
			snapshotContentBuilderAndToFileWriter.buildAndWrite(
				file,
				snapshotName,
				status
			);

			List<Integer> snapshotContentBuilderAndToFileWriterErrors = snapshotContentBuilderAndToFileWriter.getErrors();

			if (snapshotContentBuilderAndToFileWriterErrors.isEmpty())
			{
				return;
			}

			for (Integer snapshotContentBuilderAndToFileWriterError : snapshotContentBuilderAndToFileWriterErrors)
			{
				switch (snapshotContentBuilderAndToFileWriterError)
				{
					case FwCoreSnapshotContentBuilderAndToFileWriterErrorConstant.ERROR_SNAPSHOT_FILE_COULD_NOT_BE_WRITTEN:
						errors.add(
							FwCoreSnapshotOverwriterErrorConstant.ERROR_SNAPSHOT_FILE_COULD_NOT_BE_WRITTEN
						);
						break;
					case FwCoreSnapshotContentBuilderAndToFileWriterErrorConstant.ERROR_SNAPSHOT_FILE_COULD_NOT_BE_CLOSED:
						errors.add(
							FwCoreSnapshotOverwriterErrorConstant.ERROR_SNAPSHOT_FILE_COULD_NOT_BE_CLOSED
						);
						break;
				}
			}
		}

}
