package chatQuest.fw.core.main.service.snapshotOverwriter;

import java.util.List;

import chatQuest.fw.core.main.library.status.FwCoreStatusItf;

public interface
	FwCoreSnapshotOverwriterItf
{

	public List<Integer> getErrors();

	public void overwrite(String snapshotMetadatasMenuQuery, String snapshotName, FwCoreStatusItf status);

}
