package chatQuest.fw.core.main.service.snapshotOverwriter;

public final class
	FwCoreSnapshotOverwriterErrorConstant
{

	public static final int ERROR_QUERY_DOES_NOT_MATCH_A_FILE = 0;

	public static final int ERROR_SNAPSHOT_FILE_COULD_NOT_BE_WRITTEN = 1;

	public static final int ERROR_SNAPSHOT_FILE_COULD_NOT_BE_CLOSED = 2;

}
