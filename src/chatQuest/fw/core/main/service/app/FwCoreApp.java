package chatQuest.fw.core.main.service.app;

import java.util.ArrayList;
import java.util.List;

import chatQuest.fw.core.main.library.appRequest.FwCoreAppRequestItf;
import chatQuest.fw.core.main.library.appResponse.FwCoreAppResponseItf;
import chatQuest.fw.core.main.library.nodeRequest.FwCoreNodeRequestItf;
import chatQuest.fw.core.main.library.nodeResponse.FwCoreNodeResponseItf;
import chatQuest.fw.core.main.library.slide.FwCoreSlideItf;
import chatQuest.fw.core.main.service.appResponseByNodeResponseBuilder.FwCoreAppResponseByNodeResponseBuilderItf;
import chatQuest.fw.core.main.service.nextNodeRequestBuilder.FwCoreNextNodeRequestBuilderItf;
import chatQuest.fw.core.main.service.nodeRequestByAppRequestBuilder.FwCoreNodeRequestByAppRequestBuilderItf;
import chatQuest.fw.core.main.service.nodeRequestProcessor.FwCoreNodeRequestProcessorItf;

public final class FwCoreApp
	implements
		FwCoreAppItf
{

	private FwCoreNodeRequestByAppRequestBuilderItf nodeRequestByAppRequestBuilder;
	private FwCoreNodeRequestProcessorItf nodeRequestProcessor;
	private FwCoreNextNodeRequestBuilderItf nextNodeRequestBuilder;
	private FwCoreAppResponseByNodeResponseBuilderItf appResponseByNodeResponseBuilder;

	public FwCoreApp(
		FwCoreNodeRequestByAppRequestBuilderItf nodeRequestByAppRequestBuilder,
		FwCoreNodeRequestProcessorItf nodeRequestProcessor,
		FwCoreNextNodeRequestBuilderItf nextNodeRequestBuilder,
		FwCoreAppResponseByNodeResponseBuilderItf appResponseByNodeResponseBuilder
	)
	{
		this.nodeRequestByAppRequestBuilder = nodeRequestByAppRequestBuilder;
		this.nodeRequestProcessor = nodeRequestProcessor;
		this.nextNodeRequestBuilder = nextNodeRequestBuilder;
		this.appResponseByNodeResponseBuilder = appResponseByNodeResponseBuilder;
	}

	public FwCoreAppResponseItf run(FwCoreAppRequestItf appRequest)
	{
		FwCoreNodeRequestItf nodeRequest = nodeRequestByAppRequestBuilder.build(appRequest);
		List<FwCoreSlideItf> slidesAccumulator = new ArrayList<FwCoreSlideItf>();
		FwCoreNodeResponseItf nodeResponse;

		while (true)
		{
			nodeResponse = nodeRequestProcessor.process(nodeRequest);

			slidesAccumulator.addAll(
				nodeResponse.getSlides()
			);

			if (!nodeResponse.getProcessAgain())
			{
				break;
			}

			nodeRequest = nextNodeRequestBuilder.build(nodeResponse);
		}

		FwCoreAppResponseItf appResponse = appResponseByNodeResponseBuilder.build(nodeResponse);
		appResponse.setSlides(slidesAccumulator);
		return appResponse;
	}

}
