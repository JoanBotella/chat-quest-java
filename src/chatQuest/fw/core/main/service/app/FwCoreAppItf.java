package chatQuest.fw.core.main.service.app;

import chatQuest.fw.core.main.library.appRequest.FwCoreAppRequestItf;
import chatQuest.fw.core.main.library.appResponse.FwCoreAppResponseItf;

public interface FwCoreAppItf
{

	public FwCoreAppResponseItf run(FwCoreAppRequestItf appRequest);

}
