package chatQuest.fw.core.main.service.nodeRequestByAppRequestBuilder;

import chatQuest.fw.core.main.library.appRequest.FwCoreAppRequestItf;
import chatQuest.fw.core.main.library.nodeRequest.FwCoreNodeRequestItf;

public interface FwCoreNodeRequestByAppRequestBuilderItf
{

	public FwCoreNodeRequestItf build(FwCoreAppRequestItf appRequest);

}
