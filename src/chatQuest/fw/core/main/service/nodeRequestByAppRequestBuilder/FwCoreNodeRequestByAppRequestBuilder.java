package chatQuest.fw.core.main.service.nodeRequestByAppRequestBuilder;

import chatQuest.fw.core.main.library.appRequest.FwCoreAppRequestItf;
import chatQuest.fw.core.main.library.nodeRequest.FwCoreNodeRequest;
import chatQuest.fw.core.main.library.nodeRequest.FwCoreNodeRequestItf;

public final class FwCoreNodeRequestByAppRequestBuilder
	implements
		FwCoreNodeRequestByAppRequestBuilderItf
{

	public FwCoreNodeRequestItf build(FwCoreAppRequestItf appRequest)
	{
		FwCoreNodeRequestItf nodeRequest = new FwCoreNodeRequest(
			appRequest.getStatus()
		);

		if (appRequest.hasQuery())
		{
			nodeRequest.setQuery(
				appRequest.getQueryAfterHas()
			);
		}

		return nodeRequest;
	}

}
