package chatQuest.fw.core.main.service.snapshotFileContentBuilder;

import java.time.OffsetDateTime;

import chatQuest.fw.core.main.library.snapshotMetadata.FwCoreSnapshotMetadata;
import chatQuest.fw.core.main.library.snapshotMetadata.FwCoreSnapshotMetadataItf;
import chatQuest.fw.core.main.library.status.FwCoreStatusItf;
import chatQuest.fw.core.main.service.snapshotFileContentBuilder.FwCoreSnapshotFileContentBuilderItf;
import chatQuest.fw.core.main.service.snapshotMetadataSerializer.FwCoreSnapshotMetadataSerializerItf;
import chatQuest.fw.core.main.service.statusSerializer.FwCoreStatusSerializerItf;

public final class
	FwCoreSnapshotFileContentBuilder
implements
	FwCoreSnapshotFileContentBuilderItf
{

	private FwCoreSnapshotMetadataSerializerItf snapshotMetadataSerializer;
	private FwCoreStatusSerializerItf statusSerializer;

	public FwCoreSnapshotFileContentBuilder(
		FwCoreSnapshotMetadataSerializerItf snapshotMetadataSerializer,
		FwCoreStatusSerializerItf statusSerializer
	)
	{
		this.snapshotMetadataSerializer = snapshotMetadataSerializer;
		this.statusSerializer = statusSerializer;
	}

	public String build(String snapshotName, FwCoreStatusItf status)
	{
		FwCoreSnapshotMetadataItf snapshotMetadata = buildSnapshotMetadata(snapshotName);
		return 
			snapshotMetadataSerializer.serialize(snapshotMetadata)
			+ "\n" + statusSerializer.serialize(status)
		;
	}

		private FwCoreSnapshotMetadataItf buildSnapshotMetadata(String name)
		{
			return new FwCoreSnapshotMetadata(
				name,
				OffsetDateTime.now()
			);
		}

}
