package chatQuest.fw.core.main.service.snapshotFileContentBuilder;

import chatQuest.fw.core.main.library.status.FwCoreStatusItf;

public interface
	FwCoreSnapshotFileContentBuilderItf
{

	public String build(String snapshotName, FwCoreStatusItf status);

}
