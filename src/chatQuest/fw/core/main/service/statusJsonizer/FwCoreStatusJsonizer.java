package chatQuest.fw.core.main.service.statusJsonizer;

import org.json.JSONObject;

import chatQuest.fw.core.main.library.JsonSerializationConstant;
import chatQuest.fw.core.main.library.status.FwCoreStatusItf;
import chatQuest.fw.core.main.service.settingsJsonizer.FwCoreSettingsJsonizerItf;
import chatQuest.fw.core.main.service.statusJsonizer.FwCoreStatusJsonizerItf;

public final class
	FwCoreStatusJsonizer
implements
	FwCoreStatusJsonizerItf
{

	FwCoreSettingsJsonizerItf settingsJsonizer;

	FwCoreStatusItf status;
	JSONObject object;

	public FwCoreStatusJsonizer(
		FwCoreSettingsJsonizerItf settingsJsonizer
	)
	{
		this.settingsJsonizer = settingsJsonizer;
	}

	public JSONObject jsonize(FwCoreStatusItf status)
	{
		this.status = status;
		object = new JSONObject();

		putBreadcrumbs();

		putSettings();

		putVariables();

		this.status = null;
		JSONObject r = object;
		object = null;

		return r;
	}

		private void putBreadcrumbs()
		{
			object.put(
				JsonSerializationConstant.STATUS_BREADCRUMBS_KEY,
				status.getBreadcrumbs()
			);
		}

		private void putSettings()
		{
			object.put(
				JsonSerializationConstant.STATUS_SETTINGS_KEY,
				settingsJsonizer.jsonize(status.getSettings())
			);
		}

		private void putVariables()
		{
			object.put(
				JsonSerializationConstant.STATUS_VARIABLES_KEY,
				status.getVariables()
			);
		}

}
