package chatQuest.fw.core.main.service.statusJsonizer;

import org.json.JSONObject;
import chatQuest.fw.core.main.library.status.FwCoreStatusItf;

public interface
	FwCoreStatusJsonizerItf
{

	public JSONObject jsonize(FwCoreStatusItf status);

}
