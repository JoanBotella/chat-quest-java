package chatQuest.fw.core.main.service.breadcrumbsCopier;

import java.util.List;

public interface FwCoreBreadcrumbsCopierItf
{

	public List<String> copy(List<String> source);

}
