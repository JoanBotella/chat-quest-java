package chatQuest.fw.core.main.service.breadcrumbsCopier;

import java.util.ArrayList;
import java.util.List;

public final class FwCoreBreadcrumbsCopier
	implements
		FwCoreBreadcrumbsCopierItf
{

	public List<String> copy(List<String> source)
	{
		List<String> r = new ArrayList<String>();

		for (String breadcrumb: source)
		{
			r.add(breadcrumb);
		}

		return r;
	}

}
