package chatQuest.fw.core.main.service.snapshotMetadataSerializer;

import chatQuest.fw.core.main.library.snapshotMetadata.FwCoreSnapshotMetadataItf;
import chatQuest.fw.core.main.service.snapshotMetadataJsonizer.FwCoreSnapshotMetadataJsonizerItf;
import chatQuest.fw.core.main.service.snapshotMetadataSerializer.FwCoreSnapshotMetadataSerializerItf;

public final class
	FwCoreSnapshotMetadataSerializer
implements
	FwCoreSnapshotMetadataSerializerItf
{

	private FwCoreSnapshotMetadataJsonizerItf snapshotMetadataJsonizer;

	public FwCoreSnapshotMetadataSerializer(
		FwCoreSnapshotMetadataJsonizerItf snapshotMetadataJsonizer
	)
	{
		this.snapshotMetadataJsonizer = snapshotMetadataJsonizer;
	}

	public String serialize(FwCoreSnapshotMetadataItf snapshotMetadata)
	{
		return
			snapshotMetadataJsonizer
				.jsonize(snapshotMetadata)
				.toString()
		;
	}

}
