package chatQuest.fw.core.main.service.snapshotMetadataSerializer;

import chatQuest.fw.core.main.library.snapshotMetadata.FwCoreSnapshotMetadataItf;

public interface
	FwCoreSnapshotMetadataSerializerItf
{

	public String serialize(FwCoreSnapshotMetadataItf snapshotMetadata);

}
