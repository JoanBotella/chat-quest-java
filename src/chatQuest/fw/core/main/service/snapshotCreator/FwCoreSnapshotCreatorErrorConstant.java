package chatQuest.fw.core.main.service.snapshotCreator;

public final class
	FwCoreSnapshotCreatorErrorConstant
{

	public static final int ERROR_SNAPSHOT_DIRECTORY_COULD_NOT_BE_CREATED = 0;

	public static final int ERROR_SNAPSHOT_FILE_COULD_NOT_BE_CREATED = 1;

	public static final int ERROR_SNAPSHOT_FILE_COULD_NOT_BE_WRITTEN = 2;

	public static final int ERROR_SNAPSHOT_FILE_COULD_NOT_BE_CLOSED = 3;

	public static final int ERROR_SNAPSHOT_FILE_COULD_NOT_BE_DELETED = 4;

}
