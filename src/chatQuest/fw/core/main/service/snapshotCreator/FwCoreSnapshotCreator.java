package chatQuest.fw.core.main.service.snapshotCreator;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import chatQuest.fw.core.main.library.status.FwCoreStatusItf;
import chatQuest.fw.core.main.service.configurationContainer.FwCoreConfigurationContainerItf;
import chatQuest.fw.core.main.service.snapshotContentBuilderAndToFileWriter.FwCoreSnapshotContentBuilderAndToFileWriterErrorConstant;
import chatQuest.fw.core.main.service.snapshotContentBuilderAndToFileWriter.FwCoreSnapshotContentBuilderAndToFileWriterItf;
import chatQuest.fw.core.main.service.snapshotCreator.FwCoreSnapshotCreatorItf;
import chatQuest.fw.core.main.service.snapshotFileBuilder.FwCoreSnapshotFileBuilderItf;

public final class
	FwCoreSnapshotCreator
implements
	FwCoreSnapshotCreatorItf
{
	private FwCoreConfigurationContainerItf configurationContainer;
	private FwCoreSnapshotFileBuilderItf snapshotFileBuilder;
	private FwCoreSnapshotContentBuilderAndToFileWriterItf snapshotContentBuilderAndToFileWriter;

	private List<Integer> errors;
	private String snapshotName;
	private FwCoreStatusItf snapshotStatus;
	private File snapshotsDirectory;
	private File snapshotFile;

	public FwCoreSnapshotCreator(
		FwCoreConfigurationContainerItf configurationContainer,
		FwCoreSnapshotFileBuilderItf snapshotFileBuilder,
		FwCoreSnapshotContentBuilderAndToFileWriterItf snapshotContentBuilderAndToFileWriter
	)
	{
		this.configurationContainer = configurationContainer;
		this.snapshotFileBuilder = snapshotFileBuilder;
		this.snapshotContentBuilderAndToFileWriter = snapshotContentBuilderAndToFileWriter;

		errors = buildErrors();
	}

		private List<Integer> buildErrors()
		{
			return new ArrayList<Integer>();
		}

	public List<Integer> getErrors()
	{
		return errors;
	}

	public void create(String name, FwCoreStatusItf status)
	{
		snapshotName = name;
		snapshotStatus = status;

		errors.clear();

		run();

		snapshotName = null;
		snapshotStatus = null;
		snapshotsDirectory = null;
		snapshotFile = null;
	}

		private void run()
		{
			setupSnapshotsDirectory();
	
			createSnapshotsDirectoryIfNotExists();

			if (!errors.isEmpty())
			{
				return;
			}

			setupSnapshotFile();
	
			createSnapshotFile();

			if (!errors.isEmpty())
			{
				return;
			}

			tryToBuildSnapshotContentAndWriteFile();

			if (!errors.isEmpty())
			{
				deleteSnapshotFile();
				return;
			}
		}

			private void setupSnapshotsDirectory()
			{
				snapshotsDirectory = configurationContainer.get().getSnapshotsDirectory();
			}

			private void createSnapshotsDirectoryIfNotExists()
			{
				if (snapshotsDirectory.exists())
				{
					return;
				}

				if (snapshotsDirectory.mkdirs())
				{
					return;
				}

				errors.add(
					FwCoreSnapshotCreatorErrorConstant.ERROR_SNAPSHOT_DIRECTORY_COULD_NOT_BE_CREATED
				);
			}

			private void setupSnapshotFile()
			{
				snapshotFile = snapshotFileBuilder.build(snapshotsDirectory);
			}

			private void createSnapshotFile()
			{
				boolean success = false;

				try
				{
					success = snapshotFile.createNewFile();
				}
				catch (IOException e)
				{
				}

				if (!success)
				{
					errors.add(
						FwCoreSnapshotCreatorErrorConstant.ERROR_SNAPSHOT_FILE_COULD_NOT_BE_CREATED
					);
				}
			}

			private void tryToBuildSnapshotContentAndWriteFile()
			{
				snapshotContentBuilderAndToFileWriter.buildAndWrite(
					snapshotFile,
					snapshotName,
					snapshotStatus
				);
	
				List<Integer> snapshotContentBuilderAndToFileWriterErrors = snapshotContentBuilderAndToFileWriter.getErrors();
	
				if (snapshotContentBuilderAndToFileWriterErrors.isEmpty())
				{
					return;
				}
	
				for (Integer snapshotContentBuilderAndToFileWriterError : snapshotContentBuilderAndToFileWriterErrors)
				{
					switch (snapshotContentBuilderAndToFileWriterError)
					{
						case FwCoreSnapshotContentBuilderAndToFileWriterErrorConstant.ERROR_SNAPSHOT_FILE_COULD_NOT_BE_WRITTEN:
							errors.add(
								FwCoreSnapshotCreatorErrorConstant.ERROR_SNAPSHOT_FILE_COULD_NOT_BE_WRITTEN
							);
							break;
						case FwCoreSnapshotContentBuilderAndToFileWriterErrorConstant.ERROR_SNAPSHOT_FILE_COULD_NOT_BE_CLOSED:
							errors.add(
								FwCoreSnapshotCreatorErrorConstant.ERROR_SNAPSHOT_FILE_COULD_NOT_BE_CLOSED
							);
							break;
					}
				}
			}

			private void deleteSnapshotFile()
			{
				if (!snapshotFile.delete())
				{
					errors.add(
						FwCoreSnapshotCreatorErrorConstant.ERROR_SNAPSHOT_FILE_COULD_NOT_BE_DELETED
					);
				}
			}

}
