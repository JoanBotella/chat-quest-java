package chatQuest.fw.core.main.service.snapshotCreator;

import java.util.List;

import chatQuest.fw.core.main.library.status.FwCoreStatusItf;

public interface
	FwCoreSnapshotCreatorItf
{

	public List<Integer> getErrors();

	public void create(String name, FwCoreStatusItf status);

}
