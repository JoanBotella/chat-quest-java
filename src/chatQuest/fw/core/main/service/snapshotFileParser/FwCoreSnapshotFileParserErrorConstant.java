package chatQuest.fw.core.main.service.snapshotFileParser;

public final class
	FwCoreSnapshotFileParserErrorConstant
{

	public static final int ERROR_SNAPSHOT_FILE_COULD_NOT_BE_FOUND = 0;

	public static final int ERROR_SNAPSHOT_FILE_COULD_NOT_BE_READ = 1;

	public static final int ERROR_SNAPSHOT_FILE_COULD_NOT_BE_CLOSED = 2;

}
