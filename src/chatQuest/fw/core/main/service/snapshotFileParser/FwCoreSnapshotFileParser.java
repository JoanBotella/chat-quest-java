package chatQuest.fw.core.main.service.snapshotFileParser;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import chatQuest.fw.core.main.library.snapshotMetadata.FwCoreSnapshotMetadataItf;
import chatQuest.fw.core.main.library.status.FwCoreStatusItf;
import chatQuest.fw.core.main.service.fileLineReader.FwCoreFileLineReaderErrorConstant;
import chatQuest.fw.core.main.service.fileLineReader.FwCoreFileLineReaderItf;
import chatQuest.fw.core.main.service.snapshotMetadataUnserializer.FwCoreSnapshotMetadataUnserializerItf;
import chatQuest.fw.core.main.service.statusUnserializer.FwCoreStatusUnserializerItf;
import chatQuest.fw.core.main.service.snapshotFileParser.FwCoreSnapshotFileParserItf;

public final class
	FwCoreSnapshotFileParser
implements
	FwCoreSnapshotFileParserItf
{

	private static final int SNAPSHOT_METADATA_LINE = 0;
	private static final int STATUS_LINE = 1;

	private FwCoreSnapshotMetadataUnserializerItf snapshotMetadataUnserializer;
	private FwCoreStatusUnserializerItf statusUnserializer;
	private FwCoreFileLineReaderItf fileLineReader;

	private List<Integer> errors;
	private FwCoreSnapshotMetadataItf snapshotMetadata;
	private FwCoreStatusItf status;
	private String line;

	public FwCoreSnapshotFileParser(
		FwCoreSnapshotMetadataUnserializerItf snapshotMetadataUnserializer,
		FwCoreStatusUnserializerItf statusUnserializer,
		FwCoreFileLineReaderItf fileLineReader
	)
	{
		this.snapshotMetadataUnserializer = snapshotMetadataUnserializer;
		this.statusUnserializer = statusUnserializer;
		this.fileLineReader = fileLineReader;

		errors = buildErrors();
	}

		private List<Integer> buildErrors()
		{
			return new ArrayList<Integer>();
		}

	public List<Integer> getErrors()
	{
		return errors;
	}

	public void parseSnapshotMetadata(File file)
	{
		errors.clear();
		snapshotMetadata = null;

		tryToReadLineFromFile(file, SNAPSHOT_METADATA_LINE);

		if (line == null)
		{
			return;
		}

		snapshotMetadataUnserializer.unserialize(line);

		if (snapshotMetadataUnserializer.hasSnapshotMetadata())
		{
			snapshotMetadata = snapshotMetadataUnserializer.getSnapshotMetadataAfterHas();
		}

		line = null;
	}

		private void tryToReadLineFromFile(File file, int lineNumber)
		{
			line = null;

			fileLineReader.read(file, lineNumber);

			if (fileLineReader.hasLine())
			{
				line = fileLineReader.getLineAfterHas();
				return;
			}

			List<Integer> fileLineReaderErrors = fileLineReader.getErrors();

			for (Integer fileLineReaderError : fileLineReaderErrors)
			{
				switch (fileLineReaderError)
				{
					case FwCoreFileLineReaderErrorConstant.ERROR_FILE_COULD_NOT_BE_FOUND:
						errors.add(
							FwCoreSnapshotFileParserErrorConstant.ERROR_SNAPSHOT_FILE_COULD_NOT_BE_FOUND
						);
						break;
					case FwCoreFileLineReaderErrorConstant.ERROR_FILE_COULD_NOT_BE_READ:
						errors.add(
							FwCoreSnapshotFileParserErrorConstant.ERROR_SNAPSHOT_FILE_COULD_NOT_BE_READ
						);
						break;
					case FwCoreFileLineReaderErrorConstant.ERROR_FILE_COULD_NOT_BE_CLOSED:
						errors.add(
							FwCoreSnapshotFileParserErrorConstant.ERROR_SNAPSHOT_FILE_COULD_NOT_BE_CLOSED
						);
						break;
				}
			}
		}

	public boolean hasSnapshotMetadata()
	{
		return snapshotMetadata != null;
	}

	public FwCoreSnapshotMetadataItf getSnapshotMetadataAfterHas()
	{
		return snapshotMetadata;
	}

	public void parseStatus(File file)
	{
		errors.clear();
		status = null;

		tryToReadLineFromFile(file, STATUS_LINE);

		if (line == null)
		{
			return;
		}

		statusUnserializer.unserialize(line);

		if (statusUnserializer.hasStatus())
		{
			status = statusUnserializer.getStatusAfterHas();
		}

		line = null;
	}

	public boolean hasStatus()
	{
		return status != null;
	}

	public FwCoreStatusItf getStatusAfterHas()
	{
		return status;
	}

}
