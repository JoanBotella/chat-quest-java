package chatQuest.fw.core.main.service.snapshotFileParser;

import java.io.File;
import java.util.List;

import chatQuest.fw.core.main.library.snapshotMetadata.FwCoreSnapshotMetadataItf;
import chatQuest.fw.core.main.library.status.FwCoreStatusItf;

public interface
	FwCoreSnapshotFileParserItf
{

	public List<Integer> getErrors();

	public void parseSnapshotMetadata(File file);

	public boolean hasSnapshotMetadata();

	public FwCoreSnapshotMetadataItf getSnapshotMetadataAfterHas();

	public void parseStatus(File file);

	public boolean hasStatus();

	public FwCoreStatusItf getStatusAfterHas();

}
