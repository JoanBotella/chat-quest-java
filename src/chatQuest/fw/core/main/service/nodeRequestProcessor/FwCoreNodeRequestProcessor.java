package chatQuest.fw.core.main.service.nodeRequestProcessor;

import java.util.List;

import chatQuest.fw.core.main.library.node.FwCoreNodeItf;
import chatQuest.fw.core.main.library.nodeRequest.FwCoreNodeRequestItf;
import chatQuest.fw.core.main.library.nodeResponse.FwCoreNodeResponseItf;
import chatQuest.fw.core.main.library.router.FwCoreRouterItf;

public final class FwCoreNodeRequestProcessor
	implements FwCoreNodeRequestProcessorItf
{

	private FwCoreRouterItf router;
	private FwCoreNodeItf systemNode;

	public FwCoreNodeRequestProcessor(
		FwCoreRouterItf router,
		FwCoreNodeItf systemNode
	)
	{
		this.router = router;
		this.systemNode = systemNode;
	}

	public FwCoreNodeResponseItf process(FwCoreNodeRequestItf nodeRequest)
	{
		FwCoreNodeItf node = routeNode(nodeRequest);

		node.run(nodeRequest);

		if (node.hasNodeResponse())
		{
			return node.getNodeResponseAfterHas();
		}

		systemNode.run(nodeRequest);

		if (systemNode.hasNodeResponse())
		{
			return systemNode.getNodeResponseAfterHas();
		}

		throw new RuntimeException("Neither the routed node nor the SystemNode built a NodeResponse");
	}

		private FwCoreNodeItf routeNode(FwCoreNodeRequestItf nodeRequest)
		{
			String nodeId = getNodeIdFromBreadcrumbs(nodeRequest);
			return router.route(nodeId);
		}

			private String getNodeIdFromBreadcrumbs(FwCoreNodeRequestItf nodeRequest)
			{
				List<String> breadcrumbs = nodeRequest.getStatus().getBreadcrumbs();
				int size = breadcrumbs.size();

				if (size > 0)
				{
					return breadcrumbs.get(size - 1);
				}

				throw new RuntimeException("Any node could be processed because breadcrumbs are empty.");
			}

}