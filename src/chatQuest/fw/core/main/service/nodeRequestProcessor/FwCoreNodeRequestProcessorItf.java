package chatQuest.fw.core.main.service.nodeRequestProcessor;

import chatQuest.fw.core.main.library.nodeRequest.FwCoreNodeRequestItf;
import chatQuest.fw.core.main.library.nodeResponse.FwCoreNodeResponseItf;

public interface FwCoreNodeRequestProcessorItf
{

	public FwCoreNodeResponseItf process(FwCoreNodeRequestItf nodeRequest);

}