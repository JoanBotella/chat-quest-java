package chatQuest.fw.core.main.service.settingsUnjsonizer;

import chatQuest.fw.core.main.library.settings.FwCoreSettingsItf;
import org.json.JSONObject;

public interface
	FwCoreSettingsUnjsonizerItf
{

	public void unjsonize(JSONObject settingsJsonObject);

	public boolean hasSettings();

	public FwCoreSettingsItf getSettingsAfterHas();

}
