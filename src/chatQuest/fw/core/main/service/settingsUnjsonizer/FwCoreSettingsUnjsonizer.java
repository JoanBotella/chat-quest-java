package chatQuest.fw.core.main.service.settingsUnjsonizer;

import org.json.JSONObject;
import org.json.JSONException;

import chatQuest.fw.core.main.library.JsonSerializationConstant;
import chatQuest.fw.core.main.library.settings.FwCoreSettings;
import chatQuest.fw.core.main.library.settings.FwCoreSettingsItf;
import chatQuest.fw.core.main.service.settingsUnjsonizer.FwCoreSettingsUnjsonizerItf;

public final class
	FwCoreSettingsUnjsonizer
implements
	FwCoreSettingsUnjsonizerItf
{

	private FwCoreSettingsItf settings;

	public boolean hasSettings()
	{
		return settings != null;
	}

	public FwCoreSettingsItf getSettingsAfterHas()
	{
		return settings;
	}

	public void unjsonize(JSONObject settingsJsonObject)
	{
		try
		{
			settings = new FwCoreSettings(
				settingsJsonObject.getString(
					JsonSerializationConstant.SETTINGS_LANGUAGE_CODE_KEY
				)
			);
		}
		catch (JSONException e)
		{
			settings = null;
		}
	}

}
