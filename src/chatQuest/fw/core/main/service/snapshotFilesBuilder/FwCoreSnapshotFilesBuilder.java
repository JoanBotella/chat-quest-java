package chatQuest.fw.core.main.service.snapshotFilesBuilder;

import java.io.File;
import java.util.Arrays;
import java.util.Collections;

import chatQuest.fw.core.main.library.configuration.FwCoreConfigurationItf;
import chatQuest.fw.core.main.service.configurationContainer.FwCoreConfigurationContainerItf;
import chatQuest.fw.core.main.service.snapshotFilesBuilder.FwCoreSnapshotFilesBuilderItf;

public final class
	FwCoreSnapshotFilesBuilder
implements
	FwCoreSnapshotFilesBuilderItf
{

	private FwCoreConfigurationContainerItf configurationContainer;

	public FwCoreSnapshotFilesBuilder(
		FwCoreConfigurationContainerItf configurationContainer
	)
	{
		this.configurationContainer = configurationContainer;
	}

	public File[] build()
	{
		FwCoreConfigurationItf configuration = configurationContainer.get();
		File snapshotsDirectory = configuration.getSnapshotsDirectory();

		if (
			!snapshotsDirectory.exists()
			|| !snapshotsDirectory.canRead()
		)
		{
			return new File[0];
		}

		File[] files = snapshotsDirectory.listFiles();
		Arrays.sort(files);
		Collections.reverse(
			Arrays.asList(files)
		);
		return files;
	}

}
