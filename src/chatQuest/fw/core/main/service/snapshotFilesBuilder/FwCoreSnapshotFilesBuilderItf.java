package chatQuest.fw.core.main.service.snapshotFilesBuilder;

import java.io.File;

public interface
	FwCoreSnapshotFilesBuilderItf
{

	public File[] build();

}
