package chatQuest.fw.core.main.service.statusUnjsonizer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;
import org.json.JSONArray;
import org.json.JSONException;

import chatQuest.fw.core.main.library.JsonSerializationConstant;
import chatQuest.fw.core.main.library.settings.FwCoreSettingsItf;
import chatQuest.fw.core.main.library.status.FwCoreStatus;
import chatQuest.fw.core.main.library.status.FwCoreStatusItf;
import chatQuest.fw.core.main.service.settingsUnjsonizer.FwCoreSettingsUnjsonizerItf;
import chatQuest.fw.core.main.service.statusUnjsonizer.FwCoreStatusUnjsonizerItf;

public final class
	FwCoreStatusUnjsonizer
implements
	FwCoreStatusUnjsonizerItf
{

	private FwCoreSettingsUnjsonizerItf settingsUnjsonizer;

	private FwCoreSettingsItf settings;
	private FwCoreStatusItf status;

	public FwCoreStatusUnjsonizer(
		FwCoreSettingsUnjsonizerItf settingsUnjsonizer
	)
	{
		this.settingsUnjsonizer = settingsUnjsonizer;
	}

	public boolean hasStatus()
	{
		return status != null;
	}

	public FwCoreStatusItf getStatusAfterHas()
	{
		return status;
	}

	public void unjsonize(JSONObject statusJsonObject)
	{
		settings = null;

		setupSettings(statusJsonObject);

		if (settings == null)
		{
			return;
		}

		try
		{
			status = new FwCoreStatus(
				getBreadcrumbs(statusJsonObject),
				settings,
				getVariables(statusJsonObject)
			);
		}
		catch (JSONException e)
		{
			status = null;
		}
	}

		private void setupSettings(JSONObject statusJsonObject)
		{
			settingsUnjsonizer.unjsonize(
				statusJsonObject.getJSONObject(
					JsonSerializationConstant.STATUS_SETTINGS_KEY
				)
			);

			if (settingsUnjsonizer.hasSettings())
			{
				settings = settingsUnjsonizer.getSettingsAfterHas();
			}
		}

		private List<String> getBreadcrumbs(JSONObject statusJsonObject)
		{
			List<String> breadcrumbs = new ArrayList<String>();
			JSONArray breadcrumbsJsonArray = statusJsonObject.getJSONArray(
				JsonSerializationConstant.STATUS_BREADCRUMBS_KEY
			);

			for (int index = 0; index < breadcrumbsJsonArray.length(); index++)
			{
				breadcrumbs.add(
					breadcrumbsJsonArray.getString(index)
				);
			}

			return breadcrumbs;
		}

		private Map<String, String> getVariables(JSONObject statusJsonObject)
		{
			Map<String, String> variables = new HashMap<String, String>();
			JSONObject variablesJsonObject = statusJsonObject.getJSONObject(
				JsonSerializationConstant.STATUS_VARIABLES_KEY
			);

			Iterator<String> keysIterator = variablesJsonObject.keys();

			while(keysIterator.hasNext())
			{
				String key = keysIterator.next();
				variables.put(
					key,
					variablesJsonObject.getString(key)
				);
			}

			return variables;
		}

}
