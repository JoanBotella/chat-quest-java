package chatQuest.fw.core.main.service.statusUnjsonizer;

import org.json.JSONObject;
import chatQuest.fw.core.main.library.status.FwCoreStatusItf;

public interface
	FwCoreStatusUnjsonizerItf
{

	public void unjsonize(JSONObject statusJsonObject);

	public boolean hasStatus();

	public FwCoreStatusItf getStatusAfterHas();

}
