package chatQuest.fw.core.main.service.snapshotLoader;

import java.util.List;

import chatQuest.fw.core.main.library.status.FwCoreStatusItf;

public interface
	FwCoreSnapshotLoaderItf
{

	public List<Integer> getErrors();

	public void load(String snapshotMetadatasMenuQuery);

	public boolean hasStatus();

	public FwCoreStatusItf getStatusAfterHas();

}
