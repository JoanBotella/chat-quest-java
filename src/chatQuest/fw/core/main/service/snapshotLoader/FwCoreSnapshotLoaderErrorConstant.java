package chatQuest.fw.core.main.service.snapshotLoader;

public final class
	FwCoreSnapshotLoaderErrorConstant
{

	public static final int ERROR_QUERY_DOES_NOT_MATCH_A_FILE = 0;

	public static final int ERROR_SNAPSHOT_FILE_COULD_NOT_BE_FOUND = 1;

	public static final int ERROR_SNAPSHOT_FILE_COULD_NOT_BE_READ = 2;

	public static final int ERROR_SNAPSHOT_FILE_COULD_NOT_BE_CLOSED = 3;

}
