package chatQuest.fw.core.main.service.snapshotLoader;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import chatQuest.fw.core.main.library.status.FwCoreStatusItf;
import chatQuest.fw.core.main.service.fileBuilderBySnapshotMetadatasMenuQuery.FwCoreFileBuilderBySnapshotMetadatasMenuQueryItf;
import chatQuest.fw.core.main.service.snapshotFileParser.FwCoreSnapshotFileParserErrorConstant;
import chatQuest.fw.core.main.service.snapshotFileParser.FwCoreSnapshotFileParserItf;
import chatQuest.fw.core.main.service.snapshotLoader.FwCoreSnapshotLoaderItf;

public final class
	FwCoreSnapshotLoader
implements
	FwCoreSnapshotLoaderItf
{

	private FwCoreFileBuilderBySnapshotMetadatasMenuQueryItf fileBuilderBySnapshotMetadatasMenuQuery;
	private FwCoreSnapshotFileParserItf snapshotFileParser;

	private List<Integer> errors;
	private File file;
	private FwCoreStatusItf status;

	public FwCoreSnapshotLoader(
		FwCoreFileBuilderBySnapshotMetadatasMenuQueryItf fileBuilderBySnapshotMetadatasMenuQuery,
		FwCoreSnapshotFileParserItf snapshotFileParser
	)
	{
		this.fileBuilderBySnapshotMetadatasMenuQuery = fileBuilderBySnapshotMetadatasMenuQuery;
		this.snapshotFileParser = snapshotFileParser;

		errors = buildErrors();
	}

		private List<Integer> buildErrors()
		{
			return new ArrayList<Integer>();
		}

	public List<Integer> getErrors()
	{
		return errors;
	}

	public void load(String snapshotMetadatasMenuQuery)
	{
		errors.clear();
		status = null;
		file = null;

		tryToBuildFile(snapshotMetadatasMenuQuery);

		if (!errors.isEmpty())
		{
			return;
		}

		tryToParseStatus();

		file = null;
	}

		private void tryToBuildFile(String snapshotMetadatasMenuQuery)
		{
			fileBuilderBySnapshotMetadatasMenuQuery.build(snapshotMetadatasMenuQuery);

			if (fileBuilderBySnapshotMetadatasMenuQuery.hasFile())
			{
				file = fileBuilderBySnapshotMetadatasMenuQuery.getFileAfterHas();
				return;
			}

			errors.add(
				FwCoreSnapshotLoaderErrorConstant.ERROR_QUERY_DOES_NOT_MATCH_A_FILE
			);
		}

		private void tryToParseStatus()
		{
			snapshotFileParser.parseStatus(file);

			if (snapshotFileParser.hasStatus())
			{
				status = snapshotFileParser.getStatusAfterHas();
				return;
			}
	
			List<Integer> snapshotFileParserErrors = snapshotFileParser.getErrors();
	
			for (Integer snapshotFileParserError : snapshotFileParserErrors)
			{
				switch (snapshotFileParserError)
				{
					case FwCoreSnapshotFileParserErrorConstant.ERROR_SNAPSHOT_FILE_COULD_NOT_BE_FOUND:
						errors.add(
							FwCoreSnapshotLoaderErrorConstant.ERROR_SNAPSHOT_FILE_COULD_NOT_BE_FOUND
						);
						break;
					case FwCoreSnapshotFileParserErrorConstant.ERROR_SNAPSHOT_FILE_COULD_NOT_BE_READ:
						errors.add(
							FwCoreSnapshotLoaderErrorConstant.ERROR_SNAPSHOT_FILE_COULD_NOT_BE_READ
						);
						break;
					case FwCoreSnapshotFileParserErrorConstant.ERROR_SNAPSHOT_FILE_COULD_NOT_BE_CLOSED:
						errors.add(
							FwCoreSnapshotLoaderErrorConstant.ERROR_SNAPSHOT_FILE_COULD_NOT_BE_CLOSED
						);
						break;
				}
			}
		}

	public boolean hasStatus()
	{
		return status != null;
	}

	public FwCoreStatusItf getStatusAfterHas()
	{
		return status;
	}

}
