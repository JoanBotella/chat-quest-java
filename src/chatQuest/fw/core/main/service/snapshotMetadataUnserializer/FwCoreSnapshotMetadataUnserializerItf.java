package chatQuest.fw.core.main.service.snapshotMetadataUnserializer;

import chatQuest.fw.core.main.library.snapshotMetadata.FwCoreSnapshotMetadataItf;

public interface
	FwCoreSnapshotMetadataUnserializerItf
{

	public void unserialize(String snapshotMetadataString);

	public boolean hasSnapshotMetadata();

	public FwCoreSnapshotMetadataItf getSnapshotMetadataAfterHas();

}
