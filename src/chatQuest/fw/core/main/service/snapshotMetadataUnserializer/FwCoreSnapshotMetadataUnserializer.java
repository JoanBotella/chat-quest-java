package chatQuest.fw.core.main.service.snapshotMetadataUnserializer;

import org.json.JSONObject;
import org.json.JSONException;
import chatQuest.fw.core.main.library.snapshotMetadata.FwCoreSnapshotMetadataItf;
import chatQuest.fw.core.main.service.snapshotMetadataUnjsonizer.FwCoreSnapshotMetadataUnjsonizerItf;

public final class
	FwCoreSnapshotMetadataUnserializer
implements
	FwCoreSnapshotMetadataUnserializerItf
{

	private FwCoreSnapshotMetadataUnjsonizerItf snapshotMetadataUnjsonizer;

	private FwCoreSnapshotMetadataItf snapshotMetadata;

	public FwCoreSnapshotMetadataUnserializer(
		FwCoreSnapshotMetadataUnjsonizerItf snapshotMetadataUnjsonizer
	)
	{
		this.snapshotMetadataUnjsonizer = snapshotMetadataUnjsonizer;
	}

	public boolean hasSnapshotMetadata()
	{
		return snapshotMetadata != null;
	}

	public FwCoreSnapshotMetadataItf getSnapshotMetadataAfterHas()
	{
		return snapshotMetadata;
	}

	public void unserialize(String snapshotMetadataString)
	{
		snapshotMetadata = null;
		JSONObject snapshotMetadataJsonObject;

		try
		{
			snapshotMetadataJsonObject = new JSONObject(snapshotMetadataString);
		}
		catch (JSONException e)
		{
			return;
		}

		snapshotMetadataUnjsonizer.unjsonize(snapshotMetadataJsonObject);

		if (snapshotMetadataUnjsonizer.hasSnapshotMetadata())
		{
			snapshotMetadata = snapshotMetadataUnjsonizer.getSnapshotMetadataAfterHas();
		}
	}

}
