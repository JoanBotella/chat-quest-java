package chatQuest.fw.core.main.service.snapshotFileBuilder;

import java.io.File;

public interface
	FwCoreSnapshotFileBuilderItf
{

	public File build(File snapshotsDirectory);

}
