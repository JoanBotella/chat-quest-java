package chatQuest.fw.core.main.service.snapshotFileBuilder;

import java.io.File;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import chatQuest.fw.core.main.service.snapshotFileBuilder.FwCoreSnapshotFileBuilderItf;

public final class
	FwCoreSnapshotFileBuilder
implements
	FwCoreSnapshotFileBuilderItf
{

	public File build(File snapshotsDirectory)
	{
		int index = -1;
		String snapshotsDirectoryAbsolutePath = snapshotsDirectory.getAbsolutePath();
		String name = buildName();
		String snapshotFileAbsolutePath;
		File snapshotFile;

		do
		{
			index++;

			snapshotFileAbsolutePath = snapshotsDirectoryAbsolutePath + "/" + name;

			if (index > 0)
			{
				snapshotFileAbsolutePath = snapshotFileAbsolutePath + "_" + index;
			}

			snapshotFile = new File(snapshotFileAbsolutePath);
		}
		while (snapshotFile.exists());

		return snapshotFile;
	}

		private String buildName()
		{
			LocalDateTime now = LocalDateTime.now();
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
			return formatter.format(now);
		}

}
