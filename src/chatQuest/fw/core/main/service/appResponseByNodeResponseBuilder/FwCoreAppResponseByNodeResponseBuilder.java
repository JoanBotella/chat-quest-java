package chatQuest.fw.core.main.service.appResponseByNodeResponseBuilder;

import chatQuest.fw.core.main.library.appResponse.FwCoreAppResponse;
import chatQuest.fw.core.main.library.appResponse.FwCoreAppResponseItf;
import chatQuest.fw.core.main.library.nodeResponse.FwCoreNodeResponseItf;

public final class FwCoreAppResponseByNodeResponseBuilder
	implements
		FwCoreAppResponseByNodeResponseBuilderItf
{

	public FwCoreAppResponseItf build(FwCoreNodeResponseItf nodeResponse)
	{
		FwCoreAppResponseItf appResponse = new FwCoreAppResponse(
			nodeResponse.getStatus()
		);

		appResponse.setSlides(
			nodeResponse.getSlides()
		);

		appResponse.setEnd(
			nodeResponse.getEnd()
		);

		return appResponse;
	}

}
