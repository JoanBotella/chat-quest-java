package chatQuest.fw.core.main.service.appResponseByNodeResponseBuilder;

import chatQuest.fw.core.main.library.appResponse.FwCoreAppResponseItf;
import chatQuest.fw.core.main.library.nodeResponse.FwCoreNodeResponseItf;

public interface FwCoreAppResponseByNodeResponseBuilderItf
{

	public FwCoreAppResponseItf build(FwCoreNodeResponseItf nodeResponse);

}
