package chatQuest.fw.core.main.service.bodyBuilderBySnapshotDeleter;

import chatQuest.fw.core.translation.library.translation.FwCoreTranslationItf;
import chatQuest.fw.core.main.service.snapshotDeleter.FwCoreSnapshotDeleterItf;

public interface
	FwCoreBodyBuilderBySnapshotDeleterItf
{

	public String build(FwCoreSnapshotDeleterItf snapshotDeleter, FwCoreTranslationItf translation);

}
