package chatQuest.fw.core.main.service.bodyBuilderBySnapshotDeleter;

import java.util.List;

import chatQuest.fw.core.translation.library.translation.FwCoreTranslationItf;
import chatQuest.fw.core.main.service.bodyBuilderBySnapshotDeleter.FwCoreBodyBuilderBySnapshotDeleterItf;
import chatQuest.fw.core.main.service.snapshotDeleter.FwCoreSnapshotDeleterErrorConstant;
import chatQuest.fw.core.main.service.snapshotDeleter.FwCoreSnapshotDeleterItf;

public final class
	FwCoreBodyBuilderBySnapshotDeleter
implements
	FwCoreBodyBuilderBySnapshotDeleterItf
{

	public String build(FwCoreSnapshotDeleterItf snapshotDeleter, FwCoreTranslationItf translation)
	{
		List<Integer> errors = snapshotDeleter.getErrors();

		if (!errors.isEmpty())
		{
			for (Integer error : errors)
			{
				String body = "";

				switch (error)
				{
					case FwCoreSnapshotDeleterErrorConstant.ERROR_QUERY_DOES_NOT_MATCH_A_FILE:
						body += translation.fwCore_deleteSnapshotMenu_body_error_queryDoesNotMatchAFile();
						break;
					case FwCoreSnapshotDeleterErrorConstant.ERROR_SNAPSHOT_COULD_NOT_BE_DELETED:
						body += translation.fwCore_deleteSnapshotMenu_body_error_snapshotFileCouldNotBeDeleted();
						break;
				}

				return body;
			}
		}

		return translation.fwCore_deleteSnapshotMenu_body_success();
	}

}
