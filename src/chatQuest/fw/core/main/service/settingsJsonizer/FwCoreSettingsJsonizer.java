package chatQuest.fw.core.main.service.settingsJsonizer;

import org.json.JSONObject;

import chatQuest.fw.core.main.library.JsonSerializationConstant;
import chatQuest.fw.core.main.library.settings.FwCoreSettingsItf;
import chatQuest.fw.core.main.service.settingsJsonizer.FwCoreSettingsJsonizerItf;

public final class
	FwCoreSettingsJsonizer
implements
	FwCoreSettingsJsonizerItf
{

	public JSONObject jsonize(FwCoreSettingsItf settings)
	{
		JSONObject object = new JSONObject();

		object.put(
			JsonSerializationConstant.SETTINGS_LANGUAGE_CODE_KEY,
			settings.getLanguageCode()
		);

		return object;
	}

}
