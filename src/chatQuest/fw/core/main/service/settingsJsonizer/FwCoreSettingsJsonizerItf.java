package chatQuest.fw.core.main.service.settingsJsonizer;

import org.json.JSONObject;
import chatQuest.fw.core.main.library.settings.FwCoreSettingsItf;

public interface
	FwCoreSettingsJsonizerItf
{

	public JSONObject jsonize(FwCoreSettingsItf settings);

}
