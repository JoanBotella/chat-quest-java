package chatQuest.fw.core.main.service.variablesCopier;

import java.util.Map;

public interface FwCoreVariablesCopierItf
{

	public Map<String, String> copy(Map<String, String> source);

}
