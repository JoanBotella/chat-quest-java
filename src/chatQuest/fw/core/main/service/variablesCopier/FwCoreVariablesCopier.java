package chatQuest.fw.core.main.service.variablesCopier;

import java.util.HashMap;
import java.util.Map;

public final class FwCoreVariablesCopier
	implements
		FwCoreVariablesCopierItf
{

	public Map<String, String> copy(Map<String, String> source)
	{
		Map<String, String> r = new HashMap<String, String>();

		for (Map.Entry<String, String> entry: source.entrySet())
		{
			r.put(
				entry.getKey(),
				entry.getValue()
			);
		}

		return r;
	}
}
