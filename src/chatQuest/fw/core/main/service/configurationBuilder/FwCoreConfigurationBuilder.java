package chatQuest.fw.core.main.service.configurationBuilder;

import java.io.File;

import chatQuest.fw.core.main.library.configuration.FwCoreConfiguration;
import chatQuest.fw.core.main.library.configuration.FwCoreConfigurationItf;

public final class
	FwCoreConfigurationBuilder
implements
	FwCoreConfigurationBuilderItf
{

	public FwCoreConfigurationItf build()
	{
		return new FwCoreConfiguration(
			new File(
				System.getProperty("user.dir") + "/snapshot"
			)
		);
	}

}
