package chatQuest.fw.core.main.service.configurationBuilder;

import chatQuest.fw.core.main.library.configuration.FwCoreConfigurationItf;

public interface
	FwCoreConfigurationBuilderItf
{

	public FwCoreConfigurationItf build();

}
