package chatQuest.fw.core.main.service.configurationContainer;

import chatQuest.fw.core.main.library.configuration.FwCoreConfigurationItf;

public interface
	FwCoreConfigurationContainerItf
{

	public FwCoreConfigurationItf get();

}
