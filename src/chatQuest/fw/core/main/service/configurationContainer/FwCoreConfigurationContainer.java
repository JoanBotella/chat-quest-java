package chatQuest.fw.core.main.service.configurationContainer;

import chatQuest.fw.core.main.library.configuration.FwCoreConfigurationItf;
import chatQuest.fw.core.main.service.configurationBuilder.FwCoreConfigurationBuilderItf;
import chatQuest.fw.core.main.service.configurationContainer.FwCoreConfigurationContainerItf;

public final class
	FwCoreConfigurationContainer
implements
	FwCoreConfigurationContainerItf
{

	private FwCoreConfigurationBuilderItf configurationBuilder;

	private FwCoreConfigurationItf configuration;

	public FwCoreConfigurationContainer(
			FwCoreConfigurationBuilderItf configurationBuilder
	)
	{
		this.configurationBuilder = configurationBuilder;
	}

	public FwCoreConfigurationItf get()
	{
		if (configuration == null)
		{
			configuration = build();
		}
		return configuration;
	}

		private FwCoreConfigurationItf build()
		{
			return configurationBuilder.build();
		}

}
