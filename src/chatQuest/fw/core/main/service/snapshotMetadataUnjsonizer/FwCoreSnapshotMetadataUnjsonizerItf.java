package chatQuest.fw.core.main.service.snapshotMetadataUnjsonizer;

import org.json.JSONObject;

import chatQuest.fw.core.main.library.snapshotMetadata.FwCoreSnapshotMetadataItf;

public interface
	FwCoreSnapshotMetadataUnjsonizerItf
{

	public void unjsonize(JSONObject snapshotMetadataJsonObject);

	public boolean hasSnapshotMetadata();

	public FwCoreSnapshotMetadataItf getSnapshotMetadataAfterHas();

}
