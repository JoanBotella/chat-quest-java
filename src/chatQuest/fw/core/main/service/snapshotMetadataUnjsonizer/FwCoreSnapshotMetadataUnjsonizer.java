package chatQuest.fw.core.main.service.snapshotMetadataUnjsonizer;

import java.time.OffsetDateTime;
import java.util.Date;

import org.json.JSONObject;
import org.json.JSONException;

import chatQuest.fw.core.main.library.JsonSerializationConstant;
import chatQuest.fw.core.main.library.snapshotMetadata.FwCoreSnapshotMetadata;
import chatQuest.fw.core.main.library.snapshotMetadata.FwCoreSnapshotMetadataItf;
import chatQuest.fw.core.main.service.snapshotMetadataUnjsonizer.FwCoreSnapshotMetadataUnjsonizerItf;

public final class
	FwCoreSnapshotMetadataUnjsonizer
implements
	FwCoreSnapshotMetadataUnjsonizerItf
{

	private FwCoreSnapshotMetadataItf snapshotMetadata;

	public boolean hasSnapshotMetadata()
	{
		return snapshotMetadata != null;
	}

	public FwCoreSnapshotMetadataItf getSnapshotMetadataAfterHas()
	{
		return snapshotMetadata;
	}

	public void unjsonize(JSONObject snapshotMetadataJsonObject)
	{
		try
		{
			snapshotMetadata = new FwCoreSnapshotMetadata(
				snapshotMetadataJsonObject.getString(
					JsonSerializationConstant.SNAPSHOTS_METADATA_NAME_KEY
				),
				OffsetDateTime.parse(
					snapshotMetadataJsonObject.getString(
						JsonSerializationConstant.SNAPSHOTS_METADATA_DATE_TIME_KEY
					)
				)
			);
		}
		catch (JSONException e)
		{
			snapshotMetadata = null;
		}
	}

}
