package chatQuest.fw.core.main.service.bodyBuilderBySnapshotLoader;

import java.util.List;

import chatQuest.fw.core.translation.library.translation.FwCoreTranslationItf;
import chatQuest.fw.core.main.service.bodyBuilderBySnapshotLoader.FwCoreBodyBuilderBySnapshotLoaderItf;
import chatQuest.fw.core.main.service.snapshotLoader.FwCoreSnapshotLoaderErrorConstant;
import chatQuest.fw.core.main.service.snapshotLoader.FwCoreSnapshotLoaderItf;

public final class
	FwCoreBodyBuilderBySnapshotLoader
implements
	FwCoreBodyBuilderBySnapshotLoaderItf
{

	public String build(FwCoreSnapshotLoaderItf snapshotLoader, FwCoreTranslationItf translation)
	{
		List<Integer> errors = snapshotLoader.getErrors();

		if (!errors.isEmpty())
		{
			String body = "";

			for (Integer error : errors)
			{
				switch (error)
				{
					case FwCoreSnapshotLoaderErrorConstant.ERROR_QUERY_DOES_NOT_MATCH_A_FILE:
						body += translation.fwCore_loadSnapshotMenu_body_error_queryDoesNotMatchAFile();
						break;
					case FwCoreSnapshotLoaderErrorConstant.ERROR_SNAPSHOT_FILE_COULD_NOT_BE_FOUND:
						body += translation.fwCore_loadSnapshotMenu_body_error_snapshotFileCouldNotBeFound();
						break;
					case FwCoreSnapshotLoaderErrorConstant.ERROR_SNAPSHOT_FILE_COULD_NOT_BE_READ:
						body += translation.fwCore_loadSnapshotMenu_body_error_snapshotFileCouldNotBeRead();
						break;
					case FwCoreSnapshotLoaderErrorConstant.ERROR_SNAPSHOT_FILE_COULD_NOT_BE_CLOSED:
						body += translation.fwCore_loadSnapshotMenu_body_error_snapshotFileCouldNotBeClosed();
						break;
				}
			}

			return body;
		}

		if (snapshotLoader.hasStatus())
		{
			return translation.fwCore_loadSnapshotMenu_body_success();
		}

		return translation.fwCore_loadSnapshotMenu_body_error_snapshotFileCouldNotBeLoaded();
	}

}
