package chatQuest.fw.core.main.service.bodyBuilderBySnapshotLoader;

import chatQuest.fw.core.translation.library.translation.FwCoreTranslationItf;
import chatQuest.fw.core.main.service.snapshotLoader.FwCoreSnapshotLoaderItf;

public interface
	FwCoreBodyBuilderBySnapshotLoaderItf
{

	public String build(FwCoreSnapshotLoaderItf snapshotLoader, FwCoreTranslationItf translation);

}
