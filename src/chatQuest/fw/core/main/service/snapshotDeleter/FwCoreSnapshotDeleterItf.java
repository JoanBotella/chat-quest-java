package chatQuest.fw.core.main.service.snapshotDeleter;

import java.util.List;

public interface
	FwCoreSnapshotDeleterItf
{

	public List<Integer> getErrors();

	public void delete(String snapshotMetadatasMenuQuery);

}
