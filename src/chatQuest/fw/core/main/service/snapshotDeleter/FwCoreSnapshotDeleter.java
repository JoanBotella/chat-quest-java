package chatQuest.fw.core.main.service.snapshotDeleter;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import chatQuest.fw.core.main.service.fileBuilderBySnapshotMetadatasMenuQuery.FwCoreFileBuilderBySnapshotMetadatasMenuQueryItf;
import chatQuest.fw.core.main.service.snapshotDeleter.FwCoreSnapshotDeleterItf;

public final class
	FwCoreSnapshotDeleter
implements
	FwCoreSnapshotDeleterItf
{

	private FwCoreFileBuilderBySnapshotMetadatasMenuQueryItf fileBuilderBySnapshotMetadatasMenuQuery;

	private List<Integer> errors;
	File file;

	public FwCoreSnapshotDeleter(
		FwCoreFileBuilderBySnapshotMetadatasMenuQueryItf fileBuilderBySnapshotMetadatasMenuQuery
	)
	{
		this.fileBuilderBySnapshotMetadatasMenuQuery = fileBuilderBySnapshotMetadatasMenuQuery;

		errors = buildErrors();
	}

		private List<Integer> buildErrors()
		{
			return new ArrayList<Integer>();
		}

	public List<Integer> getErrors()
	{
		return errors;
	}

	public void delete(String snapshotMetadatasMenuQuery)
	{
		errors.clear();
		file = null;

		tryToBuildFile(snapshotMetadatasMenuQuery);

		if (!errors.isEmpty())
		{
			return;
		}

		tryToDeleteFile();

		file = null;
	}

		private void tryToBuildFile(String snapshotMetadatasMenuQuery)
		{
			fileBuilderBySnapshotMetadatasMenuQuery.build(snapshotMetadatasMenuQuery);

			if (fileBuilderBySnapshotMetadatasMenuQuery.hasFile())
			{
				file = fileBuilderBySnapshotMetadatasMenuQuery.getFileAfterHas();
				return;
			}

			errors.add(
				FwCoreSnapshotDeleterErrorConstant.ERROR_QUERY_DOES_NOT_MATCH_A_FILE
			);
		}

		private void tryToDeleteFile()
		{
			boolean success;

			try
			{
				success = file.delete();
			}
			catch (Exception e)
			{
				success = false;
			}

			if (success)
			{
				return;
			}

			errors.add(
				FwCoreSnapshotDeleterErrorConstant.ERROR_SNAPSHOT_COULD_NOT_BE_DELETED
			);
		}

}
