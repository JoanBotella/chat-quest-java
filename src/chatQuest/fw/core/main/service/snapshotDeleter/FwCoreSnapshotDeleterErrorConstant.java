package chatQuest.fw.core.main.service.snapshotDeleter;

public final class
	FwCoreSnapshotDeleterErrorConstant
{

	public static final int ERROR_QUERY_DOES_NOT_MATCH_A_FILE= 0;

	public static final int ERROR_SNAPSHOT_COULD_NOT_BE_DELETED = 1;

}
