package chatQuest.fw.core.main.service.settingsCopier;

import chatQuest.fw.core.main.library.settings.FwCoreSettingsItf;

public interface FwCoreSettingsCopierItf
{

	public FwCoreSettingsItf copy(FwCoreSettingsItf source);

}
