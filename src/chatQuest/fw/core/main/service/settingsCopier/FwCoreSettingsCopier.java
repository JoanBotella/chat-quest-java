package chatQuest.fw.core.main.service.settingsCopier;

import chatQuest.fw.core.main.library.settings.FwCoreSettings;
import chatQuest.fw.core.main.library.settings.FwCoreSettingsItf;

public final class FwCoreSettingsCopier
	implements
		FwCoreSettingsCopierItf
{

	public FwCoreSettingsItf copy(FwCoreSettingsItf source)
	{
		return new FwCoreSettings(
			source.getLanguageCode()
		);
	}
}
