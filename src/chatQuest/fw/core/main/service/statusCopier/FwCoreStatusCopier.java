package chatQuest.fw.core.main.service.statusCopier;

import chatQuest.fw.core.main.library.status.FwCoreStatus;
import chatQuest.fw.core.main.library.status.FwCoreStatusItf;
import chatQuest.fw.core.main.service.breadcrumbsCopier.FwCoreBreadcrumbsCopierItf;
import chatQuest.fw.core.main.service.settingsCopier.FwCoreSettingsCopierItf;
import chatQuest.fw.core.main.service.variablesCopier.FwCoreVariablesCopierItf;

public final class FwCoreStatusCopier
	implements
		FwCoreStatusCopierItf
{

	private FwCoreBreadcrumbsCopierItf breadcrumbsCopier;
	private FwCoreSettingsCopierItf settingsCopier;
	private FwCoreVariablesCopierItf variablesCopier;

	public FwCoreStatusCopier(
		FwCoreBreadcrumbsCopierItf breadcrumbsCopier,
		FwCoreSettingsCopierItf settingsCopier,
		FwCoreVariablesCopierItf variablesCopier
	)
	{
		this.breadcrumbsCopier = breadcrumbsCopier;
		this.settingsCopier = settingsCopier;
		this.variablesCopier = variablesCopier;
	}

	public FwCoreStatusItf copy(FwCoreStatusItf source)
	{
		return new FwCoreStatus(
			breadcrumbsCopier.copy(
				source.getBreadcrumbs()
			),
			settingsCopier.copy(
				source.getSettings()
			),
			variablesCopier.copy(
				source.getVariables()
			)
		);
	}

}
