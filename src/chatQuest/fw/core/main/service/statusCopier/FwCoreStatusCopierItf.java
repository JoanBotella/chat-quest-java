package chatQuest.fw.core.main.service.statusCopier;

import chatQuest.fw.core.main.library.status.FwCoreStatusItf;

public interface FwCoreStatusCopierItf
{

	public FwCoreStatusItf copy(FwCoreStatusItf source);

}
