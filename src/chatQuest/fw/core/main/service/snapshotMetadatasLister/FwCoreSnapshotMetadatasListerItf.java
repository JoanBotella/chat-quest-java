package chatQuest.fw.core.main.service.snapshotMetadatasLister;

import java.util.List;

import chatQuest.fw.core.main.library.snapshotMetadata.FwCoreSnapshotMetadataItf;

public interface
	FwCoreSnapshotMetadatasListerItf
{

	public List<FwCoreSnapshotMetadataItf> list();

}
