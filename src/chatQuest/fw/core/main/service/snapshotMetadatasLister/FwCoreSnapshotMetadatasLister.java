package chatQuest.fw.core.main.service.snapshotMetadatasLister;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import chatQuest.fw.core.main.library.snapshotMetadata.FwCoreSnapshotMetadataItf;
import chatQuest.fw.core.main.service.snapshotFileParser.FwCoreSnapshotFileParserItf;
import chatQuest.fw.core.main.service.snapshotFilesBuilder.FwCoreSnapshotFilesBuilderItf;
import chatQuest.fw.core.main.service.snapshotMetadatasLister.FwCoreSnapshotMetadatasListerItf;

public final class
	FwCoreSnapshotMetadatasLister
implements
	FwCoreSnapshotMetadatasListerItf
{

	private FwCoreSnapshotFilesBuilderItf snapshotFilesBuilder;
	private FwCoreSnapshotFileParserItf snapshotFileParser;

	private List<FwCoreSnapshotMetadataItf> snapshotMetadatas;

	public FwCoreSnapshotMetadatasLister(
		FwCoreSnapshotFilesBuilderItf snapshotFilesBuilder,
		FwCoreSnapshotFileParserItf snapshotFileParser
	)
	{
		this.snapshotFilesBuilder = snapshotFilesBuilder;
		this.snapshotFileParser = snapshotFileParser;
	}

	public List<FwCoreSnapshotMetadataItf> list()
	{
		snapshotMetadatas = new ArrayList<FwCoreSnapshotMetadataItf>();
		File files[] = snapshotFilesBuilder.build();

		for (File file : files)
		{
			snapshotFileParser.parseSnapshotMetadata(file);

			if (snapshotFileParser.hasSnapshotMetadata())
			{
				snapshotMetadatas.add(
					snapshotFileParser.getSnapshotMetadataAfterHas()
				);
			}
		}

		return snapshotMetadatas;
	}

}
