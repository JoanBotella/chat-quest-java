package chatQuest.fw.core.main.service.snapshotContentBuilderAndToFileWriter;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import chatQuest.fw.core.main.library.status.FwCoreStatusItf;
import chatQuest.fw.core.main.service.fileWriter.FwCoreFileWriterErrorConstant;
import chatQuest.fw.core.main.service.fileWriter.FwCoreFileWriterItf;
import chatQuest.fw.core.main.service.snapshotContentBuilderAndToFileWriter.FwCoreSnapshotContentBuilderAndToFileWriterItf;
import chatQuest.fw.core.main.service.snapshotFileContentBuilder.FwCoreSnapshotFileContentBuilderItf;
import chatQuest.fw.core.main.service.snapshotContentBuilderAndToFileWriter.FwCoreSnapshotContentBuilderAndToFileWriterErrorConstant;

public final class
	FwCoreSnapshotContentBuilderAndToFileWriter
implements
	FwCoreSnapshotContentBuilderAndToFileWriterItf
{

	private FwCoreSnapshotFileContentBuilderItf snapshotFileContentBuilder;
	private FwCoreFileWriterItf fileWriter;

	private List<Integer> errors;

	public FwCoreSnapshotContentBuilderAndToFileWriter(
		FwCoreSnapshotFileContentBuilderItf snapshotFileContentBuilder,
		FwCoreFileWriterItf fileWriter
	)
	{
		this.snapshotFileContentBuilder = snapshotFileContentBuilder;
		this.fileWriter = fileWriter;

		errors = buildErrors();
	}

		private List<Integer> buildErrors()
		{
			return new ArrayList<Integer>();
		}

	public List<Integer> getErrors()
	{
		return errors;
	}

	public void buildAndWrite(File file, String snapshotName, FwCoreStatusItf status)
	{
		errors.clear();

		String content = buildContent(
			snapshotName,
			status
		);

		tryToWriteFile(
			file,
			content
		);
	}

		private String buildContent(String snapshotName, FwCoreStatusItf status)
		{
			return snapshotFileContentBuilder.build(
				snapshotName,
				status
			);
		}

		private void tryToWriteFile(File file, String content)
		{
			fileWriter.write(
				file,
				content
			);

			List<Integer> fileWriterErrors = fileWriter.getErrors();

			if (fileWriterErrors.isEmpty())
			{
				return;
			}

			for (Integer fileWriterError : fileWriterErrors)
			{
				switch (fileWriterError)
				{
					case FwCoreFileWriterErrorConstant.ERROR_FILE_COULD_NOT_BE_WRITTEN:
						errors.add(
							FwCoreSnapshotContentBuilderAndToFileWriterErrorConstant.ERROR_SNAPSHOT_FILE_COULD_NOT_BE_WRITTEN
						);
						break;
					case FwCoreFileWriterErrorConstant.ERROR_FILE_COULD_NOT_BE_CLOSED:
						errors.add(
							FwCoreSnapshotContentBuilderAndToFileWriterErrorConstant.ERROR_SNAPSHOT_FILE_COULD_NOT_BE_CLOSED
						);
						break;
				}
			}
		}

}
