package chatQuest.fw.core.main.service.snapshotContentBuilderAndToFileWriter;

public final class
	FwCoreSnapshotContentBuilderAndToFileWriterErrorConstant
{

	public static final int ERROR_SNAPSHOT_FILE_COULD_NOT_BE_WRITTEN = 0;

	public static final int ERROR_SNAPSHOT_FILE_COULD_NOT_BE_CLOSED = 1;

}
