package chatQuest.fw.core.main.service.snapshotContentBuilderAndToFileWriter;

import java.io.File;
import java.util.List;

import chatQuest.fw.core.main.library.status.FwCoreStatusItf;

public interface
	FwCoreSnapshotContentBuilderAndToFileWriterItf
{

	public List<Integer> getErrors();

	public void buildAndWrite(File file, String snapshotName, FwCoreStatusItf status);

}
