package chatQuest.fw.core.main.service.nodeResponseByNodeRequestBuilder;

import chatQuest.fw.core.main.library.nodeRequest.FwCoreNodeRequestItf;
import chatQuest.fw.core.main.library.nodeResponse.FwCoreNodeResponse;
import chatQuest.fw.core.main.library.nodeResponse.FwCoreNodeResponseItf;
import chatQuest.fw.core.main.service.statusCopier.FwCoreStatusCopierItf;

public final class FwCoreNodeResponseByNodeRequestBuilder
	implements
		FwCoreNodeResponseByNodeRequestBuilderItf
{

	private FwCoreStatusCopierItf statusCopier;

	public FwCoreNodeResponseByNodeRequestBuilder(
		FwCoreStatusCopierItf statusCopier
	)
	{
		this.statusCopier = statusCopier;
	}

	public FwCoreNodeResponseItf build(FwCoreNodeRequestItf nodeRequest)
	{
		return new FwCoreNodeResponse(
			statusCopier.copy(
				nodeRequest.getStatus()
			)
		);
	}

}
