package chatQuest.fw.core.main.service.nodeResponseByNodeRequestBuilder;

import chatQuest.fw.core.main.library.nodeRequest.FwCoreNodeRequestItf;
import chatQuest.fw.core.main.library.nodeResponse.FwCoreNodeResponseItf;

public interface FwCoreNodeResponseByNodeRequestBuilderItf
{

	public FwCoreNodeResponseItf build(FwCoreNodeRequestItf nodeRequest);

}
