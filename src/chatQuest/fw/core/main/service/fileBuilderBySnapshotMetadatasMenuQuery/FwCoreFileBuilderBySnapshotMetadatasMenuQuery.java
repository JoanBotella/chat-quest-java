package chatQuest.fw.core.main.service.fileBuilderBySnapshotMetadatasMenuQuery;

import java.io.File;

import chatQuest.fw.core.main.service.fileBuilderBySnapshotMetadatasMenuQuery.FwCoreFileBuilderBySnapshotMetadatasMenuQueryItf;
import chatQuest.fw.core.main.service.snapshotFilesBuilder.FwCoreSnapshotFilesBuilderItf;

public final class
	FwCoreFileBuilderBySnapshotMetadatasMenuQuery
implements
	FwCoreFileBuilderBySnapshotMetadatasMenuQueryItf
{

	private FwCoreSnapshotFilesBuilderItf snapshotFilesBuilder;

	private File file;
	private Integer index;

	public FwCoreFileBuilderBySnapshotMetadatasMenuQuery(
		FwCoreSnapshotFilesBuilderItf snapshotFilesBuilder
	)
	{
		this.snapshotFilesBuilder = snapshotFilesBuilder;
	}

	public boolean hasFile()
	{
		return file != null;
	}

	public File getFileAfterHas()
	{
		return file;
	}

	public void build(String query)
	{
		file = null;
		index = null;

		tryToSetupIndex(query);

		if (index == null)
		{
			return;
		}

		tryToSetupFile();
	}

		private void tryToSetupIndex(String query)
		{
			try
			{
				index = Integer.parseInt(query);
			}
			catch (Exception e)
			{
				return;
			}

			index = index - 2;
		}

		private void tryToSetupFile()
		{
			File[] files = snapshotFilesBuilder.build();

			if (
				files.length > 0
				&& index < files.length
			)
			{
				file = files[index];
			}
		}

}
