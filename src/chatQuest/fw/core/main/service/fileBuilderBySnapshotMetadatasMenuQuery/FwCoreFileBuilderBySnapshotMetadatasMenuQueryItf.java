package chatQuest.fw.core.main.service.fileBuilderBySnapshotMetadatasMenuQuery;

import java.io.File;

public interface
	FwCoreFileBuilderBySnapshotMetadatasMenuQueryItf
{

	public void build(String query);

	public boolean hasFile();

	public File getFileAfterHas();

}
