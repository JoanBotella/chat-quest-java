package chatQuest.fw.core.main.service.bodyBuilderBySnapshotOverwriter;

import java.util.List;

import chatQuest.fw.core.translation.library.translation.FwCoreTranslationItf;
import chatQuest.fw.core.main.service.bodyBuilderBySnapshotOverwriter.FwCoreBodyBuilderBySnapshotOverwriterItf;
import chatQuest.fw.core.main.service.snapshotOverwriter.FwCoreSnapshotOverwriterErrorConstant;
import chatQuest.fw.core.main.service.snapshotOverwriter.FwCoreSnapshotOverwriterItf;

public final class
	FwCoreBodyBuilderBySnapshotOverwriter
implements
	FwCoreBodyBuilderBySnapshotOverwriterItf
{

	public String build(FwCoreSnapshotOverwriterItf snapshotOverwriter, FwCoreTranslationItf translation)
	{
		List<Integer> errors = snapshotOverwriter.getErrors();

		if (!errors.isEmpty())
		{
			String body = "";

			for (Integer error : errors)
			{
				switch (error)
				{
					case FwCoreSnapshotOverwriterErrorConstant.ERROR_QUERY_DOES_NOT_MATCH_A_FILE:
						body += translation.fwCore_overwriteSnapshotProcess_body_error_queryDoesNotMatchAFile();
						break;
					case FwCoreSnapshotOverwriterErrorConstant.ERROR_SNAPSHOT_FILE_COULD_NOT_BE_WRITTEN:
						body += translation.fwCore_overwriteSnapshotProcess_body_error_snapshotFileCouldNotBeWritten();
						break;
					case FwCoreSnapshotOverwriterErrorConstant.ERROR_SNAPSHOT_FILE_COULD_NOT_BE_CLOSED:
						body += translation.fwCore_overwriteSnapshotProcess_body_error_snapshotFileCouldNotBeClosed();
						break;
				}
			}

			return body;
		}

		return translation.fwCore_overwriteSnapshotProcess_body_success();
	}

}
