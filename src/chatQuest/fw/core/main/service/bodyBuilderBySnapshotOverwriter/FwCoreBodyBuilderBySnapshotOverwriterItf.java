package chatQuest.fw.core.main.service.bodyBuilderBySnapshotOverwriter;

import chatQuest.fw.core.translation.library.translation.FwCoreTranslationItf;
import chatQuest.fw.core.main.service.snapshotOverwriter.FwCoreSnapshotOverwriterItf;

public interface
	FwCoreBodyBuilderBySnapshotOverwriterItf
{

	public String build(FwCoreSnapshotOverwriterItf snapshotOverwriter, FwCoreTranslationItf translation);

}
