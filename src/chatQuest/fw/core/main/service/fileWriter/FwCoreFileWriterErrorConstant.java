package chatQuest.fw.core.main.service.fileWriter;

public final class
	FwCoreFileWriterErrorConstant
{

	public static final int ERROR_FILE_COULD_NOT_BE_WRITTEN = 0;
	public static final int ERROR_FILE_COULD_NOT_BE_CLOSED = 1;

}
