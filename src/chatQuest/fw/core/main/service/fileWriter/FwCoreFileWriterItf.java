package chatQuest.fw.core.main.service.fileWriter;

import java.io.File;
import java.util.List;

public interface
	FwCoreFileWriterItf
{

	public List<Integer> getErrors();

	public void write(File file, String content);

}
