package chatQuest.fw.core.main.service.fileWriter;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import chatQuest.fw.core.main.service.fileWriter.FwCoreFileWriterItf;

public final class
	FwCoreFileWriter
implements
	FwCoreFileWriterItf
{

	private List<Integer> errors;

	public FwCoreFileWriter(
	)
	{
		errors = buildErrors();
	}

		private List<Integer> buildErrors()
		{
			return new ArrayList<Integer>();
		}

	public List<Integer> getErrors()
	{
		return errors;
	}

	public void write(File file, String content)
	{
		FileOutputStream fileOutputStream;

		try
		{
			fileOutputStream = new FileOutputStream(
				file.getAbsolutePath()
			);
		}
		catch (Exception e)
		{
			errors.add(
				FwCoreFileWriterErrorConstant.ERROR_FILE_COULD_NOT_BE_WRITTEN
			);
			return;
		}

		OutputStreamWriter outputStreamWriter = new OutputStreamWriter(
			fileOutputStream,
			StandardCharsets.UTF_8
		);

		Writer writer = new BufferedWriter(outputStreamWriter);

		try
		{
			writer.write(content);
		}
		catch (Exception e)
		{
			errors.add(
				FwCoreFileWriterErrorConstant.ERROR_FILE_COULD_NOT_BE_WRITTEN
			);
		}

		try
		{
			writer.close();
		}
		catch (Exception e)
		{
			errors.add(
				FwCoreFileWriterErrorConstant.ERROR_FILE_COULD_NOT_BE_CLOSED
			);
		}
	}

}
