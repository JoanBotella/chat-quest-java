package chatQuest.fw.core.main.service.statusSerializer;

import chatQuest.fw.core.main.library.status.FwCoreStatusItf;

public interface FwCoreStatusSerializerItf
{

	public String serialize(FwCoreStatusItf status);

}
