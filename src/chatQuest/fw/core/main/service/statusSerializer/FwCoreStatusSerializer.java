package chatQuest.fw.core.main.service.statusSerializer;

import chatQuest.fw.core.main.library.status.FwCoreStatusItf;
import chatQuest.fw.core.main.service.statusJsonizer.FwCoreStatusJsonizerItf;
import chatQuest.fw.core.main.service.statusSerializer.FwCoreStatusSerializerItf;;

public final class
	FwCoreStatusSerializer
implements
	FwCoreStatusSerializerItf
{

	private FwCoreStatusJsonizerItf statusJsonizer;

	public FwCoreStatusSerializer(
		FwCoreStatusJsonizerItf statusJsonizer
	)
	{
		this.statusJsonizer = statusJsonizer;
	}

	public String serialize(FwCoreStatusItf status)
	{
		return statusJsonizer
			.jsonize(status)
			.toString()
		;
	}

}
