package chatQuest.fw.core.main.service.snapshotMetadataJsonizer;

import org.json.JSONObject;
import chatQuest.fw.core.main.library.snapshotMetadata.FwCoreSnapshotMetadataItf;

public interface
	FwCoreSnapshotMetadataJsonizerItf
{

	public JSONObject jsonize(FwCoreSnapshotMetadataItf snapshotMetadata);

}
