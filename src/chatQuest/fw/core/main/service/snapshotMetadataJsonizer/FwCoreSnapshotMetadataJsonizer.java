package chatQuest.fw.core.main.service.snapshotMetadataJsonizer;

import org.json.JSONObject;

import chatQuest.fw.core.main.library.JsonSerializationConstant;
import chatQuest.fw.core.main.library.snapshotMetadata.FwCoreSnapshotMetadataItf;
import chatQuest.fw.core.main.service.snapshotMetadataJsonizer.FwCoreSnapshotMetadataJsonizerItf;

public final class
	FwCoreSnapshotMetadataJsonizer
implements
	FwCoreSnapshotMetadataJsonizerItf
{

	public JSONObject jsonize(FwCoreSnapshotMetadataItf snapshotMetadata)
	{
		JSONObject object = new JSONObject();

		object.put(
			JsonSerializationConstant.SNAPSHOTS_METADATA_NAME_KEY,
			snapshotMetadata.getName()
		);
		object.put(
			JsonSerializationConstant.SNAPSHOTS_METADATA_DATE_TIME_KEY,
			snapshotMetadata.getDateTime().toString()
		);

		return object;
	}

}
