package chatQuest.fw.core.main.service.bodyBuilderBySnapshotCreator;

import java.util.List;

import chatQuest.fw.core.translation.library.translation.FwCoreTranslationItf;
import chatQuest.fw.core.main.service.bodyBuilderBySnapshotCreator.FwCoreBodyBuilderBySnapshotCreatorItf;
import chatQuest.fw.core.main.service.snapshotCreator.FwCoreSnapshotCreatorErrorConstant;
import chatQuest.fw.core.main.service.snapshotCreator.FwCoreSnapshotCreatorItf;

public final class
	FwCoreBodyBuilderBySnapshotCreator
implements
	FwCoreBodyBuilderBySnapshotCreatorItf
{

	public String build(FwCoreSnapshotCreatorItf snapshotCreator, FwCoreTranslationItf translation)
	{
		List<Integer> errors = snapshotCreator.getErrors();

		if (errors.isEmpty())
		{
			return translation.fwCore_createSnapshotProcess_body_success();
		}

		String body = "";

		for (Integer error : errors)
		{
			switch (error)
			{
				case FwCoreSnapshotCreatorErrorConstant.ERROR_SNAPSHOT_DIRECTORY_COULD_NOT_BE_CREATED:
					body += translation.fwCore_createSnapshotProcess_body_error_snapshotsDirectoryCouldNotBeCreated();
					break;
				case FwCoreSnapshotCreatorErrorConstant.ERROR_SNAPSHOT_FILE_COULD_NOT_BE_CREATED:
					body += translation.fwCore_createSnapshotProcess_body_error_snapshotFileCouldNotBeCreated();
					break;
				case FwCoreSnapshotCreatorErrorConstant.ERROR_SNAPSHOT_FILE_COULD_NOT_BE_WRITTEN:
					body += translation.fwCore_createSnapshotProcess_body_error_snapshotFileCouldNotBeWritten();
					break;
				case FwCoreSnapshotCreatorErrorConstant.ERROR_SNAPSHOT_FILE_COULD_NOT_BE_CLOSED:
					body += translation.fwCore_createSnapshotProcess_body_error_snapshotFileCouldNotBeClosed();
					break;
				case FwCoreSnapshotCreatorErrorConstant.ERROR_SNAPSHOT_FILE_COULD_NOT_BE_DELETED:
					body += translation.fwCore_createSnapshotProcess_body_error_snapshotFileCouldNotBeDeleted();
					break;
			}
		}

		return body;
	}

}
