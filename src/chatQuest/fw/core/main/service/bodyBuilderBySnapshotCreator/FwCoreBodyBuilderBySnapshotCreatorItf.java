package chatQuest.fw.core.main.service.bodyBuilderBySnapshotCreator;

import chatQuest.fw.core.translation.library.translation.FwCoreTranslationItf;
import chatQuest.fw.core.main.service.snapshotCreator.FwCoreSnapshotCreatorItf;

public interface
	FwCoreBodyBuilderBySnapshotCreatorItf
{

	public String build(FwCoreSnapshotCreatorItf snapshotCreator, FwCoreTranslationItf translation);

}
