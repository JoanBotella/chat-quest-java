package chatQuest.fw.core.main.service.nextNodeRequestBuilder;

import chatQuest.fw.core.main.library.nodeRequest.FwCoreNodeRequest;
import chatQuest.fw.core.main.library.nodeRequest.FwCoreNodeRequestItf;
import chatQuest.fw.core.main.library.nodeResponse.FwCoreNodeResponseItf;

public final class FwCoreNextNodeRequestBuilder
	implements
		FwCoreNextNodeRequestBuilderItf
{

	public FwCoreNodeRequestItf build(FwCoreNodeResponseItf nodeResponse)
	{
		return new FwCoreNodeRequest(
			nodeResponse.getStatus()
		);
	}

}
