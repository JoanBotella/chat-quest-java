package chatQuest.fw.core.main.service.nextNodeRequestBuilder;

import chatQuest.fw.core.main.library.nodeRequest.FwCoreNodeRequestItf;
import chatQuest.fw.core.main.library.nodeResponse.FwCoreNodeResponseItf;

public interface FwCoreNextNodeRequestBuilderItf
{

	public FwCoreNodeRequestItf build(FwCoreNodeResponseItf nodeResponse);

}
