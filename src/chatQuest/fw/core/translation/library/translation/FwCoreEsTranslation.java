package chatQuest.fw.core.translation.library.translation;

public final class FwCoreEsTranslation
	extends
		FwCoreTranslationAbs
	implements
		FwCoreTranslationItf
{

	public String fwCore_createSnapshotAskName_body() { return "<p>¿Con qué nombre?</p>"; }
	public String fwCore_createSnapshotProcess_body_error_snapshotFileCouldNotBeClosed() { return "<p>El archivo de la instantánea no pudo cerrarse.</p>"; }
	public String fwCore_createSnapshotProcess_body_error_snapshotFileCouldNotBeCreated() { return "<p>El archivo de la instantánea no pudo crearse.</p>"; }
	public String fwCore_createSnapshotProcess_body_error_snapshotFileCouldNotBeDeleted() { return "<p>El archivo de la instantánea no pudo borrarse.</p>"; }
	public String fwCore_createSnapshotProcess_body_error_snapshotFileCouldNotBeWritten() { return "<p>El archivo de la instantánea no pudo escribirse.</p>"; }
	public String fwCore_createSnapshotProcess_body_error_snapshotsDirectoryCouldNotBeCreated() { return "<p>El directorio de instantáneas no pudo crearse.</p>"; }
	public String fwCore_createSnapshotProcess_body_success() { return "<p>Instantánea creada exitosamente.</p>"; }
	public String fwCore_deleteSnapshotMenu_body_error_queryDoesNotMatchAFile() { return "<p>Opción inválida.</p>"; }
	public String fwCore_deleteSnapshotMenu_body_error_snapshotFileCouldNotBeDeleted() { return "<p>El archivo de la instantánea no pudo borrarse.</p>"; }
	public String fwCore_deleteSnapshotMenu_body_error_snapshotFileCouldNotBeFound() { return "<p>El archivo de la instantánea no pudo encontrarse.</p>"; }
	public String fwCore_deleteSnapshotMenu_body_success() { return "<p>Instantánea borrada exitosamente.</p>"; }
	public String fwCore_deleteSnapshotMenu_title() { return "Menú de Borrado de Instantáneas"; }
	public String fwCore_exitMenu_body_bye() { return "<p>¡Adiós!</p>"; }
	public String fwCore_exitMenu_body_menu() { return "<p>¿Realmente quieres salir?</p>"; }
	public String fwCore_exitMenu_option_no() { return "No"; }
	public String fwCore_exitMenu_option_yes() { return "Sí"; }
	public String fwCore_exitMenu_title() { return "Menú de Salida"; }
	public String fwCore_languageMenu_title() { return "Menú de Idioma"; }
	public String fwCore_loadSnapshotMenu_body_error_queryDoesNotMatchAFile() { return "<p>Opción inválida.</p>"; }
	public String fwCore_loadSnapshotMenu_body_error_snapshotFileCouldNotBeClosed() { return "<p>El archivo de la instantánea no pudo cerrarse.</p>"; }
	public String fwCore_loadSnapshotMenu_body_error_snapshotFileCouldNotBeFound() { return "<p>El archivo de la instantánea no pudo encontrarse.</p>"; }
	public String fwCore_loadSnapshotMenu_body_error_snapshotFileCouldNotBeLoaded() { return "<p>La instantánea no pudo cargarse.</p>"; }
	public String fwCore_loadSnapshotMenu_body_error_snapshotFileCouldNotBeRead() { return "<p>El archivo de la instantánea no pudo leerse.</p>"; }
	public String fwCore_loadSnapshotMenu_body_success() { return "<p>Instantánea cargada con éxito.</p>"; }
	public String fwCore_loadSnapshotMenu_title() { return "Menú de Carga de Instantáneas"; }
	public String fwCore_mainMenu_option_exit() { return "Salir"; }
	public String fwCore_mainMenu_option_settings() { return "Preferencias"; }
	public String fwCore_mainMenu_option_snapshots() { return "Instantáneas"; }
	public String fwCore_mainMenu_title() { return "Menú principal"; }
	public String fwCore_overwriteSnapshotAskName_body() { return "<p>¿Con qué nombre?</p>"; }
	public String fwCore_overwriteSnapshotMenu_title() { return "Menú de Sobreescritura de Instantáneas"; }
	public String fwCore_overwriteSnapshotProcess_body_error_queryDoesNotMatchAFile() { return "<p>Opción inválida.</p>"; }
	public String fwCore_overwriteSnapshotProcess_body_error_snapshotFileCouldNotBeClosed() { return "<p>El archivo de la instantánea no pudo cerrarse.</p>"; }
	public String fwCore_overwriteSnapshotProcess_body_error_snapshotFileCouldNotBeFound() { return "<p>El archivo de la instantánea no pudo encontrarse.</p>"; }
	public String fwCore_overwriteSnapshotProcess_body_error_snapshotFileCouldNotBeWritten() { return "<p>El archivo de la instantánea no pudo escribirse.</p>"; }
	public String fwCore_overwriteSnapshotProcess_body_success() { return "<p>Instantánea sobreescrita exitosamente.</p>"; }
	public String fwCore_settingsMenu_option_language() { return "Idioma"; }
	public String fwCore_settingsMenu_title() { return "Menú de Preferencias"; }
	public String fwCore_shared_option_back() { return "Atrás"; }
	public String fwCore_shared_query_look() { return "mirar"; }
	public String fwCore_snapshotsMenu_option_create() { return "Crear"; }
	public String fwCore_snapshotsMenu_option_delete() { return "Borrar"; }
	public String fwCore_snapshotsMenu_option_load() { return "Cargar"; }
	public String fwCore_snapshotsMenu_option_overwrite() { return "Sobreescribir"; }
	public String fwCore_snapshotsMenu_title() { return "Menú de Instantáneas"; }
	public String fwCore_system_body_what() { return "<p>¿Qué?</p>"; }
	public String fwCore_system_query_exit() { return "salir,fin"; }
	public String fwCore_system_query_mainMenu() { return "menu"; }
	public String fwCore_system_query_settingsMenu() { return "preferencias"; }
	public String fwCore_system_query_snapshotsMenu() { return "instantaneas"; }
	public String fwCore_titleMenu_body_bye() { return "<p>¡Adiós!</p>"; }
	public String fwCore_titleMenu_body_letsPlay() { return "<p>¡Juguemos!</p>"; }
	public String fwCore_titleMenu_option_continue() { return "Continuar"; }
	public String fwCore_titleMenu_option_exit() { return "Salir"; }
	public String fwCore_titleMenu_option_loadSnapshot() { return "Cargar Instantánea"; }
	public String fwCore_titleMenu_option_settings() { return "Preferencias"; }
	public String fwCore_titleMenu_option_start() { return "Empezar"; }
	public String fwCore_titleMenu_title() { return "Menú de Título"; }

}
