package chatQuest.fw.core.translation.library.translation;

public abstract class FwCoreTranslationAbs
{

	public String fwCore_languageMenu_option_en() { return "English"; }
	public String fwCore_languageMenu_option_es() { return "Castillian - Castellano"; }
	public String fwCore_system_query_debugGetStatus() { return "debug get status"; }

}
