package chatQuest.fw.core.translation.library.translation;

public interface FwCoreTranslationItf
{

	public String fwCore_createSnapshotAskName_body();
	public String fwCore_createSnapshotProcess_body_error_snapshotFileCouldNotBeClosed();
	public String fwCore_createSnapshotProcess_body_error_snapshotFileCouldNotBeCreated();
	public String fwCore_createSnapshotProcess_body_error_snapshotFileCouldNotBeDeleted();
	public String fwCore_createSnapshotProcess_body_error_snapshotFileCouldNotBeWritten();
	public String fwCore_createSnapshotProcess_body_error_snapshotsDirectoryCouldNotBeCreated();
	public String fwCore_createSnapshotProcess_body_success();
	public String fwCore_deleteSnapshotMenu_body_error_queryDoesNotMatchAFile();
	public String fwCore_deleteSnapshotMenu_body_error_snapshotFileCouldNotBeDeleted();
	public String fwCore_deleteSnapshotMenu_body_error_snapshotFileCouldNotBeFound();
	public String fwCore_deleteSnapshotMenu_body_success();
	public String fwCore_deleteSnapshotMenu_title();
	public String fwCore_exitMenu_body_bye();
	public String fwCore_exitMenu_body_menu();
	public String fwCore_exitMenu_option_no();
	public String fwCore_exitMenu_option_yes();
	public String fwCore_exitMenu_title();
	public String fwCore_languageMenu_option_en();
	public String fwCore_languageMenu_option_es();
	public String fwCore_languageMenu_title();
	public String fwCore_loadSnapshotMenu_body_error_queryDoesNotMatchAFile();
	public String fwCore_loadSnapshotMenu_body_error_snapshotFileCouldNotBeClosed();
	public String fwCore_loadSnapshotMenu_body_error_snapshotFileCouldNotBeFound();
	public String fwCore_loadSnapshotMenu_body_error_snapshotFileCouldNotBeLoaded();
	public String fwCore_loadSnapshotMenu_body_error_snapshotFileCouldNotBeRead();
	public String fwCore_loadSnapshotMenu_body_success();
	public String fwCore_loadSnapshotMenu_title();
	public String fwCore_mainMenu_option_exit();
	public String fwCore_mainMenu_option_settings();
	public String fwCore_mainMenu_option_snapshots();
	public String fwCore_mainMenu_title();
	public String fwCore_overwriteSnapshotAskName_body();
	public String fwCore_overwriteSnapshotMenu_title();
	public String fwCore_overwriteSnapshotProcess_body_error_queryDoesNotMatchAFile();
	public String fwCore_overwriteSnapshotProcess_body_error_snapshotFileCouldNotBeClosed();
	public String fwCore_overwriteSnapshotProcess_body_error_snapshotFileCouldNotBeFound();
	public String fwCore_overwriteSnapshotProcess_body_error_snapshotFileCouldNotBeWritten();
	public String fwCore_overwriteSnapshotProcess_body_success();
	public String fwCore_settingsMenu_option_language();
	public String fwCore_settingsMenu_title();
	public String fwCore_shared_option_back();
	public String fwCore_shared_query_look();
	public String fwCore_snapshotsMenu_option_create();
	public String fwCore_snapshotsMenu_option_delete();
	public String fwCore_snapshotsMenu_option_load();
	public String fwCore_snapshotsMenu_option_overwrite();
	public String fwCore_snapshotsMenu_title();
	public String fwCore_system_body_what();
	public String fwCore_system_query_debugGetStatus();
	public String fwCore_system_query_exit();
	public String fwCore_system_query_mainMenu();
	public String fwCore_system_query_settingsMenu();
	public String fwCore_system_query_snapshotsMenu();
	public String fwCore_titleMenu_body_bye();
	public String fwCore_titleMenu_body_letsPlay();
	public String fwCore_titleMenu_option_continue();
	public String fwCore_titleMenu_option_exit();
	public String fwCore_titleMenu_option_loadSnapshot();
	public String fwCore_titleMenu_option_settings();
	public String fwCore_titleMenu_option_start();
	public String fwCore_titleMenu_title();

}
