package chatQuest.fw.core.translation.library.translation;

public final class FwCoreEnTranslation
	extends
		FwCoreTranslationAbs
	implements
		FwCoreTranslationItf
{

	public String fwCore_createSnapshotAskName_body() { return "<p>With what name?</p>"; }
	public String fwCore_createSnapshotProcess_body_error_snapshotFileCouldNotBeClosed() { return "<p>The snapshot file could not be closed.</p>"; }
	public String fwCore_createSnapshotProcess_body_error_snapshotFileCouldNotBeCreated() { return "<p>The snapshot file could not be created.</p>"; }
	public String fwCore_createSnapshotProcess_body_error_snapshotFileCouldNotBeDeleted() { return "<p>The snapshot file could not be deleted.</p>"; }
	public String fwCore_createSnapshotProcess_body_error_snapshotFileCouldNotBeWritten() { return "<p>The snapshot file could not be written.</p>"; }
	public String fwCore_createSnapshotProcess_body_error_snapshotsDirectoryCouldNotBeCreated() { return "<p>The snapshots directory could not be created.</p>"; }
	public String fwCore_createSnapshotProcess_body_success() { return "<p>Snapshot created successfully.</p>"; }
	public String fwCore_deleteSnapshotMenu_body_error_queryDoesNotMatchAFile() { return "<p>Invalid option.</p>"; }
	public String fwCore_deleteSnapshotMenu_body_error_snapshotFileCouldNotBeDeleted() { return "<p>The snapshot file could not be deleted.</p>"; }
	public String fwCore_deleteSnapshotMenu_body_error_snapshotFileCouldNotBeFound() { return "<p>The snapshot file could not be found.</p>"; }
	public String fwCore_deleteSnapshotMenu_body_success() { return "<p>Snapshot deleted successfully.</p>"; }
	public String fwCore_deleteSnapshotMenu_title() { return "Delete Snapshot Menu"; }
	public String fwCore_exitMenu_body_bye() { return "<p>Bye!</p>"; }
	public String fwCore_exitMenu_body_menu() { return "<p>Are you sure?</p>"; }
	public String fwCore_exitMenu_option_no() { return "No"; }
	public String fwCore_exitMenu_option_yes() { return "Yes"; }
	public String fwCore_exitMenu_title() { return "Exit Menu"; }
	public String fwCore_languageMenu_title() { return "Language Menu"; }
	public String fwCore_loadSnapshotMenu_body_error_queryDoesNotMatchAFile() { return "<p>Invalid option.</p>"; }
	public String fwCore_loadSnapshotMenu_body_error_snapshotFileCouldNotBeClosed() { return "<p>The snapshot file could not be closed.</p>"; }
	public String fwCore_loadSnapshotMenu_body_error_snapshotFileCouldNotBeFound() { return "<p>The snapshot file could not be found.</p>"; }
	public String fwCore_loadSnapshotMenu_body_error_snapshotFileCouldNotBeLoaded() { return "<p>The snapshot could not be loaded.</p>"; }
	public String fwCore_loadSnapshotMenu_body_error_snapshotFileCouldNotBeRead() { return "<p>The snapshot file could not be read.</p>"; }
	public String fwCore_loadSnapshotMenu_body_success() { return "<p>Snapshot loaded successfully.</p>"; }
	public String fwCore_loadSnapshotMenu_title() { return "Load Snapshot Menu"; }
	public String fwCore_mainMenu_option_exit() { return "Exit"; }
	public String fwCore_mainMenu_option_settings() { return "Settings"; }
	public String fwCore_mainMenu_option_snapshots() { return "Snapshots"; }
	public String fwCore_mainMenu_title() { return "Main Menu"; }
	public String fwCore_overwriteSnapshotAskName_body() { return "<p>With what name?</p>"; }
	public String fwCore_overwriteSnapshotMenu_title() { return "Overwrite Snapshot Menu"; }
	public String fwCore_overwriteSnapshotProcess_body_error_queryDoesNotMatchAFile() { return "<p>Invalid option.</p>"; }
	public String fwCore_overwriteSnapshotProcess_body_error_snapshotFileCouldNotBeClosed() { return "<p>The snapshot file could not be closed.</p>"; }
	public String fwCore_overwriteSnapshotProcess_body_error_snapshotFileCouldNotBeFound() { return "<p>The snapshot file could not be found.</p>"; }
	public String fwCore_overwriteSnapshotProcess_body_error_snapshotFileCouldNotBeWritten() { return "<p>The snapshot file could not be written.</p>"; }
	public String fwCore_overwriteSnapshotProcess_body_success() { return "<p>Snapshot overwritten successfully.</p>"; }
	public String fwCore_settingsMenu_option_language() { return "Language"; }
	public String fwCore_settingsMenu_title() { return "Settings Menu"; }
	public String fwCore_shared_option_back() { return "Back"; }
	public String fwCore_shared_query_look() { return "look"; }
	public String fwCore_snapshotsMenu_option_create() { return "Create"; }
	public String fwCore_snapshotsMenu_option_delete() { return "Delete"; }
	public String fwCore_snapshotsMenu_option_load() { return "Load"; }
	public String fwCore_snapshotsMenu_option_overwrite() { return "Overwrite"; }
	public String fwCore_snapshotsMenu_title() { return "Snapshots Menu"; }
	public String fwCore_system_body_what() { return "<p>What?</p>"; }
	public String fwCore_system_query_exit() { return "exit,quit,end"; }
	public String fwCore_system_query_mainMenu() { return "menu"; }
	public String fwCore_system_query_settingsMenu() { return "settings"; }
	public String fwCore_system_query_snapshotsMenu() { return "snapshots"; }
	public String fwCore_titleMenu_body_bye() { return "<p>Bye!</p>"; }
	public String fwCore_titleMenu_body_letsPlay() { return "<p>Let's play!</p>"; }
	public String fwCore_titleMenu_option_continue() { return "Continue"; }
	public String fwCore_titleMenu_option_exit() { return "Exit"; }
	public String fwCore_titleMenu_option_loadSnapshot() { return "Load Snapshot"; }
	public String fwCore_titleMenu_option_settings() { return "Settings"; }
	public String fwCore_titleMenu_option_start() { return "Start"; }
	public String fwCore_titleMenu_title() { return "Title Menu"; }

}
