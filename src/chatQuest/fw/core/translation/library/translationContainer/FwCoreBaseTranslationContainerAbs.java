package chatQuest.fw.core.translation.library.translationContainer;

import java.util.HashMap;
import java.util.Map;

public abstract class FwCoreBaseTranslationContainerAbs<
	TTranslation
>
	implements
		FwCoreBaseTranslationContainerItf<
			TTranslation
		>
{

	private Map<String, TTranslation> translations;
	private TTranslation translation;

	public FwCoreBaseTranslationContainerAbs(
	)
	{
		translations = new HashMap<String, TTranslation>();
	}

	public TTranslation get(String languageCode)
	{
		if (!translations.containsKey(languageCode))
		{
			translations.put(
				languageCode,
				build(languageCode)
			);
		}
		return translations.get(languageCode);
	}

		private TTranslation build(String languageCode)
		{
			setupTranslation(languageCode);

			if (translation == null)
			{
				throw new RuntimeException("There is no translation for the language code " + languageCode);
			}

			TTranslation built = translation;
			translation = null;
			return built;
		}

			protected abstract void setupTranslation(String languageCode);

	protected boolean hasTranslation()
	{
		return translation != null;
	}

	protected void setTranslation(TTranslation translation)
	{
		this.translation = translation;
	}

}
