package chatQuest.fw.core.translation.library.translationContainer;

public interface FwCoreBaseTranslationContainerItf<
	TTranslation
>
{

	public TTranslation get(String languageCode);

}
