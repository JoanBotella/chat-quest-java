package chatQuest.fw.core.translation.service.translationContainer;

import chatQuest.fw.core.main.library.language.FwCoreLanguageCode;
import chatQuest.fw.core.translation.library.translation.FwCoreEnTranslation;
import chatQuest.fw.core.translation.library.translation.FwCoreEsTranslation;
import chatQuest.fw.core.translation.library.translation.FwCoreTranslationItf;
import chatQuest.fw.core.translation.library.translationContainer.FwCoreBaseTranslationContainerAbs;

public final class FwCoreTranslationContainer
	extends
		FwCoreBaseTranslationContainerAbs<
			FwCoreTranslationItf
		>
	implements
		FwCoreTranslationContainerItf
{

	protected void setupTranslation(String languageCode)
	{
		FwCoreTranslationItf translation;

		switch (languageCode)
		{
			case FwCoreLanguageCode.EN:
				translation = buildEnTranslation();
				break;
			case FwCoreLanguageCode.ES:
				translation = buildEsTranslation();
				break;
			default:
				return;
		}

		setTranslation(translation);
	}

		protected FwCoreTranslationItf buildEnTranslation()
		{
			return new FwCoreEnTranslation();
		}

		protected FwCoreTranslationItf buildEsTranslation()
		{
			return new FwCoreEsTranslation();
		}

}
