package chatQuest.fw.core.translation.service.translationContainer;

import chatQuest.fw.core.translation.library.translation.FwCoreTranslationItf;
import chatQuest.fw.core.translation.library.translationContainer.FwCoreBaseTranslationContainerItf;

public interface FwCoreTranslationContainerItf
	extends
		FwCoreBaseTranslationContainerItf<
			FwCoreTranslationItf
		>
{

}
