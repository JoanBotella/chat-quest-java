package chatQuest.fw.cli.translation.service.translationContainer;

import chatQuest.fw.cli.translation.library.translation.FwCliTranslationItf;
import chatQuest.fw.core.translation.library.translationContainer.FwCoreBaseTranslationContainerItf;

public interface FwCliTranslationContainerItf
	extends
		FwCoreBaseTranslationContainerItf<
			FwCliTranslationItf
		>
{
	
}
