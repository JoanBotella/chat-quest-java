package chatQuest.fw.cli.translation.service.translationContainer;

import chatQuest.fw.cli.translation.library.translation.FwCliEnTranslation;
import chatQuest.fw.cli.translation.library.translation.FwCliEsTranslation;
import chatQuest.fw.cli.translation.library.translation.FwCliTranslationItf;
import chatQuest.fw.core.main.library.language.FwCoreLanguageCode;
import chatQuest.fw.core.translation.library.translationContainer.FwCoreBaseTranslationContainerAbs;

public abstract class FwCliTranslationContainer
	extends
		FwCoreBaseTranslationContainerAbs<
			FwCliTranslationItf
		>
	implements
		FwCliTranslationContainerItf
{

	protected void setupTranslation(String languageCode)
	{
		FwCliTranslationItf translation;

		switch (languageCode)
		{
			case FwCoreLanguageCode.EN:
				translation = buildEnTranslation();
				break;
			case FwCoreLanguageCode.ES:
				translation = buildEsTranslation();
				break;
			default:
				return;
		}

		setTranslation(translation);
	}

		protected FwCliTranslationItf buildEnTranslation()
		{
			return new FwCliEnTranslation();
		}

		protected FwCliTranslationItf buildEsTranslation()
		{
			return new FwCliEsTranslation();
		}

}
