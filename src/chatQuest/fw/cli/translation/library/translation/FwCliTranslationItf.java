package chatQuest.fw.cli.translation.library.translation;

public interface FwCliTranslationItf
{

	public String cli_slidePrompt();

	public String cli_queryPrompt();

}
