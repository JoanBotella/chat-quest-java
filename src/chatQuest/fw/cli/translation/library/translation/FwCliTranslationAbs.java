package chatQuest.fw.cli.translation.library.translation;

public abstract class FwCliTranslationAbs
	implements
		FwCliTranslationItf
{

	public String cli_slidePrompt() { return "..."; }

	public String cli_queryPrompt() { return "> "; }

}
