package chatQuest.fw.cli.main.library.queryNormalizer;

public final class FwCliEsQueryNormalizer
	implements
		FwCliQueryNormalizerItf
{

	public String normalize(String query)
	{
		query = query.toLowerCase();

		query = query.replaceAll("á", "a");
		query = query.replaceAll("é", "e");
		query = query.replaceAll("í", "i");
		query = query.replaceAll("ó", "o");
		query = query.replaceAll("ú", "u");

		query = query.replaceAll(" (el|la|los|las|hacia|a|al|en|con) ", " ");

		return query;
	}

}
