package chatQuest.fw.cli.main.library.queryNormalizer;

public final class FwCliEnQueryNormalizer
	implements
		FwCliQueryNormalizerItf
{

	public String normalize(String query)
	{
		query = query.toLowerCase();

		query = query.replaceAll(" (a|the|at|to|through|up) ", " ");

		return query;
	}

}
