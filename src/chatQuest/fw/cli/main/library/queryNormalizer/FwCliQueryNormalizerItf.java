package chatQuest.fw.cli.main.library.queryNormalizer;

public interface FwCliQueryNormalizerItf
{

	public String normalize(String query);

}
