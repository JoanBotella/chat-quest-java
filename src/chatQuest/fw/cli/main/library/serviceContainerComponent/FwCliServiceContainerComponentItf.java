package chatQuest.fw.cli.main.library.serviceContainerComponent;

import chatQuest.fw.cli.main.service.appRequestBuilder.FwCliAppRequestBuilderItf;
import chatQuest.fw.cli.main.service.appResponseDispatcher.FwCliAppResponseDispatcherItf;
import chatQuest.fw.cli.main.service.appRunner.FwCliAppRunnerItf;
import chatQuest.fw.cli.main.service.prompter.FwCliPrompterItf;
import chatQuest.fw.cli.main.service.queryBuilder.FwCliQueryBuilderItf;
import chatQuest.fw.cli.main.service.queryNormalizerContainer.FwCliQueryNormalizerContainerItf;
import chatQuest.fw.cli.main.service.stylizer.FwCliStylizerItf;
import chatQuest.fw.cli.translation.service.translationContainer.FwCliTranslationContainerItf;

public interface FwCliServiceContainerComponentItf
{

	public FwCliAppRequestBuilderItf getFwCliAppRequestBuilder();

	public FwCliAppResponseDispatcherItf getFwCliAppResponseDispatcher();

	public FwCliAppRunnerItf getFwCliAppRunner();

	public FwCliPrompterItf getFwCliPrompter();

	public FwCliQueryBuilderItf getFwCliQueryBuilder();

	public FwCliQueryNormalizerContainerItf getFwCliQueryNormalizerContainer();

	public FwCliStylizerItf getFwCliStylizer();

	public FwCliTranslationContainerItf getFwCliTranslationContainer();

}
