package chatQuest.fw.cli.main.library.queryNormalizerContainer;

import chatQuest.fw.cli.main.library.queryNormalizer.FwCliQueryNormalizerItf;

public interface FwCliBaseQueryNormalizerContainerItf
{

	public FwCliQueryNormalizerItf get(String languageCode);

}
