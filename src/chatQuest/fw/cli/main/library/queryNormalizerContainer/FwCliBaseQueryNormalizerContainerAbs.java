package chatQuest.fw.cli.main.library.queryNormalizerContainer;

import java.util.HashMap;
import java.util.Map;

import chatQuest.fw.cli.main.library.queryNormalizer.FwCliQueryNormalizerItf;

public abstract class FwCliBaseQueryNormalizerContainerAbs
	implements
		FwCliBaseQueryNormalizerContainerItf
{

	private Map<String, FwCliQueryNormalizerItf> queryNormalizers;
	private FwCliQueryNormalizerItf queryNormalizer;

	public FwCliBaseQueryNormalizerContainerAbs(
	)
	{
		queryNormalizers = new HashMap<String, FwCliQueryNormalizerItf>();
	}

	public FwCliQueryNormalizerItf get(String languageCode)
	{
		if (!queryNormalizers.containsKey(languageCode))
		{
			queryNormalizers.put(
				languageCode,
				build(languageCode)
			);
		}
		return queryNormalizers.get(languageCode);
	}

		private FwCliQueryNormalizerItf build(String languageCode)
		{
			setupQueryNormalizer(languageCode);

			if (queryNormalizer == null)
			{
				throw new RuntimeException("There is no QueryNormalizer for the language code \"" + languageCode + "\"");
			}

			FwCliQueryNormalizerItf built = queryNormalizer;
			queryNormalizer = null;
			return built;
		}

		protected abstract void setupQueryNormalizer(String languageCode);

	protected boolean hasQueryNormalizer()
	{
		return queryNormalizer != null;
	}

	protected void setQueryNormalizer(FwCliQueryNormalizerItf queryNormalizer)
	{
		this.queryNormalizer = queryNormalizer;
	}

}
