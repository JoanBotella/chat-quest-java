package chatQuest.fw.cli.main.service.appRequestBuilder;

import chatQuest.fw.core.main.library.appRequest.FwCoreAppRequestItf;
import chatQuest.fw.core.main.library.status.FwCoreStatusItf;

public interface FwCliAppRequestBuilderItf
{

	public void tryToBuildWithQuery(FwCoreStatusItf status);

	public void tryToBuildWithoutQuery(FwCoreStatusItf status);

	public boolean hasAppRequest();

	public FwCoreAppRequestItf getAppRequestAfterHas();

}
