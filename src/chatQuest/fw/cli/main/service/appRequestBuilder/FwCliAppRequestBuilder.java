package chatQuest.fw.cli.main.service.appRequestBuilder;

import chatQuest.fw.cli.main.service.queryBuilder.FwCliQueryBuilderItf;
import chatQuest.fw.core.main.library.appRequest.FwCoreAppRequest;
import chatQuest.fw.core.main.library.appRequest.FwCoreAppRequestItf;
import chatQuest.fw.core.main.library.status.FwCoreStatusItf;

public final class FwCliAppRequestBuilder
	implements
		FwCliAppRequestBuilderItf
{

	private FwCliQueryBuilderItf queryBuilder;

	private FwCoreAppRequestItf appRequest;

	public FwCliAppRequestBuilder(
		FwCliQueryBuilderItf queryBuilder
	)
	{
		this.queryBuilder = queryBuilder;
	}

	public void tryToBuildWithoutQuery(FwCoreStatusItf status)
	{
		appRequest = new FwCoreAppRequest(status);
	}

	public void tryToBuildWithQuery(FwCoreStatusItf status)
	{
		tryToBuildWithoutQuery(status);

		queryBuilder.tryToBuild(
			status.getSettings().getLanguageCode()
		);

		if (queryBuilder.hasQuery())
		{
			appRequest.setQuery(
				queryBuilder.getQueryAfterHas()
			);
		}
	}

	public boolean hasAppRequest()
	{
		return appRequest != null;
	}

	public FwCoreAppRequestItf getAppRequestAfterHas()
	{
		return appRequest;
	}

}
