package chatQuest.fw.cli.main.service.queryNormalizerContainer;

import chatQuest.fw.cli.main.library.queryNormalizer.FwCliEnQueryNormalizer;
import chatQuest.fw.cli.main.library.queryNormalizer.FwCliEsQueryNormalizer;
import chatQuest.fw.cli.main.library.queryNormalizer.FwCliQueryNormalizerItf;
import chatQuest.fw.cli.main.library.queryNormalizerContainer.FwCliBaseQueryNormalizerContainerAbs;
import chatQuest.fw.core.main.library.language.FwCoreLanguageCode;

public final class FwCliQueryNormalizerContainer
	extends
		FwCliBaseQueryNormalizerContainerAbs
	implements
		FwCliQueryNormalizerContainerItf
{

	protected void setupQueryNormalizer(String languageCode)
	{
		FwCliQueryNormalizerItf queryNormalizer;

		switch (languageCode)
		{
			case FwCoreLanguageCode.EN:
				queryNormalizer = buildCliEnQueryNormalizer();
				break;
			case FwCoreLanguageCode.ES:
				queryNormalizer = buildCliEsQueryNormalizer();
				break;
			default:
				return;
		}

		setQueryNormalizer(queryNormalizer);
	}

		protected FwCliQueryNormalizerItf buildCliEnQueryNormalizer()
		{
			return new FwCliEnQueryNormalizer();
		}

		protected FwCliQueryNormalizerItf buildCliEsQueryNormalizer()
		{
			return new FwCliEsQueryNormalizer();
		}

}
