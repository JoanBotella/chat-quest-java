package chatQuest.fw.cli.main.service.prompter;

public interface
	FwCliPrompterItf
{

	public String prompt(String question);

}