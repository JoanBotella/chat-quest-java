package chatQuest.fw.cli.main.service.prompter;

import java.util.Scanner;

public final class FwCliPrompter
	implements
		FwCliPrompterItf
{

	private Scanner scanner;

	public FwCliPrompter(
	)
	{
		scanner = new Scanner(System.in);
	}

	public String prompt(String question)
	{
		System.out.print(question);
		return scanner.nextLine();
	}

}