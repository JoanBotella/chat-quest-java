package chatQuest.fw.cli.main.service.stylizer;

public final class FwCliStylizer
	implements
		FwCliStylizerItf
{

	// https://gist.github.com/iamnewton/8754917

	// Regular colors

	static final String FG_BLACK = "\u001b[0;30m";
	static final String FG_RED = "\u001b[0;31m";
	static final String FG_GREEN = "\u001b[0;32m";
	static final String FG_YELLOW = "\u001b[0;33m";
	static final String FG_BLUE = "\u001b[0;34m";
	static final String FG_MAGENTA = "\u001b[0;35m";
	static final String FG_CYAN = "\u001b[0;36m";
	static final String FG_WHITE = "\u001b[0;37m";

	// Bold

	static final String BOLD_BLACK = "\u001b[1;30m";
	static final String BOLD_RED = "\u001b[1;31m";
	static final String BOLD_GREEN = "\u001b[1;32m";
	static final String BOLD_YELLOW = "\u001b[1;33m";
	static final String BOLD_BLUE = "\u001b[1;34m";
	static final String BOLD_MAGENTA = "\u001b[1;35m";
	static final String BOLD_CYAN = "\u001b[1;36m";
	static final String BOLD_WHITE = "\u001b[1;37m";

	// Underline

	static final String UNDERLINE_BLACK = "\u001b[4;30m";
	static final String UNDERLINE_RED = "\u001b[4;31m";
	static final String UNDERLINE_GREEN = "\u001b[4;32m";
	static final String UNDERLINE_YELLOW = "\u001b[4;33m";
	static final String UNDERLINE_BLUE = "\u001b[4;34m";
	static final String UNDERLINE_MAGENTA = "\u001b[4;35m";
	static final String UNDERLINE_CYAN = "\u001b[4;36m";
	static final String UNDERLINE_WHITE = "\u001b[4;37m";

	// Background

	static final String BG_BLACK = "\u001b[40m";
	static final String BG_RED = "\u001b[41m";
	static final String BG_GREEN = "\u001b[42m";
	static final String BG_YELLOW = "\u001b[43m";
	static final String BG_BLUE = "\u001b[44m";
	static final String BG_MAGENTA = "\u001b[45m";
	static final String BG_CYAN = "\u001b[46m";
	static final String BG_WHITE = "\u001b[47m";

	// High intensity

	static final String HI_FG_BLACK = "\u001b[0;90m";
	static final String HI_FG_RED = "\u001b[0;91m";
	static final String HI_FG_GREEN = "\u001b[0;92m";
	static final String HI_FG_YELLOW = "\u001b[0;93m";
	static final String HI_FG_BLUE = "\u001b[0;94m";
	static final String HI_FG_MAGENTA = "\u001b[0;95m";
	static final String HI_FG_CYAN = "\u001b[0;96m";
	static final String HI_FG_WHITE = "\u001b[0;97m";

	// Bold high intensity

	static final String HI_BOLD_BLACK = "\u001b[1;90m";
	static final String HI_BOLD_RED = "\u001b[1;91m";
	static final String HI_BOLD_GREEN = "\u001b[1;92m";
	static final String HI_BOLD_YELLOW = "\u001b[1;93m";
	static final String HI_BOLD_BLUE = "\u001b[1;94m";
	static final String HI_BOLD_MAGENTA = "\u001b[1;95m";
	static final String HI_BOLD_CYAN = "\u001b[1;96m";
	static final String HI_BOLD_WHITE = "\u001b[1;97m";

	// Bold high intensity background

	static final String HI_BG_BLACK = "\u001b[0,100m";
	static final String HI_BG_RED = "\u001b[0,101m";
	static final String HI_BG_GREEN = "\u001b[0,102m";
	static final String HI_BG_YELLOW = "\u001b[0,103m";
	static final String HI_BG_BLUE = "\u001b[0,104m";
	static final String HI_BG_MAGENTA = "\u001b[0,105m";
	static final String HI_BG_CYAN = "\u001b[0,106m";
	static final String HI_BG_WHITE = "\u001b[0,107m";

	// Styles

	static final String STYLE_RESET = "\u001b[0m";
	static final String STYLE_BRIGHT = "\u001b[1m";
	static final String STYLE_DIM = "\u001b[2m";
	static final String STYLE_ITALIC = "\u001b[3m";
	static final String STYLE_UNDERSCORE = "\u001b[4m";
	static final String STYLE_BLINK = "\u001b[5m";
	static final String STYLE_REVERSE = "\u001b[7m";
	static final String STYLE_HIDDEN = "\u001b[8m";
	static final String STYLE_STRIKETHROUG = "\u001b[9m";

	public String stylizeBody(String body)
	{
		body = body.replaceAll("<br />", "\n");

		body = body.replace("<p>", "\n");
		body = body.replace("<p class=\"talking\">", "\n" + FwCliStylizer.STYLE_ITALIC);
		body = body.replace("</p>", "\n" + FwCliStylizer.STYLE_RESET);

		body = body.replace("<span class=\"character\">", FwCliStylizer.FG_GREEN);
		body = body.replace("<span class=\"verb\">", FwCliStylizer.FG_YELLOW);
		body = body.replace("<span class=\"item\">", FwCliStylizer.FG_BLUE);
		body = body.replace("<span class=\"exit\">", FwCliStylizer.FG_RED);

		body = body.replace("</span>", FwCliStylizer.STYLE_RESET);

		return body;
	}

	public String stylizeTitle(String text)
	{
		return '\n' + FwCliStylizer.STYLE_BRIGHT + text + FwCliStylizer.STYLE_RESET;
	}

	public String stylizeSlidePrompt(String text)
	{
		return FwCliStylizer.HI_FG_BLACK + text + FwCliStylizer.STYLE_RESET;
	}

	public String stylizeQueryPrompt(String text)
	{
		return FwCliStylizer.HI_FG_BLACK + text + FwCliStylizer.STYLE_RESET;
	}

}
