package chatQuest.fw.cli.main.service.stylizer;

public interface
	FwCliStylizerItf
{

	public String stylizeBody(String body);

	public String stylizeTitle(String text);

	public String stylizeSlidePrompt(String text);

	public String stylizeQueryPrompt(String text);

}
