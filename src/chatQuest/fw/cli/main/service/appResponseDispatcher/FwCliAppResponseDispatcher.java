package chatQuest.fw.cli.main.service.appResponseDispatcher;

import java.util.List;

import chatQuest.fw.cli.translation.library.translation.FwCliTranslationItf;
import chatQuest.fw.cli.main.service.prompter.FwCliPrompterItf;
import chatQuest.fw.cli.main.service.stylizer.FwCliStylizerItf;
import chatQuest.fw.cli.translation.service.translationContainer.FwCliTranslationContainerItf;
import chatQuest.fw.core.main.library.appResponse.FwCoreAppResponseItf;
import chatQuest.fw.core.main.library.slide.FwCoreSlideItf;

public final class FwCliAppResponseDispatcher
	implements
		FwCliAppResponseDispatcherItf
{

	private FwCliStylizerItf stylizer;
	private FwCliPrompterItf prompter;
	private FwCliTranslationContainerItf translationContainer;

	public FwCliAppResponseDispatcher(
		FwCliStylizerItf stylizer,
		FwCliPrompterItf prompter,
		FwCliTranslationContainerItf translationContainer
	)
	{
		this.stylizer = stylizer;
		this.prompter = prompter;
		this.translationContainer = translationContainer;
	}

	public void dispatch(FwCoreAppResponseItf appResponse)
	{
		List<FwCoreSlideItf> slides = appResponse.getSlides();
		String prompt = buildPrompt(appResponse);
		int slidesSize = slides.size();

		for (int index = 0; index < slidesSize; index++)
		{
			if (index > 0)
			{
				prompter.prompt(
					stylizer.stylizeSlidePrompt(prompt)
				);
			}

			dispatchSlide(
				slides.get(index)
			);
		}
	}

		private String buildPrompt(FwCoreAppResponseItf appResponse)
		{
			String languageCode = appResponse.getStatus().getSettings().getLanguageCode();
			FwCliTranslationItf translation = translationContainer.get(languageCode);
			return translation.cli_slidePrompt();
		}

		private void dispatchSlide(FwCoreSlideItf slide)
		{
			if (slide.hasTitle())
			{
				System.out.println(
					stylizer.stylizeTitle(
						slide.getTitleAfterHas()
					)
				);
			}

			if (slide.hasBody())
			{
				System.out.println(
					stylizer.stylizeBody(
						slide.getBodyAfterHas()
					)
				);
			}
		}

}
