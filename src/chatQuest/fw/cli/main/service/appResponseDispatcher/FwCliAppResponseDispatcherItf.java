package chatQuest.fw.cli.main.service.appResponseDispatcher;

import chatQuest.fw.core.main.library.appResponse.FwCoreAppResponseItf;

public interface FwCliAppResponseDispatcherItf
{

	public void dispatch(FwCoreAppResponseItf coreAppResponse);

}
