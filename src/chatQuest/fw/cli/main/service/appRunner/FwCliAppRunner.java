package chatQuest.fw.cli.main.service.appRunner;

import chatQuest.fw.cli.main.service.appRequestBuilder.FwCliAppRequestBuilderItf;
import chatQuest.fw.cli.main.service.appResponseDispatcher.FwCliAppResponseDispatcherItf;
import chatQuest.fw.core.main.library.appRequest.FwCoreAppRequestItf;
import chatQuest.fw.core.main.library.appResponse.FwCoreAppResponseItf;
import chatQuest.fw.core.main.library.status.FwCoreStatusItf;
import chatQuest.fw.core.main.service.app.FwCoreAppItf;
import chatQuest.fw.core.main.service.startingStatusBuilder.FwCoreStartingStatusBuilderItf;

public final class FwCliAppRunner
	implements
		FwCliAppRunnerItf
{

	private FwCoreStartingStatusBuilderItf startingStatusBuilder;
	private FwCliAppRequestBuilderItf appRequestBuilder;
	private FwCoreAppItf app;
	private FwCliAppResponseDispatcherItf appResponseDispatcher;

	private boolean firstStep;
	private boolean end;

	public FwCliAppRunner(
		FwCoreStartingStatusBuilderItf startingStatusBuilder,
		FwCliAppRequestBuilderItf appRequestBuilder,
		FwCoreAppItf app,
		FwCliAppResponseDispatcherItf appResponseDispatcher
	)
	{
		this.startingStatusBuilder = startingStatusBuilder;
		this.appRequestBuilder = appRequestBuilder;
		this.app = app;
		this.appResponseDispatcher = appResponseDispatcher;

		firstStep = true;
		end = false;
	}

	public void run()
	{
		firstStep = true;

		FwCoreStatusItf status = startingStatusBuilder.build();

		while (!end)
		{
			status = step(status);
		}
	}

		private FwCoreStatusItf step(FwCoreStatusItf status)
		{
			FwCoreAppRequestItf appRequest = buildAppRequest(status);

			FwCoreAppResponseItf appResponse = app.run(appRequest);

			appResponseDispatcher.dispatch(appResponse);

			end = appResponse.getEnd();

			return appResponse.getStatus();
		}

			private FwCoreAppRequestItf buildAppRequest(FwCoreStatusItf status)
			{
				if (firstStep)
				{
					appRequestBuilder.tryToBuildWithoutQuery(status);
					firstStep = false;
				}
				else
				{
					appRequestBuilder.tryToBuildWithQuery(status);
				}

				if (appRequestBuilder.hasAppRequest())
				{
					return appRequestBuilder.getAppRequestAfterHas();
				}

				throw new RuntimeException("AppRequest could not be built!");
			}

}
