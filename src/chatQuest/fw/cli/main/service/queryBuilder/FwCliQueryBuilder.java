package chatQuest.fw.cli.main.service.queryBuilder;

import chatQuest.fw.cli.main.library.queryNormalizer.FwCliQueryNormalizerItf;
import chatQuest.fw.cli.translation.library.translation.FwCliTranslationItf;
import chatQuest.fw.cli.main.service.prompter.FwCliPrompterItf;
import chatQuest.fw.cli.main.service.queryNormalizerContainer.FwCliQueryNormalizerContainerItf;
import chatQuest.fw.cli.translation.service.translationContainer.FwCliTranslationContainerItf;

public final class FwCliQueryBuilder
	implements
		FwCliQueryBuilderItf
{

	private FwCliPrompterItf prompter;
	private FwCliQueryNormalizerContainerItf queryNormalizerContainer;
	private FwCliTranslationContainerItf translationContainer;

	private String query;

	public FwCliQueryBuilder(
		FwCliPrompterItf prompter,
		FwCliQueryNormalizerContainerItf queryNormalizerContainer,
		FwCliTranslationContainerItf translationContainer
	)
	{
		this.prompter = prompter;
		this.queryNormalizerContainer = queryNormalizerContainer;
		this.translationContainer = translationContainer;
	}

	public void tryToBuild(String languageCode)
	{
		query = null;

		String answer = prompt(languageCode);
		if (answer != "")
		{
			FwCliQueryNormalizerItf cliQueryNormalizer = queryNormalizerContainer.get(languageCode);
			query = cliQueryNormalizer.normalize(answer);
		}
	}

		private String prompt(String languageCode)
		{
			FwCliTranslationItf translation = translationContainer.get(languageCode);
			return prompter.prompt(
				translation.cli_queryPrompt()
			);
		}

	public boolean hasQuery()
	{
		return query != null;
	}

	public String getQueryAfterHas()
	{
		return query;
	}

}
