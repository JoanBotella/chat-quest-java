package chatQuest.fw.cli.main.service.queryBuilder;

public interface FwCliQueryBuilderItf
{

	public void tryToBuild(String languageCode);

	public boolean hasQuery();

	public String getQueryAfterHas();

}
